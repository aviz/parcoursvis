import * as treeUtils from "@/treeUtils";

function wrapperFactory() {
  //0 -- 1 -- 2 -- 3
  //       -- 4
  //  -- 5 -- 6

  let tree = {
    id: 0,
    children: [
      {
        id: 1,
        children: [
          {
            id: 2,
            children: [
              {
                id: 3,
                children: []
              }
            ]
          },
          {
            id: 4,
            children: [],
          }
        ]
      },
      {
        id: 5,
        children: [
          {
            id: 6,
            children: [],
          }
        ],
      }
    ]
  };

  return tree;
}

describe("TreeUtils", () => {
  test("getParents", () => {
    let root = wrapperFactory();
    let node = root.children[0].children[1];
    let parents = treeUtils.getParents(root, node);

    expect(parents.length).toBe(2);
    expect(parents[0].id).toBe(0);
    expect(parents[1].id).toBe(1);
    expect(parents[1].children[1]).toBe(node);

    node = root.children[0].children[0].children[0];
    parents = treeUtils.getParents(root, node);
    expect(parents.length).toBe(3);
    for(let i = 0; i < 3; i++)
      expect(parents[i].id).toBe(i);

    expect(treeUtils.getParents(root, root).length).toBe(0);
  });

  test("searchNodeByID", () => {
    let root = wrapperFactory();
    let node = treeUtils.searchNodeByID(root, 3);
    expect(node.id).toBe(3);

    node = treeUtils.searchNodeByID(root, -1); //Does not exist
    expect(node).toBeNull();

    node = treeUtils.searchNodeByID(null, 0); //Does not exist
    expect(node).toBeNull();
  });

  test("hysteresisSortDesc", () => {
    let root1 =  {
      id: 0,
      count: 0,
      children: [
        {
          id: 1,
          count: 150
        },
        {
          id: 2,
          count: 100
        },
        {
          id: 3,
          count: 50
        },
        {
          id: 4,
          count: 49
        },
        {
          id: 5,
          count: 20
        }
       ]
      };
    root1.count = root1.children.reduce((x, y) => x+y.count, 0);

    let root2 =  {
      id: 0,
      count: 100,
      children: [
        {
          id: 1,
          count: 160
        },
        {
          id: 7,
          count: 220
        },
        {
          id: 3,
          count: 50
        },
        {
          id: 5,
          count: 20
        },
        {
          id: 4,
          count: 51
        },
        {
          id: 6,
          count: 23
        },
        {
          id: 2,
          count: 200
        },
       ]
      };
    let initialCount = root2.children.reduce((x, y) => x+y.count, 0);
    expect(initialCount).toBeGreaterThan(3); //Needed to check between 20 and 23
    root2.count = initialCount;

    let rootSorted = treeUtils.hysteresisSortDesc(root1, root2, 0.02);
    expect(rootSorted.children.length).toBe(7);
    expect(rootSorted.count).toBe(initialCount);

    //Check forall{i, j} : 0 < i < j < len(sortedNodes) -> weight(sortedNodes[j]) − weight(sortedNodes[i]) >= hysteresisInertia
    let isUnperfect=false;
    let inertia = 0.1*initialCount;
    for(let i = 0; i < rootSorted.children.length-1; i++) {
      for(let j = i+1; j < rootSorted.children.length; j++) {
        expect(rootSorted.children[j].count-rootSorted.children[i].count).toBeLessThanOrEqual(inertia);
      }
      isUnperfect |= rootSorted.children[i+1].count-rootSorted.children[i].count > 0;
    }
    expect(isUnperfect).toBeTruthy();

    //Check the "true" sort
    rootSorted = treeUtils.hysteresisSortDesc(null, root2, 0.02);
    expect(rootSorted.children.length).toBe(7);
    expect(rootSorted.count).toBe(initialCount);

    for(let i = 0; i < rootSorted.children.length-1; i++) {
      expect(rootSorted.children[i+1].count-rootSorted.children[i].count).toBeLessThanOrEqual(0);
    }
  });

  test("hysteresisSortAsc", () => {
    let root1 =  {
      id: 0,
      count: 0,
      children: [
        {
          id: 1,
          count: 150
        },
        {
          id: 2,
          count: 100
        },
        {
          id: 3,
          count: 50
        },
        {
          id: 4,
          count: 49
        },
        {
          id: 5,
          count: 20
        }
       ]
      };
    root1.count = root1.children.reduce((x, y) => x+y.count, 0);

    let root2 =  {
      id: 0,
      count: 100,
      children: [
        {
          id: 1,
          count: 160
        },
        {
          id: 7,
          count: 220
        },
        {
          id: 3,
          count: 50
        },
        {
          id: 5,
          count: 20
        },
        {
          id: 4,
          count: 51
        },
        {
          id: 6,
          count: 23
        },
        {
          id: 2,
          count: 200
        },
       ]
      };
    let initialCount = root2.children.reduce((x, y) => x+y.count, 0);
    expect(initialCount).toBeGreaterThan(3); //Needed to check between 20 and 23
    root2.count = initialCount;

    let rootSorted = treeUtils.hysteresisSortAsc(root1, root2, 0.02);
    expect(rootSorted.children.length).toBe(7);
    expect(rootSorted.count).toBe(initialCount);

    //Check forall{i, j} : 0 < i < j < len(sortedNodes) -> weight(sortedNodes[i]) − weight(sortedNodes[j]) >= hysteresisInertia
    let isUnperfect=false;
    let inertia = 0.1*initialCount;
    for(let i = 0; i < rootSorted.children.length-1; i++) {
      for(let j = i+1; j < rootSorted.children.length; j++) {
        expect(rootSorted.children[i].count-rootSorted.children[j].count).toBeLessThanOrEqual(inertia);
      }
      isUnperfect |= rootSorted.children[i].count-rootSorted.children[i+1].count > 0;
    }
    expect(isUnperfect).toBeTruthy();

    //Check the "true" sort
    rootSorted = treeUtils.hysteresisSortAsc(null, root2, 0.02);
    expect(rootSorted.children.length).toBe(7);
    expect(rootSorted.count).toBe(initialCount);

    for(let i = 0; i < rootSorted.children.length-1; i++) {
      expect(rootSorted.children[i].count-rootSorted.children[i+1].count).toBeLessThanOrEqual(0);
    }
  });

  test("HysteresisSort -- wrong cases", () => {
    let root1 =  {
      id: 0,
      count: 0,
      children: [
        {
          id: 1,
          count: 150
        },
        {
          id: 2,
          count: 100
        },
        {
          id: 3,
          count: 50
        },
        {
          id: 4,
          count: 49
        },
        {
          id: 5,
          count: 20
        }
       ]
      };
    root1.count = root1.children.reduce((x, y) => x+y.count, 0);

    //Remove id=5
    let root2 =  {
      id: 0,
      count: 100,
      children: [
        {
          id: 1,
          count: 160
        },
        {
          id: 2,
          count: 220
        },
        {
          id: 3,
          count: 50
        },
        {
          id: 4,
          count: 51
        },
       ]
      };

    root2.count = root2.children.reduce((x, y) => x+y.count, 0);

    let resDesc = treeUtils.hysteresisSortDesc(root1, {...root2, children: [...root2.children]});
    let resAsc  = treeUtils.hysteresisSortAsc(root1,  {...root2, children: [...root2.children]});

    //Expect a perfectly sorted result
    for(let i = 0; i < resDesc.children.length-1; i++) {
      expect(resDesc.children[i+1].count-resDesc.children[i].count).toBeLessThanOrEqual(0);
    }
    for(let i = 0; i < resAsc.children.length-1; i++) {
      expect(resAsc.children[i].count-resAsc.children[i+1].count).toBeLessThanOrEqual(0);
    }
  });
});
