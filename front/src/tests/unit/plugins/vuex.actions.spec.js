import { getters, actions, defaultFilter, defaultDistribution } from "@/plugins/vuex";
import { connection } from "../utils";
import { TabView, ComparisonMode } from "@/Config";
import { HistoryNode, History } from "@/History";

describe("Vuex Actions", () => {
  test("Initialization", async () => {
    const state = {
      server_url: `ws://${process.env.VUE_APP_IP_VM}:${process.env.VUE_APP_PORT_VM_WS}/ws/`,
      ws: null,
    };

    let initialize_set_display = false
    let commit   = (message, data=null) => {
      expect(message == "set_display" || message == "set_ws").toBeTruthy();
      if(message == "set_display") {
        expect(data).toBeNull();
        initialize_set_display = true;
      }
      else if(message == "set_ws") {
        state.ws = data;
      }
    };
    let dispatch = jest.fn();

    //Test the websocket
    actions.initApp({state, commit, dispatch});
    expect(initialize_set_display).toBeTruthy();
    expect(state.ws).toBeTruthy();
    let isConnected = await connection(state.ws);
    expect(isConnected).toBeTruthy();

    //Test that the initial message is sent
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch.mock.calls[0][0]).toBe("get_node_tree"); 
    expect(dispatch.mock.calls[0][1]).toStrictEqual({reset: true}); 

    let obj = {data: JSON.stringify({foo: "bar"})};

    //Send a message on the websocket
    state.ws.onmessage(obj);
    expect(dispatch).toHaveBeenCalledTimes(2);
    expect(dispatch.mock.calls[1][0]).toBe("recv_message"); 
    expect(dispatch.mock.calls[1][1]).toStrictEqual(JSON.parse(obj.data));

    //Check the resize
    jest.useFakeTimers();
    //Twice because of the throttle
    for(let i = 0; i < 2; i++) {
      initialize_set_display = false;
      global.dispatchEvent(new Event('resize'));
      jest.runAllTimers();
      expect(initialize_set_display).toBeTruthy();
    }
    jest.useRealTimers();
  });

  test("set_filter", () => {
    let filter = {
      duration: {max: 100, min: 0},
      age: {max: 100, min: 0},
      no_treatment_duration: 180,
      interruption_duration: 180,
      depth: 5,
      prune_threshold: 50,
      diseases: [{name: "disease1", status: "both"}],
      aggregate_coarse_level: 0,
      hidden_items: [],
    };

    let hasInsertedHistory = false;

    let commit = (message, data) => {
      expect(message).toBe("insert_history_node");
      expect(data.type).toBe("filter");
      expect(data.data).toStrictEqual(filter);
      hasInsertedHistory = true;
    };

    const state = {
      filter: defaultFilter()
    };

    actions.set_filter({state, commit}, {filter, saveToHistory: true});
    expect(getters.filter(state)).toStrictEqual(filter);
    expect(hasInsertedHistory).toBeTruthy();

    hasInsertedHistory = false;
    state.filter = defaultFilter();
    actions.set_filter({state, commit}, {filter, saveToHistory: false});
    expect(getters.filter(state)).toStrictEqual(filter);
    expect(hasInsertedHistory).toBeFalsy();
  });

  test("set_main_view_zoomed_node_id", () => {
    let id = 0
    let hasInsertedHistory = false;

    let commit = (message, data) => {
      expect(message).toBe("insert_history_node");
      expect(data.type).toBe("zoom_main_view");
      expect(data.data).toBe(id);
      hasInsertedHistory = true;
    };

    const state = {
      main_view_zoomed_node_id: -1
    };

    actions.set_main_view_zoomed_node_id({state, commit}, {id, saveToHistory: true});
    expect(getters.main_view_zoomed_node_id(state)).toBe(id);
    expect(hasInsertedHistory).toBeTruthy();

    //The function should do nothing if oldID == newID
    hasInsertedHistory = false;
    actions.set_main_view_zoomed_node_id({state, commit}, {id, saveToHistory: true});
    expect(getters.main_view_zoomed_node_id(state)).toBe(id);
    expect(hasInsertedHistory).toBeFalsy();

    //Repeat, but do not save to history
    state.main_view_zoomed_node_id = -1;
    actions.set_main_view_zoomed_node_id({state, commit}, {id, saveToHistory: false});
    expect(getters.main_view_zoomed_node_id(state)).toBe(id);
    expect(hasInsertedHistory).toBeFalsy();
  });

  test("set_sequence_view_zoomed_nodes_ids", () => {
    let ids = [0, 1]
    let hasInsertedHistory = false;

    let commit = (message, data) => {
      expect(message).toBe("insert_history_node");
      expect(data.type).toBe("zoom_sequence_view");
      expect(data.data).toBe(ids);
      hasInsertedHistory = true;
    };

    const state = {
      sequence_view_zoomed_nodes_ids: [-1, -1],
    };

    actions.set_sequence_view_zoomed_nodes_ids({state, commit}, {ids, saveToHistory: true});
    expect(getters.sequence_view_zoomed_nodes_ids(state)).toStrictEqual(ids);
    expect(hasInsertedHistory).toBeTruthy();

    //Test different possibilities by changing either the first or the second entry
    for(let i = 0; i < 2; i++) {
      ids = [ids[0], ids[1]]; //Copy
      ids[i] = -1;
      hasInsertedHistory = false;
      actions.set_sequence_view_zoomed_nodes_ids({state, commit}, {ids, saveToHistory: true});
      expect(getters.sequence_view_zoomed_nodes_ids(state)).toStrictEqual(ids);
      expect(hasInsertedHistory).toBeTruthy();
    }

    //The function should do nothing if oldID == newID
    hasInsertedHistory = false;
    actions.set_sequence_view_zoomed_nodes_ids({state, commit}, {ids, saveToHistory: true});
    expect(getters.sequence_view_zoomed_nodes_ids(state)).toStrictEqual(ids);
    expect(hasInsertedHistory).toBeFalsy();

    //Repeat, but do not save to history
    ids = [0, 1];
    actions.set_sequence_view_zoomed_nodes_ids({state, commit}, {ids, saveToHistory: false});
    expect(getters.sequence_view_zoomed_nodes_ids(state)).toStrictEqual(ids);
    expect(hasInsertedHistory).toBeFalsy();
  });

  test("on_align_sequence", () => {
    let sequence = ["alphabloc", "phyto"];
    let hasInsertedHistory = false;
    let hasSetSequenceView = false;
    let commit = (message, data) => {
      expect(message).toBe("insert_history_node");
      expect(data.type).toBe("align_sequence");
      expect(data.data).toStrictEqual({sequence: sequence, nOccurence: -1});
      expect(hasInsertedHistory).toBeFalsy();
      hasInsertedHistory = true
    };
    let dispatch = (message, data) => {
      expect(message).toBe("set_sequence_view_zoomed_nodes_ids");
      expect(data.ids).toStrictEqual([-1, -1]);
      expect(data.saveToHistory).toBeFalsy();
      expect(hasSetSequenceView).toBeFalsy();
      hasSetSequenceView = true;
    };

    actions.on_align_sequence({commit, dispatch}, {sequence, nOccurence: -1, saveToHistory: false});
    expect(hasInsertedHistory).toBeFalsy();
    expect(hasSetSequenceView).toBeTruthy();

    sequence = ["alphabloc", "fivealpha"];
    hasSetSequenceView = false;
    actions.on_align_sequence({commit, dispatch}, {sequence, nOccurence: -1, saveToHistory: true});
    expect(hasInsertedHistory).toBeTruthy();
    expect(hasSetSequenceView).toBeTruthy();
  });

  test("on_history", () => {
    let history = new History(new HistoryNode("filter", {name: "filter1"}, "root"));
    history.insertNode(new HistoryNode("filter", {name: "filter2"}, "filter2"), true);
    history.insertNode(new HistoryNode("align_sequence", {sequence: ["alphabloc"], nOccurence: 0}, "align"), true);
    history.insertNode(new HistoryNode("zoom_main_view", 1, "zoom"), true);
    history.insertNode(new HistoryNode("zoom_sequence_view", [2, 4], "zoom sequence"), true);

    let hasReset = false;

    const state = {
      history: history,
      filter: null,
      align_sequence: null,
      align_sequence_by: null,
      main_view_zoomed_node_id: 0,
      sequence_view_zoomed_nodes_ids: [-1, 2],
      history_reset: true,
      comparison_mode: ComparisonMode.NONE_COMPARISON_MODE,
      comparison_nodes: [null, null]
    };

    const dispatch = (message, data) => {
      expect(data.saveToHistory).toBeFalsy(); //Do not save anything in the history while replaying the history
      if(data.reset != undefined && data.reset) {
        expect(hasReset).toBeFalsy(); //Reset only once the tree!
        hasReset=true;
      }
      if(message == "update_node_tree") {
        state.filter = data.filter;
      }
      else if(message == "set_align_sequence") {
        state.align_sequence    = data.sequence;
        state.align_sequence_by = data.nOccurence;
      }
      else if(message == "set_main_view_zoomed_node_id")
        state.main_view_zoomed_node_id = data.id;
      else if(message == "set_sequence_view_zoomed_nodes_ids")
        state.sequence_view_zoomed_nodes_ids = data.ids;
    };

    actions.on_history({state, dispatch}, history.tree.children[0]); //filter -> filter
    expect(state.filter.name).toBe("filter2");
    expect(state.main_view_zoomed_node_id).toBe(-1);
    expect(state.sequence_view_zoomed_nodes_ids).toStrictEqual([-1, -1]);
    expect(state.align_sequence).toStrictEqual([]);
    expect(hasReset).toBe(true);

    //Test with comparisons without reset
    hasReset = false;
    state.history_reset = false;
    state.filter = state.align_sequence = state.align_sequence_by = null;
    state.comparison_mode = ComparisonMode.TARGET_COMPARISON_MODE;
    actions.on_history({state, dispatch}, history.tree);
    expect(state.filter.name).toBe("filter1");
    expect(state.main_view_zoomed_node_id).toBe(-1);
    expect(state.sequence_view_zoomed_nodes_ids).toStrictEqual([-1, -1]);
    expect(state.align_sequence).toStrictEqual([]);
    expect(state.comparison_mode).toBe(ComparisonMode.TARGET_COMPARISON_MODE);
    expect(state.comparison_nodes[ComparisonMode.TARGET_COMPARISON_MODE]).toBe(state.history.tree);
    expect(hasReset).toBe(false);

    //Go to the end of the tree
    state.history_reset = true;
    state.filter = state.align_sequence = state.align_sequence_by = null;
    actions.on_history({state, dispatch}, history.curNode);
    expect(state.filter.name).toBe("filter2");
    expect(state.main_view_zoomed_node_id).toBe(1);
    expect(state.sequence_view_zoomed_nodes_ids).toStrictEqual([2, 4]);
    expect(state.align_sequence).toStrictEqual(["alphabloc"]);
    expect(state.comparison_mode).toBe(ComparisonMode.TARGET_COMPARISON_MODE);
    expect(state.comparison_nodes[ComparisonMode.TARGET_COMPARISON_MODE]).toBe(state.history.curNode);
    expect(hasReset).toBe(true);
  });

  test("start_new_comparison", async () => {
    let sourceNode = new HistoryNode("source", {}, "source");
    let targetNode = new HistoryNode("target", {}, "target");

    const state = {
      comparison_mode: ComparisonMode.NONE_COMPARISON_MODE,
      comparison_nodes: [null, null],
      history: {jumpTo: jest.fn(), curNode: sourceNode, tree: new HistoryNode("root", {}, "root")},
      ws: {send: jest.fn()},
      unique_event_id: 0,
    };
    state[ComparisonMode.SOURCE_COMPARISON_MODE] = sourceNode;

    await actions.start_new_comparison({state}, targetNode);

    //Test the new state
    expect(getters.comparison_mode(state)).toBe(ComparisonMode.TARGET_COMPARISON_MODE);
    expect(getters.comparison_nodes(state)[0]).toBe(sourceNode);
    expect(getters.comparison_nodes(state)[1]).toBe(targetNode);

    //Check that the history commands have been issued correctly
    expect(getters.history(state).jumpTo).toHaveBeenCalledTimes(2);
    expect(getters.history(state).jumpTo.mock.calls[1][0]).toBe(targetNode);
    expect(getters.history(state).jumpTo.mock.calls[0][0]).toBe(state.history.tree);

    //Check that the network message has been issues correctly
    expect(state.ws.send).toHaveBeenCalledTimes(1);
    expect(JSON.parse(state.ws.send.mock.calls[0][0])).toMatchObject({
      action: 'comparison',
      params: {
        stop: true,
        ctx_index: ComparisonMode.TARGET_COMPARISON_MODE
      }
    });
  });

  test("set_comparison_mode", async () => {
    let sourceNode = new HistoryNode("source", {}, "source");
    let targetNode = new HistoryNode("target", {}, "target");

    const state = {
      comparison_mode: ComparisonMode.SOURCE_COMPARISON_MODE,
      comparison_nodes: [sourceNode, targetNode],
      history: {jumpTo: jest.fn(), curNode: sourceNode, tree: new HistoryNode("root", {}, "root")},
      ws: {send: jest.fn()},
      unique_event_id: 0,
    };

    const dispatch = (type, data) => {
      expect(type).toBe("get_node_tree");
      expect(data).toStrictEqual({reset: false, process: true});
    };

    //Test Source --> Target and Target --> Source
    for(let mode of [ComparisonMode.TARGET_COMPARISON_MODE, ComparisonMode.SOURCE_COMPARISON_MODE]) {
      await actions.set_comparison_mode({state, dispatch}, mode);
      expect(state.comparison_mode).toBe(mode);
      expect(state.history.jumpTo).toHaveBeenCalledTimes(1);
      expect(state.history.jumpTo).toHaveBeenLastCalledWith(state.comparison_nodes[mode]);
      expect(state.ws.send).toHaveBeenCalledTimes(1);
      expect(JSON.parse(state.ws.send.mock.calls[0][0])).toMatchObject({action: "comparison", params: {stop: false, ctx_index: mode}});

      state.ws.send        = jest.fn();
      state.history.jumpTo = jest.fn();
    }

    //Test Source --> None
    await actions.set_comparison_mode({state, dispatch}, ComparisonMode.NONE_COMPARISON_MODE);
    expect(state.comparison_mode).toBe(ComparisonMode.NONE_COMPARISON_MODE);
    expect(state.comparison_nodes[0]).toBe(sourceNode);
    expect(state.comparison_nodes[1]).toBeNull();
    expect(state.history.jumpTo).toHaveBeenCalledTimes(1);
    expect(state.history.jumpTo).toHaveBeenLastCalledWith(sourceNode);
    expect(state.ws.send).toHaveBeenCalledTimes(1);
    expect(JSON.parse(state.ws.send.mock.calls[0][0])).toMatchObject({action: "comparison", params: {stop: true, ctx_index: ComparisonMode.SOURCE_COMPARISON_MODE}});
    state.ws.send        = jest.fn();
    state.history.jumpTo = jest.fn();

    //Test None --> None
    await actions.set_comparison_mode({state, dispatch}, ComparisonMode.NONE_COMPARISON_MODE);
    expect(state.comparison_mode).toBe(ComparisonMode.NONE_COMPARISON_MODE);
    expect(state.comparison_nodes[0]).toBe(sourceNode);
    expect(state.comparison_nodes[1]).toBeNull();
    expect(state.history.jumpTo).toHaveBeenCalledTimes(0);
    expect(state.ws.send).toHaveBeenCalledTimes(0);
  });

  test("set_hidden_items", async () => {
    const filter = defaultFilter();
    const state = {
      filter: {...filter},
    };
    const items = ["alphabloc"];
    let hasUpdateNode = false;

    const dispatch = (message, data) => {
      expect(message).toBe("update_node_tree");
      expect(data.filter).toStrictEqual({...filter, hidden_items: items});
      expect(data.reset).toBeFalsy();
      hasUpdateNode = true;
    };

    await actions.set_hidden_items({state, dispatch}, items);
    expect(hasUpdateNode).toBeTruthy();
  });

  test("update_node_tree", async () => {
    let hasSetFilter = false;
    let hasGotNode   = false;

    let foo = {type: "bar"};

    const dispatch = (message, data) => {
      if(message == "set_filter") {
        hasSetFilter = true;
        expect(data).toMatchObject({filter: foo, saveToHistory: false});
      }
      else if(message == "get_node_tree") {
        expect(hasSetFilter).toBeTruthy();
        hasGotNode = true;
        expect(data).toMatchObject({reset: true});
      }
    };

    await actions.update_node_tree({dispatch}, {filter: foo, saveToHistory: false});
    expect(hasSetFilter).toBeTruthy();
    expect(hasGotNode).toBeTruthy();
  });

  test("get_node_tree", async () => {
    const filter = defaultFilter();
    const state = {
      filter: filter,
      ws: {send: jest.fn()}
    };
    let hasResetMainViewZoom     = false;
    let hasResetSequenceViewZoom = false;
    
    const commit = (type) => {
        expect(type).toBe("set_distribution");
    };

    const dispatch = (type, data) => {
      if(type == "set_main_view_zoomed_node_id") {
        hasResetMainViewZoom = true;
        expect(data).toMatchObject({id: -1, saveToHistory: false});
      }
      else if(type == "set_sequence_view_zoomed_nodes_ids") {
        hasResetSequenceViewZoom = true;
        expect(data).toMatchObject({ids: [-1, -1], saveToHistory: false});
      }
      else //Invalid message
        expect(false).toBeTruthy();
    };

    await actions.get_node_tree({state, commit, dispatch}, {reset: false, process: true});
    expect(hasResetMainViewZoom).toBeFalsy();
    expect(hasResetSequenceViewZoom).toBeFalsy();
    expect(state.ws.send).toHaveBeenCalledTimes(1);
    expect(JSON.parse(state.ws.send.mock.calls[0][0])).toMatchObject({params: {}, reset: false, process: true, action: "node_tree"});
    state.ws.send=jest.fn();


    await actions.get_node_tree({state, commit, dispatch}, {reset: true, process: false});
    expect(hasResetMainViewZoom).toBeTruthy();
    expect(hasResetSequenceViewZoom).toBeTruthy();
    expect(state.ws.send).toHaveBeenCalledTimes(1);
    expect(JSON.parse(state.ws.send.mock.calls[0][0])).toMatchObject({params: state.filter, reset: true, process: false, action: "node_tree"});
  });

  test("recv_message", async () => {
    let hasSetTree         = false;
    let hasUpdateTree      = false;
    let hasGotNode         = false;
    let hasSetDistribution = false;
    let hasGotDistribution = false;

    let tree         = {foo: "bar", done: false};
    let last_distribution_params = {node_id: 0, action: "distribution", color: "#ffff00ff"}; 
    let distribution = defaultDistribution();

    const dispatch = (type, data) => {
      if(type == "get_node_tree") {
        expect(type).toBe("get_node_tree");
        expect(data).toMatchObject({reset: false, process: true});
        hasGotNode = true;
      }
      else if(type == "get_distribution") {
        hasGotDistribution = true;
        expect(data).toMatchObject(last_distribution_params);
      }
      else {
        expect(false).toBeTruthy();
      }
    };

    const commit = (type, data) => {
      if(type == "set_tree") {
        hasSetTree = true;
        expect(data).toBe(tree);
      }
      else if(type == "set_current_tree") {
        expect(hasSetTree).toBeTruthy();
        hasUpdateTree = true;
      }
      else if(type == "set_distribution") {
        hasSetDistribution = true;
        expect(data).toBe(distribution);
      }
      else //Invalid
        expect(false).toBeTruthy();
    };

    const state = {
      distribution: defaultDistribution(),
      last_distribution_params: last_distribution_params,
    };

    //Test nodetree messages
    //Not yet done (i.e., still in progression)
    await actions.recv_message({state, commit, dispatch}, {type: "nodetree", data: tree});
    expect(hasSetDistribution).toBeFalsy();
    expect(hasSetTree).toBeTruthy();
    expect(hasUpdateTree).toBeTruthy();
    expect(hasGotNode).toBeTruthy();
    //Done
    hasSetTree = hasUpdateTree = hasGotNode = false;
    tree.done = true;
    await actions.recv_message({state, commit, dispatch}, {type: "nodetree", data: tree});
    expect(hasSetTree).toBeTruthy();
    expect(hasUpdateTree).toBeTruthy();
    expect(hasGotNode).toBeFalsy(); //Do not ask anything else as we are done with the progressive algorithm

    //Test distribution messages
    hasSetTree = hasUpdateTree = hasGotNode = false;
    await actions.recv_message({state, commit, dispatch}, {type: "distribution", data: distribution});
    expect(hasSetDistribution).toBeTruthy();
    expect(hasSetTree).toBeFalsy();
    expect(hasUpdateTree).toBeFalsy();
    expect(hasGotNode).toBeFalsy();
    expect(hasGotDistribution).toBeFalsy();

    hasSetTree = hasUpdateTree = hasGotNode = hasSetDistribution = false;
    state.distribution.age.bins = [0,1,2,3];
    await actions.recv_message({state, commit, dispatch}, {type: "nodetree", data: tree});
    expect(hasSetDistribution).toBeFalsy();
    expect(hasGotDistribution).toBeTruthy();
  });

  test("get_distribution", async () => {
    const state = {
      unique_event_id: 0,
      requested: {color: undefined},
      ws: {send: jest.fn()},
    };

    const params = {
      color: "red",
      foo: "bar",
    };

    let hasSetTab = false;

    const commit = (message, data) => {
      expect(message).toBe("set_tab");
      expect(data).toBe(TabView.DETAIL_TAB);
      expect(hasSetTab).toBeFalsy();
      hasSetTab = true;
    };

    await actions.get_distribution({state, commit}, params);
    expect(getters.requested(state).color).toBe(params.color);
    expect(state.ws.send).toHaveBeenCalledTimes(1);
    expect(JSON.parse(state.ws.send.mock.calls[0][0])).toMatchObject(params);
  });

  test("set_align_sequence", async () => {
    const state = {
      unique_event_id: 0,
      ws: {send: jest.fn()},
      full_tree: {types: [{name: "alphabloc", id: 0},
                          {name: "phyto", id: 1}
                         ]
                 },
    };

    let i = 0;
    for(let sequence of [["phyto", "alphabloc"], null]){
      let params = {
        sequence: sequence,
        nOccurence: -1
      };

      let hasOnAlign = false;

      const commit = (message) => {
        expect(message).toBe("set_distribution");
      };

      const dispatch = (message, data) => {
        expect(message).toBe("on_align_sequence");
        expect(data).toStrictEqual({...params, saveToHistory: false});
        expect(hasOnAlign).toBeFalsy();
        hasOnAlign = true;
      };

      actions.set_align_sequence({state, commit, dispatch}, {...params, saveToHistory: false});
      expect(state.ws.send).toHaveBeenCalledTimes(i+1);
      expect(JSON.parse(state.ws.send.mock.calls[i][0])).toMatchObject({params: {align_sequence: (sequence == null ? [] : [1, 0]), align_sequence_by: -1},
                                                                        reset: true,
                                                                        process: true,
                                                                        event_id: i,
                                                                        action: "node_tree"});
      i++;
    }
  });
});
