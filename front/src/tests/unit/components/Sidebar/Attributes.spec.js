import { mount, createLocalVue } from '@vue/test-utils';

import Vuetify from 'vuetify';
import Vuex from 'vuex';
import Vue from 'vue';

import { defaultFilter, getters } from "@/plugins/vuex";
import AttributesView from "@/components/Sidebar/Attributes.vue";

Vue.use(Vuetify);
Vue.use(Vuex);

const localVue = createLocalVue();

let App = localVue.component('App', {
      components: { AttributesView },
      template: `
        <div data-app>
          <AttributesView />
        </div>
      `
    })

describe('Attributes.vue', () => {
  let state;
  let actions;

  let store;
  let vuetify;

  let defaultAttributes = defaultFilter();

  beforeEach(() => {
    state = {
      attributes: {
        diseases: { default: ["Diabete", "Hypertension"] },
        depth:     { min: 3, max: 50, default: defaultAttributes.depth,  name: "depth" },
        prune_threshold: { min: 0, max: 50, default: defaultAttributes.prune_threshold, name: "prune_threshold" },
        no_treatment_duration: { max: 720, min: 0, default: defaultAttributes.no_treatment_duration, name: "no treatment duration" },
        interruption_duration: { max: 720, min: 0, default: defaultAttributes.interruption_duration, name: "interruption duration" },
        duration: { max: 2000, min: 0, default: [...defaultAttributes.duration], view: 1200, name: "treatment duration" },
        age: { max: 120, min: 0, default: [...defaultAttributes.age], name: "patient age" },
        aggregate_coarse_level: {min: 0, default: defaultAttributes.aggregate_coarse_level, name: "aggregation level"},
        hidden_items: [],
      },
      filter: {...defaultAttributes}
    };
    actions = {
      update_node_tree : jest.fn()
    };

    store   = new Vuex.Store({ state, actions, getters });
    vuetify = new Vuetify();
  });

  function wrapperFactory() {
    const wrapper = mount(App, {localVue, store, vuetify, attachTo: document.body});
    return wrapper;
  }

  test("Manipulate Widgets", async () => {
    const wrapper = wrapperFactory();
    await wrapper.vm.$nextTick();

    //Click when nothing changed
    wrapper.find("#update-filter").trigger('click');
    await wrapper.vm.$nextTick();
    expect(actions.update_node_tree).toHaveBeenCalledTimes(0);

    wrapper.findComponent(AttributesView).vm.$data.localFilter.depth=50;
    await wrapper.vm.$nextTick();

    //Apply changes
    wrapper.find("#update-filter").trigger('click');
    await Vue.nextTick();

    let newFilter = {...defaultAttributes};
    newFilter.depth = 50;

    //Check that update_node_tree has been updated
    expect(actions.update_node_tree).toHaveBeenCalledTimes(1);
    expect(actions.update_node_tree.mock.calls[0][1].filter.depth).toBe(50);

    //Reset
    wrapper.find("#reset-filter").trigger('click');
    await Vue.nextTick();

    //Check the depth value
    expect(wrapper.findComponent(AttributesView).vm.$data.localFilter.depth).toBe(5);

    //Click again
    wrapper.find("#update-filter").trigger('click');
    await wrapper.vm.$nextTick();
    expect(actions.update_node_tree).toHaveBeenCalledTimes(2);
    expect(actions.update_node_tree.mock.calls[1][1].filter.depth).toBe(5);
  });
});
