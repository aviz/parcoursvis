import { shallowMount } from '@vue/test-utils';

import Modal from '@/modals/Modal';

function wrapperFactory() {
  const wrapper = shallowMount(Modal, {
                                 propsData: {
                                   showModal: true,
                                 },
                                 slots: {
                                   header: "header",
                                   body: "body",
                                   footer: "footer"
                                 }
                              });
  return wrapper;
}

describe('Modal.vue', () => {
  test("Visibility", async () => {
    let wrapper = wrapperFactory();
    await wrapper.vm.$nextTick();
    expect(wrapper.find("*").exists()).toBeTruthy();

    wrapper.setProps({showModal: false});
    await wrapper.vm.$nextTick();
    expect(wrapper.find("*").exists()).toBeFalsy();
  });

  test("Slots", () => {
    let wrapper = wrapperFactory();
    expect(wrapper.html()).toContain("header");
    expect(wrapper.html()).toContain("body");
    expect(wrapper.html()).toContain("footer");
  });

  test("Click", async () => {
    let wrapper = wrapperFactory();
    await wrapper.find(".modal-mask").trigger("click");
    expect(wrapper.emitted().clickMask).toBeTruthy();
  });
});
