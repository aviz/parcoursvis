/** \brief  Basic listener interface to listener to "History" event. */
class HistoryListener {

  /** \brief  Called when the "undo" function is being called 
   * \param _newNode the new current node
   * \param _oldNode the old current node*/
  onUndo(_newNode, _oldNode) {}

  /** \brief  Called when the "redo" function is being called 
   * \param _newNode the new current node
   * \param _oldNode the old current node*/
  onRedo(_newNode, _oldNode) {}

  /** \brief  Called when the "jumpNode" function is being called
   * \param _newNode the new current node
   * \param _oldNode the old current node*/
  onJumpNode(_newNode, _oldNode) {}
}

/** Node class to use with the tree "History"*/
class HistoryNode {
  /** Constructor
   * \param type The type of the node, useful to know what this node refers to
   * \param data The internal data associated with the history. For the moment, only "raw data" that store the whole state of the application (at any point in time) is accepted
   * \param label The label to associate this node with, useful for rendering a Tree associated with the History*/
  constructor(type, data, label="") {
    this.timestamp  = Date.now(); /*!< The timestamp of when this node was created. This can be overwritten if needed*/
    this.type       = type;  /*!< The type of the node. Use 'label' for display information*/
    this.label      = label; /*!< The label to display*/
    this.parent     = null;  /*!< What is the parent of this node?*/
    this.children   = [];    /*!< What are the children of this node?*/
    this.nextRedo   = null;  /*!< What would be the next node to use in the redo process? This is used exclusively by the "History" object*/
    this.data       = data;  /*!< The data associated to this node*/
    this.ephemeral  = false; /*!< Is this node ephemeral?*/
    this.extend     = true;  /*!< Parameter used for the rendering: do we expose its children?*/
    this.bookmarked = false; /*!< Is this node bookmarked?*/
  }

  /** Add a new node as a child 
   * \param node the node to add */
  addChild(node) {
    this.children.push(node);
    node.parent = this;
  }

  /** Get the JSON string of the "this.children" attribute 
   * \param indent the indetation to apply
   * \return the JSON string corresponding to this.children*/
  childrenString(indent) {
    if(this.children.length == 0) return "[]";
    let str = '[';
    for(let i = 0; i < this.children.length-1; i++)
      str += this.children[i].toString(indent) + ', ';
    str += this.children[this.children.length-1].toString(indent) + ']';
    return str;
  }

  toString(indent=0) {
    let indentStr = ' '.repeat(indent);
    return `{
${indentStr}"timestamp"  : ${this.timestamp},
${indentStr}"type"       : "${this.type}",
${indentStr}"label"      : "${this.label}",
${indentStr}"ephemeral"  : ${this.ephemeral},
${indentStr}"bookmarked" : ${this.bookmarked},
${indentStr}"data"       : ${JSON.stringify(this.data)},
${indentStr}"children"   : ${this.childrenString(indent+2)}
${indentStr}}`
  }

  /** Import from a JSON object (i.e., already parsed using JSON.parse) a previous stored HistoryNode object. 
   * \return a new HistoryNode object*/
  static importFromJSONObject(obj) {
    let node = new HistoryNode(obj.type, obj.data, obj.label);
    node.timestamp  = obj.timestamp;
    node.ephemeral  = obj.ephemeral;
    node.bookmarked = obj.bookmarked;
    for(let i = 0; i < obj.children.length; i++)
      node.addChild(HistoryNode.importFromJSONObject(obj.children[i]));
    return node;
  }
}

/** Create an history based on users' events. */
class History {
  /** Initialize the history with a root HistoryNode
   * \param root the HistoryNode to use as the root of this History*/
  constructor(root) {
    this.tree          = root;
    this.curNode       = root;
    this.listeners     = [];
    this.absoluteTypes = [];
    this.disableSaving = false;
  }

  /** Add a new listener to call on events
   * \param listener the listener to add. It should follow the same interface as HistoryListener 
   * \return true if the listener was not already registered, false otherwise. If false, this function does nothing*/
  addListener(listener) {
    let idx = this.listeners.indexOf(listener);
    if(idx == -1) {
      this.listeners.push(listener);
      return true;
    }
    return false;
  }

  /** \brief  Set all the types that contain absolute data (i.e., the whole state of the application)
  * \param types array of types (string) that contain all the absolute types */
  setAbsoluteTypes(types) {
    this.absoluteTypes = types;
  }

  /** \brief  Replay the history that would lead to the state of node. this.curNode is not modified. If you want to do so, call "jumpTo" either before (and then call "replayHistory" as a callback) or after "replayHistory"
   * \param clbk the callback function to call. It should accept only one parameter: the node to apply the changes (either delta or absolute). If no absolute node has been found, clbk is first called with a parameter==null to reset the application to its default state.
   * \param node the node to replay the history to. Set it to null to replay the history to this.curNode from an unknown state */
  replayHistory(clbk, node) {
    this.disableSaving = true;
    if(node == null)
      node = this.curNode;

    //If this node if an absolute node: it contains everything
    if(this.absoluteTypes.indexOf(node.type) != -1) {
      clbk(node);
      this.disableSaving = false;
      return;
    }

    let clbkTree = [];

    //Otherwise node is the child of the previous node -> apply the diff
    //We also want to force to replay the history if node == this.curNode
    if(this.curNode != node && History.isNodeAvailable_rec(this.curNode, node)) {
      for(let temp = node; temp != this.curNode; temp = temp.parent) {
        clbkTree.push(temp);
        if(this.absoluteTypes.indexOf(temp.type) != -1) //Absolute state: stop there
          break;
      }
    }

    //If nothing previous applies, we need to replay the whole history from an absolute point
    else {
      let foundAbsolute = false;
      //Otherwise search for an absolute state
      for(let temp = node; temp != null; temp = temp.parent) {
        clbkTree.push(temp);
        if(this.absoluteTypes.indexOf(temp.type) != -1) {
          foundAbsolute = true;
          break;
        }
      }
      if(!foundAbsolute) //If absolute was not found --> need to reset the whole state to its default value
        clbk(null);
    }

    //Replays all the delta 
    for(let i = clbkTree.length-1; i >= 0; i--)
      clbk(clbkTree[i]);
    this.disableSaving = false;
  }

  /** Remove an already registered listener
   * \param listener the listener to remove.
   * \return true if the listener was already registered, false otherwise*/
  removeListener(listener) {
    let idx = this.listeners.indexOf(listener);
    if(idx != -1) {
      this.listeners.splice(idx);
      return true;
    }
    return false;
  }

  /** Insert a node at the current selected node (this.curNode)
   * \param node the node to-be inserted
   * \param asCurrent true if the new node should be the current node, false otherwise*/
  insertNode(node, asCurrent=false) {
    if(this.disableSaving)
      return;

    this.curNode.addChild(node);
    if(asCurrent)
      this.curNode = node;
  }

  /** State whether the undo functionality is meaningful or not*/
  get canUndo() {
    return this.curNode.parent != null;
  }

  /** State whether the redo functionality is meaningful or not*/
  get canRedo() {
    return this.curNode.nextRedo != null;
  }

  /** Undo to the previous node in the hierarchy
   * \return true on success (canUndo), false otherwise*/
  undo() {
    if(this.canUndo) {
      let oldNode = this.curNode;
      let parent = this.curNode.parent;
      parent.nextRedo = this.curNode;

      this.listeners.forEach(l => l.onUndo(this.curNode.parent, oldNode));
      this.curNode    = this.curNode.parent;
      return true;
    }
    return false;
  }

  /** Go back to the previous visited node
   * \return true on success (canRedo), false otherwise*/
  redo() {
    if(this.canRedo) {
      let oldNode      = this.curNode;
      let curNode      = oldNode.nextRedo;
      oldNode.nextRedo = null;

      this.listeners.forEach(l => l.onRedo(curNode, oldNode));
      this.curNode = curNode;

      return true;
    }
    return false;
  }

  /** Jump to a specific node
   * \param node The node to jump to. Redo will be disactivated. The function does not jump to a node that is currently selected (curNode == node) 
   * \return true on success, false otherwise*/
  jumpTo(node) {
    if(node == this.curNode)
      return false;

    if(!this.isNodeAvailable(node))
      return false;

    let oldNode = this.curNode;
    oldNode.nextRedo = null;

    this.listeners.forEach(l => l.onJumpNode(node, oldNode));
    this.curNode = node;
    return true;
  }

  /** \brief  Is the node "node" part of the History?
   * \param node The node to look for
   * \return true on success, false otherwise*/
  isNodeAvailable(node) {
    return History.isNodeAvailable_rec(this.tree, node);
  }

  /** \brief  Is the node "node" part of the History as defined by "curtree"? (this function is recursive and should not be called outside the class)
   * \param node The node to look for
   * \return true on success, false otherwise*/
  static isNodeAvailable_rec(curtree, node) {
    if(curtree === node)
      return true;
    for(let i = 0; i < curtree.children.length; i++)
      if(History.isNodeAvailable_rec(curtree.children[i], node))
        return true;
    return false;
  }

  /** Export the history to a JSON format
   * \return the JSON string associated with this object*/
  toString() {
    let treeStr = this.tree.toString(4);
    let curNodeIDs = [];
    for(let curNode = this.curNode; curNode.parent != null; curNode = curNode.parent)
      curNodeIDs.push(curNode.parent.children.indexOf(curNode));

    return `{
  "tree": ${treeStr},
  "curNodeIDs": [${curNodeIDs.toString()}]
}`;
  }

  /** Clone this History into a "Bookmark" history.
   * \return a new History consisted only of bookmarked nodes AND all nodes from the deepest bookmarked node and the current node. All nodes are a shallow copy of the original tree, except that the "parent" property refers to the new created bookmark history. The root node is always included.
   * Each object is a HistoryNode with an additional property: "originalNode"*/
  cloneBookmark() {
    //First find the direct bookmarked relative of the current node. Useful to determine all the nodes 
    let parentBookmarked  = null;
    for(let curNode = this.curNode.parent; curNode != null; curNode = curNode.parent) {
      if(curNode.bookmarked) {
        parentBookmarked = curNode;
        break;
      }
    }

    let originalTree = this.tree;
    let cloneBookmark_rec = (retNode, curNode) => {
      if(curNode == null)
        return;

      if(curNode != originalTree && curNode.bookmarked) {
        let newNode = {...curNode, originalNode: curNode};
        retNode.addChild(newNode);

        if(curNode == parentBookmarked){
          let lastNode = null;
          for(let temp = this.curNode; temp != curNode; temp = temp.parent) {
            let newNode = {...temp, originalNode: temp};
            if(lastNode != null)
              newNode.addChild(lastNode);
          }

          if(lastNode != null)
            newNode.addChild(lastNode);
        }
        retNode = newNode;
      }

      for(let child in curNode.children){
        cloneBookmark_rec(retNode, child);
      }
    };

    let newRoot = {...this.tree, originaNode: this.tree};
    cloneBookmark_rec(newRoot, this.tree);
    return newRoot;
  }

  /** Import from a JSON string a previous stored History object. Curnode is set to its stored state
   * \return a new History object*/
  static importFromJSON(str) {
    let obj  = JSON.parse(str);
    let root = HistoryNode.importFromJSONObject(obj.tree);
    let history = new History(root);
    let curNode = root;
    for(let i = obj.curNodeIDs.length-1; i >= 0 && obj.curNodeIDs[i] < curNode.children.length; curNode = curNode.children[obj.curNodeIDs[i--]]);
    if(curNode == null)
      curNode = root;
    history.curNode = curNode;

    return history;
  }
}

export { History, HistoryListener, HistoryNode };
