/** \brief The colormap to use for the different type of events. */
let color_map = {
  "alphabloc": "#ffeda0",
  "phyto": "#84B1D8",
  "fivealpha": "#EE8373",
  "alphabloc phyto": "#B9DC7C",
  "interruption": "#D9D9D9",
  "fivealpha alphabloc": "#F4B862",
  "surgery": "#D9302C",
  "no treatment": "#999999",
  "fivealpha phyto": "#B582BB",
  "fivealpha alphabloc phyto": "#A0562F",
  "death": "#555555",
  "medication": "#EEA073",
  "pause": "lightgray",
  "default": "lightgray",
};

/** \brief In addition to color_map, this object lists, for all type of event, whether the background (color_map) is a dark color */
let is_color_dark = {
  "alphabloc": false,
  "phyto": false,
  "fivealpha": true,
  "alphabloc phyto": false,
  "interruption": false,
  "fivealpha alphabloc": false,
  "surgery": true,
  "no treatment": true,
  "fivealpha phyto": true,
  "fivealpha alphabloc phyto": true,
  "death": true,
  "medication": false,
  "pause": false,
  "default": false,
};

export { color_map, is_color_dark };
