import { ViewType, TabView, ComparisonMode, TreeToDisplay } from '../Config';
import { History, HistoryNode } from '../History';
import { color_map, is_color_dark } from '../color';
import { hysteresisSortAsc } from '../treeUtils';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex)

/** \brief  Re-init the default filter this application relies on
  * \return  the default filter to apply */
export function defaultFilter() {
  return {
    diseases: [{ name: 'Diabete', status: 'both' }, { name: 'Hypertension', status: 'both' }],
    depth: 5,
    prune_threshold: 50,
    duration: [0, 2000],
    age: [0, 120],
    no_treatment_duration: 365,
    interruption_duration: 180,
    aggregate_coarse_level: 0,
    hidden_items: [],
  }
}

/** \brief  The default align_sequence event
  * \return  The default state of the align_sequence event */
let defaultAlignedSequence = function() {
  return {sequence: [], nOccurence: 0};
};

/** \brief  The default zoom_main_view event
  * \return  The default state of the zoom_main_view event */
let defaultZoomInMainView = function() {
  return -1;
};

/** \brief  The default zoom_sequence_view event
  * \return  The default state of the zoom_sequence_view event */
let defaultZoomInSequenceView = function() {
  return [-1, -1];
};

/** \brief  The default distribution to use.
  * \return  The default distribution to use*/
export function defaultDistribution() {
  return { age: {}, duration: {}, disease: {}, nbValues: 0 }
}

let defaultAttributes = defaultFilter();

export const state = {
  server_url: (window.location.protocol == 'https:' ? `wss://${process.env.VUE_APP_IP_VM}:${process.env.VUE_APP_PORT_VM_WSS}/${process.env.VUE_APP_SERVER_PREFIX}/ws/` : 
                                                      `ws://${process.env.VUE_APP_IP_VM}:${process.env.VUE_APP_PORT_VM_WS}/${process.env.VUE_APP_SERVER_PREFIX}/ws/`),
  /** \brief Default information for the filtering view (minimum allowed values, maximum allowed values, default value, their name for the user, etc...) */
  attributes: {
    diseases: { default: ["Diabete", "Hypertension"] },
    depth:     { min: 3, max: 50, default: defaultAttributes.depth,  name: "depth" },
    prune_threshold: { min: 0, max: 50, default: defaultAttributes.prune_threshold, name: "prune_threshold" },
    no_treatment_duration: { max: 720, min: 0, default: defaultAttributes.no_treatment_duration, name: "no treatment duration" },
    interruption_duration: { max: 720, min: 0, default: defaultAttributes.interruption_duration, name: "interruption duration" },
    duration: { max: 2000, min: 0, default: [...defaultAttributes.duration], view: 1200, name: "treatment duration" },
    age: { max: 120, min: 0, default: [...defaultAttributes.age], name: "patient age" },
    date: { name: "date" },
    aggregate_coarse_level: {min: 0, default: defaultAttributes.aggregate_coarse_level, name: "aggregation level"},
    hidden_items: {default: []},
  },
  filter: defaultFilter(),
  history: new History(
    new HistoryNode("filter", defaultFilter(), "start")
  ),
  history_reset: true, /* Can we reset the history on jumpTo?*/
  comparison_nodes: [null, null], /* The nodes, in the history, to compare. index==0 -> current state. index==1 -> compared state*/
  comparison_mode: ComparisonMode.NONE_COMPARISON_MODE,
  history_listener: null,
  current_sequence: [], /*The sequence selected by clicking on a node*/
  align_sequence: [],   /*The current sequence to which the view is aligned by*/
  align_sequence_by: 0, /*To what occurence should the sequence be aligned with?*/
  diseases: [ //Colors computed with color_brewer, divergence, 8-class BrBG
      { name: "Diabete",      color: "#8c510a" },
      { name: "Hypertension", color: "#f6e8c3" }
  ],
  /** \brief Basic information for the layout of the application */
  display: {
    banner: { height: 64 }, /*!< Size of the banner*/
    footer: { height: 50 }, /*!< Size of the footer*/
    zooms:  {height: 40 },
    main: { height: 0, width: 0, innerHeight: 0, innerWidth: 0, padding: { left: 30, top: 5, right: 30, bottom: 20} }, /*!< Layout information for the main view (see ViewType)*/
    side: { height: 0, width: 0 } /*!< Layout information for components in the SideView*/
  },
  /** \brief  Display flag is used as an event object that all components listen to readjust their layout. We currently use Math.random()*/
  display_flag: undefined,
  /** \brief  The color to use for a request. Only used by Detail to display its histograms (for the moment) */
  requested: { color: undefined },
  unique_event_id: 0, // sent with a socket request, incremented each time
  full_tree: null, // non filtered tree
  current_tree: null, // tree updated with meta data
  /** \brief the current distribution to display. By default, no distribution is to be displayed */
  distribution: defaultDistribution(),
  last_distribution_params: null,
  /** \brief  The current tab of the Sidebar */
  tab: TabView.OVERVIEW_TAB,
  /** \brief the websocket of the backend server*/
  ws: null,
  /** \brief The mainview to display (see ViewType)*/
  view_type: ViewType.MAIN_VIEW_TYPE,
  /** \brief the current tree (see TreeToDisplay) to display in the sequence_view view mode*/
  sequence_view_curtree_to_display : TreeToDisplay.BEFORE,
  /** \brief Array of the IDs of the nodes the sequence_view has zoomed into. Index 0 is for TreeToDisplay.BEFORE, and Index 1 curresponds to TreeToDisplay.AFTER.*/
  sequence_view_zoomed_nodes_ids: [-1, -1],
  /** \brief the node ID the main view has zoomed into*/
  main_view_zoomed_node_id: -1,
};

export const getters = {
  history                           : state => state.history,
  comparison_mode                   : state => state.comparison_mode,
  comparison_nodes                  : state => state.comparison_nodes,
  attributes                        : state => state.attributes,
  color_map                         : () => color_map,
  is_color_dark                     : () => is_color_dark,
  diseases                          : state => state.diseases,
  display                           : state => state.display,
  display_flag                      : state => state.display_flag,
  filter                            : state => state.filter,
  tree                              : state => state.current_tree,
  distribution_age                  : state => state.distribution.age.bins       ? state.distribution.age      : null,
  distribution_duration             : state => state.distribution.duration.bins  ? state.distribution.duration : null,
  distribution_disease              : state => state.distribution.disease.length ? state.distribution.disease  : null,
  distribution_nbValues             : state => state.distribution.nbValues,
  total                             : state => (state.full_tree != null ? state.full_tree.nb_processed   : 0),
  expected_total                    : state => (state.full_tree != null ? state.full_tree.total_count    : 0),
  filtered_total                    : state => (state.full_tree != null ? state.full_tree.filtered_count : 0),
  requested                         : state => state.requested,
  tab                               : state => state.tab,
  view_type                         : state => state.view_type,
  current_sequence                  : state => state.current_sequence,
  align_sequence                    : state => state.align_sequence,
  align_sequence_by                 : state => state.align_sequence_by,
  main_view_zoomed_node_id          : state => state.main_view_zoomed_node_id,
  sequence_view_curtree_to_display  : state => state.sequence_view_curtree_to_display,
  sequence_view_zoomed_nodes_ids    : state => state.sequence_view_zoomed_nodes_ids,
  type_hierarchy                    : state => (state.full_tree != null ? state.full_tree.type_hierarchy : null),
};

export const mutations = {
  set_history(state, history) {
    if(state.history != null)
      state.history.removeListener(state.history.history_listener);
    state.history = history;

    //Force a jump from root to curNode
    let curNode = state.history.curNode;
    state.history.curNode = state.history.tree;
    state.history.addListener(state.history_listener);
    state.history.jumpTo(curNode);
  },
  insert_history_node(state, node) {
    state.history.insertNode(node, true);
    if(state.comparison_mode != ComparisonMode.NONE_COMPARISON_MODE)
      state.comparison_nodes[state.comparison_mode] = node;
  },
  set_tree(state, data) {
    // stores the full tree received, no filter
    if(data == null) {
      state.full_tree = null;
      return;
    }
    data = { ... data, name : 'root' };
    if(data.children.length > 0) {
      if(data.children[0].name == "dual_sequence")
        this.commit("set_view_type", ViewType.SEQUENCE_VIEW_TYPE);
      else
        this.commit("set_view_type", ViewType.MAIN_VIEW_TYPE);
    }
    state.align_sequence    = data.align_sequence.map(x => data.types.find(y => y.id == x).name);
    state.align_sequence_by = data.align_sequence_by;

    state.full_tree = data;
    state.attributes.prune_threshold.max = Math.round(0.01*data.total_count);
  },
  set_view_type(state, view_type){
    if(view_type < 0 || view_type >= ViewType.END_VIEW_TYPE)
      return;
    state.view_type = view_type;
    this.commit('set_display');
  },
  set_sequence_view_curtree_to_display(state, treeToDisplay) {
    state.sequence_view_curtree_to_display = treeToDisplay;
  },
  set_current_sequence(state, sequence){
    state.current_sequence = sequence;
  },
  set_display(state) {
    let mainHeight = window.innerHeight - (state.display.banner.height + state.display.footer.height + state.display.zooms.height)
    let mainView   = document.getElementById("mainview");
    let mainWidth  = (mainView ? mainView.clientWidth : 0);
    state.display.main.height = mainHeight;
    state.display.main.width  = mainWidth;
    state.display.main.innerHeight = Math.max(mainHeight - (state.display.main.padding.top + state.display.main.padding.bottom), 0);
    state.display.main.innerWidth  = Math.max(mainWidth - (state.display.main.padding.left + state.display.main.padding.right), 0);
    state.display.side.width  = document.getElementById("sideview").clientWidth;
    state.display.side.height = state.display.main.innerHeight + state.display.zooms.height;
    state.display_flag = Math.random()
  },
  set_current_tree(state) {
    //------------------------------------------------------------------------------
    //Apply some filtering and post-processing computation to be able to draw the graph.
    //Those computation could be transferred to the server to earn time
    //------------------------------------------------------------------------------
    
    if(state.full_tree == null) { //Something weird with the tree
      state.current_tree = state.full_tree;
      return;
    }

    if(state.full_tree.reset) {
      state.current_tree = null;
    }

    // sort the tree
    // Apply an hysteresis if needed
    const filter_rec = function(indata, old_tree, cum_duration) {
        let ret = (indata.name == "dual_sequence" ? {...indata} : hysteresisSortAsc(old_tree, {...indata}));//{...indata, children: indata.children.sort((a, b) => a.count - b.count)});

        ret.cum_duration = cum_duration+ret.duration;
        ret.children = ret.children.map(child => {
          let cur_old_tree = null;
          if(old_tree != null) {
            for(let i = 0; i < old_tree.children.length; i++) {
              if(old_tree.children[i].id == child.id) {
                cur_old_tree = old_tree.children[i];
                break;
              }
            }
          }
          return filter_rec(child, cur_old_tree, ret.cum_duration);
      });
      return ret;
    };
    let filtered = filter_rec(state.full_tree, state.current_tree, 0);

    //Compute count_start for all nodes
    const transformTree = function (indata, oldstart) {
      return indata.map(child => { 
        let children     = child.children ? transformTree(child.children, oldstart):null; 
        let max_duration = child.duration + (children != null && children.length > 0 ? Math.max(...children.map(c => c.max_duration)) : 0);
        let newchild = {
          ...child, 
          children:     children,
          count_start:  oldstart, 
          max_duration: max_duration,
        }; 
        oldstart += child.count; 
        return newchild; 
      });
    };
    let children = transformTree(filtered.children, 0);
    state.current_tree = {
      ...filtered, 
      children: children, 
      count_start: 0, 
      max_duration: (children && children.length > 0 ? Math.max(...children.map(c => c.max_duration)): 0)
    };
  },
  send_message(state, req) {
    let event_id = state.unique_event_id++
    req.data.event_id = event_id
    state.ws.send(JSON.stringify(req.data))
  },
  set_distribution(state, data) {
    state.distribution = data
  },
  set_ws(state, ws) {
    state.ws = ws
  },
  set_tab(state, tab) {
    if(Object.values(TabView).find((t) => t == tab) == undefined)
      return;
    state.tab = tab
  },
  set_type_hierarchy(state, hierarchy) {
    state.type_hierarchy = hierarchy;
  },
};

export const actions = {
  initApp({ state, commit, dispatch }) {
    commit('set_display')
    let ws = new WebSocket(state.server_url);
    commit('set_ws', ws)
    ws.onmessage = (event) => {
      let resp = JSON.parse(event.data)
      dispatch('recv_message', resp)
    }
    ws.onopen = () => {
      // load data at start
      dispatch('get_node_tree', {reset: true})
    }
    
    window.addEventListener('resize', throttle(() => commit('set_display'), 500))

  },
  async set_hidden_items({state, dispatch}, value) {
    state.filter.hidden_items = value;
    dispatch('update_node_tree', {filter: state.filter, reset: false});
  },
  set_filter({state, commit}, { filter, saveToHistory }) {
    state.filter = {
      duration: filter.duration,
      age: filter.age,
      no_treatment_duration: filter.no_treatment_duration,
      interruption_duration: filter.interruption_duration,
      depth: filter.depth,
      prune_threshold: filter.prune_threshold,
      diseases: filter.diseases,
      aggregate_coarse_level: filter.aggregate_coarse_level,
      hidden_items: filter.hidden_items,
    }

    if(saveToHistory)
      commit('insert_history_node', new HistoryNode("filter", state.filter, "filter"));
  },
  set_main_view_zoomed_node_id({state, commit}, { id, saveToHistory = true} ){
    if(state.main_view_zoomed_node_id != id) {
      state.main_view_zoomed_node_id = id;
      if(saveToHistory)
        commit('insert_history_node', new HistoryNode("zoom_main_view", id, `zoom in`));
    }
  },
  set_sequence_view_zoomed_nodes_ids({state, commit}, { ids, saveToHistory = true} ){
    if(state.sequence_view_zoomed_nodes_ids[0] != ids[0] || 
       state.sequence_view_zoomed_nodes_ids[1] != ids[1]) {
      state.sequence_view_zoomed_nodes_ids = ids;
      if(saveToHistory)
        commit('insert_history_node', new HistoryNode("zoom_sequence_view", ids, `zoom in`));
    }
  },
  on_align_sequence({commit, dispatch}, { sequence, nOccurence, saveToHistory=true } ) {
    dispatch('set_sequence_view_zoomed_nodes_ids', {ids: [-1, -1], saveToHistory: false});
    if(saveToHistory)
      commit('insert_history_node', new HistoryNode("align_sequence", {sequence: sequence.map(s => s), nOccurence: nOccurence}, `align_sequence ${sequence}`));
  },
  on_history({state, dispatch}, node) {
    let toReset = false;
    state.history.replayHistory((node) => {
      if(node == null) {
        let defaultAlign = defaultAlignedSequence();
        dispatch("set_align_sequence", { sequence: defaultAlign.sequence, nOccurence: defaultAlign.nOccurence, saveToHistory: false, reset:false });
        dispatch("update_node_tree", { filter: defaultFilter(), saveToHistory: false, reset: false } );
        dispatch("set_main_view_zoomed_node_id", { id: defaultZoomInMainView(), saveToHistory: false } );
        dispatch("set_sequence_view_zoomed_nodes_ids", { ids: defaultZoomInSequenceView(), saveToHistory: false } );
        state.sequence_view_zoomed_nodes_ids = defaultZoomInSequenceView();
        toReset = true;
      }
      else if(node.type == "filter") {
        dispatch("update_node_tree", { filter: node.data, saveToHistory: false, reset: false } );
        dispatch("set_main_view_zoomed_node_id", { id: defaultZoomInMainView(), saveToHistory: false } );
        dispatch("set_sequence_view_zoomed_nodes_ids", { ids: defaultZoomInSequenceView(), saveToHistory: false } );
        toReset = true;
      }

      else if(node.type == "align_sequence") {
        dispatch("set_align_sequence", { sequence: node.data.sequence, nOccurence: node.data.nOccurence, saveToHistory: false, reset:false });
        toReset = true;
      }

      else if(node.type == "zoom_main_view") {
        dispatch("set_main_view_zoomed_node_id", { id: node.data, saveToHistory: false } );
      }

      else if(node.type == "zoom_sequence_view") {
        dispatch("set_sequence_view_zoomed_nodes_ids", { ids: node.data, saveToHistory: false } );
      }
    }, node);

    if(toReset && state.history_reset) { //We only want to apply ONCE the last filter. 
      state.history.disableSaving = true;
      //Save the main_view_zoom_id that will be reset by get_node_tree...
      let main_view_zoomed_node_id       = state.main_view_zoomed_node_id;
      let sequence_view_zoomed_nodes_ids = state.sequence_view_zoomed_nodes_ids;
      dispatch("get_node_tree", {reset: true});
      dispatch("set_main_view_zoomed_node_id", { id: main_view_zoomed_node_id, saveToHistory: false } );
      dispatch("set_sequence_view_zoomed_nodes_ids", { ids: sequence_view_zoomed_nodes_ids, saveToHistory: false } );
      state.history.disableSaving = false;
    }

    if(state.comparison_mode != ComparisonMode.NONE_COMPARISON_MODE)
      state.comparison_nodes[state.comparison_mode] = node;
  },
  async start_new_comparison({state}, historyNode) {
    state.comparison_nodes[ComparisonMode.SOURCE_COMPARISON_MODE] = state.history.curNode;
    state.comparison_nodes[ComparisonMode.TARGET_COMPARISON_MODE] = historyNode;
    state.comparison_mode = ComparisonMode.TARGET_COMPARISON_MODE;
    
    let event_id = state.unique_event_id++

    state.ws.send(JSON.stringify({
      action: 'comparison',
      event_id,
      params: {
        stop: true,
        ctx_index: ComparisonMode.TARGET_COMPARISON_MODE
      }
    }));

    //Do it in two-steps to force the jump (just in case of issues...)
    state.history.jumpTo(state.history.tree);
    state.history.jumpTo(historyNode);
  },
  async set_comparison_mode({state, dispatch}, mode) {
    let historyNode = null;
    if(state.comparison_mode != ComparisonMode.NONE_COMPARISON_MODE && mode == ComparisonMode.NONE_COMPARISON_MODE) {
      let event_id = state.unique_event_id++
      state.ws.send(JSON.stringify({
        action: 'comparison',
        event_id,
        params: {
          stop: true,
          ctx_index: ComparisonMode.SOURCE_COMPARISON_MODE
        }
      }));
      state.comparison_mode = mode;
      historyNode = state.comparison_nodes[ComparisonMode.SOURCE_COMPARISON_MODE];
      state.comparison_nodes[ComparisonMode.TARGET_COMPARISON_MODE] = null;
      dispatch('get_node_tree',  {reset: false, process: true} )
    }
    else if(mode == ComparisonMode.SOURCE_COMPARISON_MODE || mode == ComparisonMode.TARGET_COMPARISON_MODE){
      let event_id = state.unique_event_id++
      state.ws.send(JSON.stringify({
        action: 'comparison',
        event_id,
        params: {
          stop: false,
          ctx_index: mode
        }
      }));
      historyNode = state.comparison_nodes[mode];
      state.comparison_mode = mode;

      dispatch('get_node_tree',  {reset: false, process: true} )
    }
    state.distribution = defaultDistribution();
    if(historyNode != null){
      //For comparison modes, we want to jump to a node, however, we do not want to relaunch the whole progressive algorithm. 
      //Putting history_reset to false will prevent to reset the state of the server
      state.history_reset = false;
      state.history.jumpTo(historyNode);
      state.history_reset = true;
    }
  },
  async update_node_tree({ dispatch }, { filter, saveToHistory=true, reset=true }){
    dispatch('set_filter', { filter, saveToHistory } );
    dispatch('get_node_tree', {reset: reset, process: true});
  },
  //Received messages
  async recv_message({ state, commit, dispatch }, resp) {
    if (resp.type == "nodetree") {
      commit('set_tree', resp.data)
      commit('set_current_tree')

      if(!resp.data.done)  //Progressive: Ask for the next iteration if there are still data to get
        dispatch('get_node_tree',  {reset: false, process: true} )

      if(state.distribution.age.bins != undefined)
        dispatch('get_distribution', state.last_distribution_params);
    } 
    else if (resp.type == "distribution") {
      commit('set_distribution', resp.data)
    }
  },

  //Messages to send
  async get_node_tree({ state, dispatch, commit}, { reset, process= true }) {
    if(reset) { //Ask for a reset --> no more in 'progressive', reset the zooming
      commit('set_distribution',                     defaultDistribution());
      dispatch('set_main_view_zoomed_node_id',       {id: -1, saveToHistory: false});
      dispatch('set_sequence_view_zoomed_nodes_ids', {ids: [-1, -1], saveToHistory: false});
    }
    let event_id = state.unique_event_id++
    state.ws.send(JSON.stringify({
      params: {...state.filter,
        hidden_items: state.filter.hidden_items.map(x => state.full_tree.types.find(y => y.name == x).id), 
      },
      reset,
      process,
      event_id,
      action: 'node_tree'
    }))
    //reset == false
  },
  async get_distribution({ state }, params) {
    state.last_distribution_params = params;
    let event_id = state.unique_event_id++
    params.event_id = event_id
    state.requested = {
      color: params.color
    }
    state.ws.send(JSON.stringify(params))
  },
  async set_align_sequence({ state, commit, dispatch }, { sequence, nOccurence, saveToHistory=true, reset=true } ) {
    if(reset) {
      commit("set_distribution", defaultDistribution());
    }
    dispatch('on_align_sequence', { sequence, nOccurence, saveToHistory } );
    let event_id = state.unique_event_id++;
    
    state.ws.send(JSON.stringify({
      params: { 
        align_sequence   : (sequence == null ? [] : sequence.map(x => state.full_tree.types.find(y => y.name == x).id)), 
        align_sequence_by: nOccurence,
      },
      reset: reset,
      process: reset,
      event_id,
      action: 'node_tree'
    }))
  }
};


/** \brief  This store is the main store that all VueJS components have access to. This object stores the global state of the application. */
class Store extends Vuex.Store
{
  constructor() {
    super({ state, getters, mutations, actions });

    let _this = this;

    //Listen for all changes in the history.
    this.state.history_listener = {
      onUndo(newNode, _oldNode) {
        _this.dispatch("on_history", newNode);
      },

      onRedo(newNode, _oldNode) {
        _this.dispatch("on_history", newNode);
      },

      onJumpNode(newNode, _oldNode) {
        _this.dispatch("on_history", newNode);
      }
    };

    this.commit("set_history", this.state.history);
  }
}

let store = new Store();

/**
 * Utility function used to limit the frequency of calls to a function when listening to an event fired continuously
 */
//https://codeburst.io/throttling-and-debouncing-in-javascript-b01cad5c8edf
const throttle = (func, limit) => {
  let lastFunc
  let lastRan
  return function() {
      const context = this
      const args = arguments
      if (!lastRan) {
          func.apply(context, args)
          lastRan = Date.now()
      } else {
          clearTimeout(lastFunc)
          lastFunc = setTimeout(function() {
              if ((Date.now() - lastRan) >= limit) {
                  func.apply(context, args)
                  lastRan = Date.now()
              }
          }, limit - (Date.now() - lastRan))
      }
  }
}

export default store
