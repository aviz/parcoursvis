import * as d3 from "d3"

/** \brief  Reinit a tooldiv <div> object
  * \param tooltip the tooltip DOM element */
let reinit = function(tooltip)
{
  tooltip.attr("class", "tooltip")         
         .style("opacity", 0)
         .style("display", "none");
};

/** \brief  Function to call when the tooltip should appear (e.g., when the mouse hovers another DOM element)
  * \param tooltip the tooltip DOM element to manipulate
  * \param event the mouse event
  * \param str the string to display in the tooltip. Only one string is for the moment accepted */
let mouseover = function(tooltip, event, str)
{
  tooltip.transition()        
         .duration(200)      
         .style("opacity", .9)
         .on('start', function(){d3.select(this).style("display", "block");});

  tooltip.html(str)
      .style("left", (event.clientX+5) + "px")     
      .style("top", (event.clientY-5) + "px");
};

/** \brief  Function to call when the tooltip should disappear (e.g., when the mouse exits another DOM element)
  * \param tooltip the tooltip DOM element to manipulate.*/
let mouseout = function(tooltip)
{
  tooltip.transition()
         .duration(500)
         .style("opacity", 0)
         .on('end', function(){d3.select(this).style("display", "none");});
};

export { reinit, mouseover, mouseout };
