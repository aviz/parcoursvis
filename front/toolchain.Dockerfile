FROM node

COPY package.json  /

# Install app dependencies
RUN npm install

ENV NODE_OPTIONS --openssl-legacy-provider

# Copy in /mnt, but use the node_modules in /
RUN npm config set prefix /
WORKDIR /src

CMD ["npm", "run", "build"]
