# Build and run the Frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

We rely on jest for the unit tests (present in src/tests/unit). The coverage analyses is available in the generated folder "coverage/lcov-report/index.html"

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# The frontend toolchain


Because of some constraints, it may be necessary to export all the compilation toolchains into a docker image, and to use this docker image to build the frontend files to distribute via an HTTP server (e.g., http-server package that npm provides)
toolchain.Dockerfile allows to create that docker image. This docker image is to be built each time to "package.json" is modified.

For the document image to work, the user needs to mount this directory (front) to target the /src directory of the docker container:

```
docker build -f toolchain.Dockerfile -t parcoursvis-toolchain-front .   #Build the docker image, supposing the user is in the front directory
docker run -v <ABSOLUTE_PATH_TO_FRONT>:/src parcoursvis-toolchain-front #Creates a docker container and starts the compilation. This command mounts the front directory to target /src on the docker container
```

If the build succeeded, a "dist" folder should be created with all the HTML/CSS/Js files to be exported via an HTTP server.
