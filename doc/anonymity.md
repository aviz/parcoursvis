# ParcoursVis Data Structures and Algorithms for Anonymity

This document explains how we enforce anonymity in the aggregation tree produced by the `ParcoursVis` system.
ParcoursVis builds a prefix tree with weights from a list of Electronic Health Records (EHRs).

The Records are read from a csv file and have the following structure (in their simpler form, more later):

```CSV
patient_id,age,event_type,date,drug
0,71,fivealpha,2015-05-29,dutastéride
0,71,fivealpha,2015-06-05,dutastéride
0,71,fivealpha,2015-06-12,dutastéride
1,50,alphabloc,2003-09-26,alfuzosine
1,50,alphabloc,2003-10-03,alfuzosine
1,50,alphabloc,2003-10-10,alfuzosine
1,50,alphabloc,2003-10-17,alfuzosine
2,53,phyto,2012-11-20,tadenan
2,53,phyto,2012-11-27,tadenan
2,53,phyto,2012-12-04,tadenan
3,81,phyto,2010-07-15,palmier de floride biogaran
3,81,phyto,2010-07-22,palmier de floride biogaran
35,74,phyto,2013-01-27,prodinan
35,74,phyto,2013-02-03,prodinan
35,74,phyto,2013-02-10,prodinan
35,74,phyto,2013-02-17,prodinan
35,74,alphabloc,2013-03-24,xatral
35,74,alphabloc,2013-03-31,xatral
35,74,alphabloc,2013-04-07,xatral
35,74,alphabloc,2013-04-14,xatral
```


Each patient is a man treated for non-cancerous prostate. Each line reports an event collected from the main SNDS French database related to one patient and one drug or treatment (in the example, we use synthetic data).
Here, patient #0 is 71 years old, has bought, on the 29th of May 2015, a box of a drug called "Dutastéride" that contains the molécule "fivealpha" (5-alpha-reductase).
Thus, the example dataset contains three events for patient #0, four for #1, etc.
When applied to real data, the CSV file is extracted from the SNDS database, and the patient order is randomly shuffled to strengthen the pseudo-anonymization and evenly distribute the statistical properties of the patients and factors.

In addition, each record can have a list of associated diseases or conditions that the patient currently has. These diseases are important to find out if they interfere with the treatment outcome. For the same patient, the associated diseases can change from one record to the next, although they tend to remain stable.

```csv
patient_id,age,event_type,date,drug,diseases
0,71,fivealpha,2015-05-29,dutastéride,diabete hypertension
```

# Creation of the Prefix Tree

The prefix tree starts with a root node and the creation algorithm adds new nodes down the tree. Each node contains (refer to) all the patients with the same treatment sequence; they are merged. Furthermore, when a patient takes the same drug consecutively, all the contiguous events are merged into one event that converts the date to a duration.

In our example, patients #2 and #35 both start with the same molecule, "phyto" (phytotherapy). All the records of patient #2 become one event with a duration from 2012-11-20 to 2012-12-04 (14 days). All the events from patient #35 become one event with a duration from 2013-01-27 to 2013-02-17 (52 days). These two patients are merged into one tree node labeled "phyto".  The events of patient #3 are also merged in the "phyto" node, with a different duration from 2010-07-15 to 2010-07-22 (15 days).

The root node will have three children labeled "fivealpha", "alphabloc", and "phyto". All the patients except #35 are summarized (aggregated) by the three initial nodes.

Patient #35 changes his treatment to "alphabloc" (Alpha blockers) after 52 days of phytotherapy. In our prefix tree, we create an "alphabloc" child to the "phyto" node containing patient #35, a duration of 21 days (2013-03-24 to 2013-04-14), and an age of 74 years.

This sequence of EHRs is compressed into four nodes, three under the root and one under the "phyto" node.

Each node represents one aggregated event but usually refers to many patients. Additionally, a node maintains the count of patients, their age distribution, the treatment duration distribution, and the distribution of diseases or conditions associated with the main pathology that could interfere with the treatments.  In our case, a `Disease` is simply a category, a string that can be encoded as an integer for optimization purposes.

```cpp
struct Node {
  unsigned count;
  EventType name;
  List<Node*> children;
  List<int> ages;
  List<int> durations;
  List<Disease> diseases;
};
```

## Simplification of the Prefix Tree
Furthermore, when visualized or used to generate synthetic data, the node structure is simplified as:
```cpp
struct Node {
  unsigned count;
  EventType name;
  List<Node*> children;
  Histogram<32,0,120> age_histogram;
  Histogram<32,0,1500> duration_histogram;
  map<Disease, unsigned> disease_count;
};
```
where a `Histogram` contains a distribution of values made of `bins` counting the number of values falling between value ranges:
```cpp
template
struct Histogram<int BINS, int MIN, int MAX> {
  unsigned bins[BINS];
  unsigned count = 0;

  Histogram(List<int> data) {
    int range = MAX - MIN;
    for (auto x : data) {
      if (x < MIN || x > MAX) continue; // ignore out of range data
      unsigned index = (x == MAX) ? BINS-1 : (x - MIN) * BINS / range;
      count++;
      bins[index]++;
    }
  }
}

```

A `DiseaseCount` is simply a count for each disease in a node: `map<Disease, unsigned>`.

In the end, the tree starts from a root node and aggregates all the existing sequence prefixes.  This tree can be saved in JSON format for archival or reuse to generate synthetic sequences.

A generated JSon tree looks like this:
```json
{
  "id": 0,
  "count": 1999972,
  "name": "_root",
  "duration": 0,
  "children": [
    {
      "id": 5,
      "count": 124726,
      "name": "fivealpha",
      "duration": 92,
      "children": [
        {
          "id": 211,
          "count": 95,
          "name": "alphabloc",
          "duration": 62,
          "children": [],
          "count_stopped": 95,
          "age_histogram": {
            "range": [0, 128],
            "bins": [
               0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  3, 12, 13,
              16,  7, 15, 12,  5,  4,  2,  2,  1,  0,  0,  0,  0,  0,  0,  0
            ],
            "median": 68,
            "mean": 68
          },
          "duration_histogram": {
            "range": [0, 2048],
            "bins": [
              66, 13, 12,  2,  2,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
               0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
            ],
            "median": 46,
            "mean": 62
          },
          "disease_barchart": [
            {"name": "Diabete"     , "value": 19},
            {"name": "Hypertension", "value": 18}
          ]
        },
        ...
      ]
    }
  ]
}
```

# Synthetic Data

To generate synthetic event sequences, we start from a real prefix tree. We first process it to comply with anonymization rules and then create synthetic patients from the anonymized tree. We will present the anonymization rules after the generation process to highlight the potential issues of generating synthetic patients.

## Generation of Synthetic Patients

For generating a new synthetic patient, we create a random path in the tree and generate EHR records accordingly.
A random path is created by picking a random child in a node according to the frequency of the children. This frequency is computed by looking at the `count` fields of the children nodes and the parent node. If the parent has a count of 100, and three children have 25 each, it means that, among the 100 patients that reached the parent node, three quarter get distributed evenly to the children, and one-quarter stopped their sequence at the parent node (the sum of children count is higher or equal the parent count).


The code looks like this:
```python
def generate_random_path(node: JSon) -> List[Dict[str, Any]]:
    events = []
    while True:
        count = node.get("count", 0)
        if count == 0:
            break
        children = node.get("children", None)
        if children is None:
            break
        weights = [child.get("count", 0) for child in children]
        cum_weights = list(accumulate(weights))
        cum_weights.append(count)
        rand = random_weighted_choice(cum_weights)
        selected_child = children[rand]
        event = {"name": selected_child["name"], "node": selected_child}
        events.append(event)
    return events
```

This function generates a random path of events from a JSON-encoded aggregated tree, each event linked to its related node in the tree. These nodes are used to generate the age and duration attributes.

The age and durations associated with the random path are generated randomly from the ages and durations histograms in the nodes.  Note that the age is taken from the distribution of the last node of its sequence, generated from the age distribution of that node, which is the most accurate for the synthetic patient finishing his sequence at that node.  Also, the list of diseases associated with each event is only generated from the last node. It could change over time, and in reality, it does, but we do not manage these changes currently.

```python
def generate_patient(node: JSon) -> List[Dict[str, Any]]:
    events = generate_random_path(node)
    last_event = events[-1]
    last_age = random_weighted_measure(last_event["node"].ages)
    diseases = last_event["node"].get("diseases", {})
    last_diseases = Set()
    count = last_event["node"].get("count")

    for disease_name, disease_count in diseases.items():
        rand_count = random.randrange(count)
        if (rand_count < disease_count):
            last_diseases.add(disease_name)

    for event in reversed(events):
        duration = weighted_measure(event["node"].get("duration_histogram"))
        event["duration"] = duration
        category = event["category"]
        if category == "no treatment":
            event["duration"] = max(duration, 360)
        elif category == "interruption":
            event["duration"] = max(duration, 180)
        event["age"] = last_age
        last_age -= duration / 365.25
        event["diseases"] = last_diseases

    rand_date  = random.uniform(MIN_DATE.toordinal(), MAX_DATE.toordinal())
    start_date = date.fromordinal(int(rand_date))
    for event in events:
        event["date"] = start_date
        if "duration" in event:
            start_date += timedelta(days=event["duration"])
    return events
```

Once a synthetic (random) patient sequence has been generated, it can be written in a CSV file with a format identical to the one shown at the beginning of this document. The process is a bit more involved since, for drugs taken with a given duration, a series of drug events should be generated with a sequence of realistic dates, given the number of treatment days offered by a box of a given drug. We skip this stage of the generation since it is irrelevant to the anonymization process.


## Anonymization of the Aggregated Tree

We want to export the aggregated tree to generate synthetic patients according to the previous algorithm. We can generate as many as needed to benchmark ParcoursVis and to provide test and demo datasets of varying sizes.  If the aggregated tree is anonymized, the synthetic patients generated cannot be used to recover information about real patients, yet the visualized data is meaningful, even if it has less information than the original one.

The rules and limits required by the Health Data Hub and the CNIL for anonymizing aggregated data are the following:
* k-anonymity: every node of the tree should contain at least k persons. In our case k = 10.
* l-diversity: If S is a sensitive attribute, a node is l-diverse if it contains l "well represented" values for S. In our case l = 2.
* r-representativity: If S is a sensitive attribute, a node should not contain more than r = 80 % of its members
* s-threshold: If S is a sensitive attribute, a node should not mention less than s representative values.

The sensitive attributes are:
* category
* age
* each disease

For our treatments, the "duration" attribute is not sensitive.

To comply with the rules, we could either filter out nodes that do not pass one of the rules or perturbate the data to eliminate the problematic issues. If we filter the nodes, our final tree is almost empty; therefore, we perturbate the attributes instead.

### k-anonymity

An anonymized tree contains nodes with at least k=50 patients. Since we are dealing with millions of patients, we can set k higher than 10 and still have enough large nodes in the end.

### s-threshold

For our node, this rule does not concern the number of children that k-anonymity solves, but the distributions. The age distribution is usually mostly Gaussian and some very young or more likely very old patients will create bins with less than 3 individuals. The same is true for associated diseases; some relatively rare diseases will appear for less than 3 individuals.
For the age bins, we redistribute the low value bins with a count less than 3 to higher value bins, and for high value bin with a count less than 3, we redistribute the counts to lower value bins.
For rare diseases, we set them to 0.
