#!/usr/bin/bash
 
#Change the paths of ParcoursVis and ParcoursUrge
PARCOURSVIS=/home/mickael/Programmation/Web/parcoursvis
PARCOURSURGE=/home/mickael/Programmation/Web/parcoursURGE

#Should we show the differences of files in addition to show which files are different?
SHOWDIFF=false

#Bash option to allow recursive glob star (**/*)
shopt -s globstar

compare_files() {
    if [ -d $1 ]
    then
        return
    fi

    SUBPATH=${1#$PARCOURSVIS}

    if $SHOWDIFF
    then
        cmp -s $1 $2 || echo "files at $SUBPATH are different" && diff $1 $2
    else
        cmp -s $1 $2 || echo "files at $SUBPATH are different"
    fi
}

echo "checking the backend files..."
for i in "back/setup.py" "back/parser/index.py" "back/parser/ParcoursvisClient.py"
do
    compare_files $PARCOURSVIS/$i $PARCOURSURGE/$i 
done

for i in $PARCOURSVIS/back/parser/src/*
do
    SUBPATH=${i#$PARCOURSVIS}
    compare_files $i $PARCOURSURGE/$SUBPATH
done

echo "checking the frontend files..."
for i in $PARCOURSVIS/front/src/**/*
do
    SUBPATH=${i#$PARCOURSVIS}
    TESTSUBPATH=/front/src/tests
    if [[ ${SUBPATH##$TESTSUBPATH} == $SUBPATH ]]
    then
        compare_files $i $PARCOURSURGE/$SUBPATH
    fi
done

echo "checking the unit tests of the backend..."
for i in $PARCOURSVIS/back/parser/src/tests/**/*
do
    SUBPATH=${i#$PARCOURSVIS}
    compare_files $i $PARCOURSURGE/$SUBPATH
done

echo "checking the unit tests of the frontend..."
for i in $PARCOURSVIS/front/src/tests/**/*
do
    SUBPATH=${i#$PARCOURSVIS}
    compare_files $i $PARCOURSURGE/$SUBPATH
done
