import json
import logging

import parcoursprog

from enum import Enum

class ContextIndex(Enum):
    MAIN_CONTEXT       = 0
    COMPARISON_CONTEXT = 1

class ParcoursvisClient:
    class Listener:
        def onChangeCurrentContext(self, client):
            pass

    def __init__(self, dataPath):
        self._dataPath  = dataPath
        self._contexts  = [parcoursprog.context(), None]
        self._listeners = []

        self._contexts[ContextIndex.MAIN_CONTEXT.value].load(dataPath)
        self._currentContext = self._contexts[ContextIndex.MAIN_CONTEXT.value]

    def _setCurrentContext(self, context):
        self._currentContext = context;
        for i in self._listeners:
            i.onChangeCurrentContext(context)

    def addListener(self, listener):
        if not listener in self._listeners:
            self._listeners.append(listener);

    def removeListener(self, listener):
        try:
            self._listeners.remove(listener)
        except ValueError:
            pass

    def startComparison(self):
        self._setCurrentContext(self._contexts[ContextIndex.MAIN_CONTEXT.value])
        self._contexts[ContextIndex.COMPARISON_CONTEXT.value] = parcoursprog.context()
        self._contexts[ContextIndex.COMPARISON_CONTEXT.value].load(self._dataPath)

    def stopComparison(self):
        self._currentContext = self._contexts[ContextIndex.MAIN_CONTEXT.value]
        self._contexts[ContextIndex.COMPARISON_CONTEXT.value] = None

    contexts          = property(lambda self : self._contexts)
    currentContext    = property(lambda self : self._currentContext, lambda self, value: self._setCurrentContext(value))
    mainContext       = property(lambda self : self._contexts[ContextIndex.MAIN_CONTEXT.value])
    comparisonContext = property(lambda self : self._contexts[ContextIndex.COMPARISON_CONTEXT.value])
    
