cmake_minimum_required(VERSION 3.14)
project(ProgressiveTests)

set(CMAKE_CXX_STANDARD 14)

SET(WARNING_FLAGS ""    CACHE STRING "The warning flags")
set(RELEASE       FALSE CACHE BOOL   "Compiling in release mode.")

if(NOT WARNING_FLAGS)
    set(WARNING_FLAGS "-Wall -Wshadow -Wno-uninitialized -Wno-strict-aliasing -Wformat -Wformat-security -Wmissing-noreturn -Winit-self  -Wpointer-arith -Wno-missing-field-initializers")
endif()

#Debug/Release version
if(RELEASE)
	MESSAGE(STATUS "Compiling in release mode")
	set(CMAKE_BUILD_TYPE "Release")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3")
else()
	MESSAGE(STATUS "Compiling in Debug mode")
	set(CMAKE_BUILD_TYPE "Debug")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O0")
endif()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DNO_PYTHON ${WARNING_FLAGS} --coverage")
set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -DNO_PYTHON ${WARNING_FLAGS} --coverage")

include_directories(${CMAKE_SOURCE_DIR}/src/)

find_package(GTest REQUIRED)
find_package(PkgConfig REQUIRED)
find_package(OpenMP REQUIRED)
pkg_check_modules(JSONCPP jsoncpp)

include_directories(${GTEST_INCLUDE_DIR})

# Add test cpp file
add_executable(pparserUnitTests
    src/tests/testbarchart.cpp
    src/tests/testcontext.cpp
    src/tests/testhistogram.cpp
    src/tests/testnode.cpp
    src/tests/testvalues.cpp
    src/tests/testgraph.cpp
    src/tests/testTypeHierarchy.cpp
    src/pparser.cpp
    src/mmap_mem.cpp
    src/CSVParser.cpp
    src/roaring.c
)

target_compile_options(pparserUnitTests PUBLIC ${JSONCPP_CFLAGS})
target_link_libraries(pparserUnitTests PUBLIC ${GTEST_LIBRARIES} -lpthread ${JSONCPP_LDFLAGS} OpenMP::OpenMP_CXX)
