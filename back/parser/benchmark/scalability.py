from cycler import cycler
import sys
import os
import json
import math
import scipy
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.ticker as ticker
import functools

def printDocAndExit():
    """
    Print the documentation of how to use the script and exit the program.
    """
    print("Run ./scalability.py inFile1 [...inFiles] [--output directoryPath]")
    print("                     [-h] [--help]")
    print("inFiles: the json files to analyse from the scalability point of view. See benchmark.py outputs.")
    print("--output: output directory to save the different PDF data analyses results. Default: ${pwd}/output/")
    print("-h --help: Print this documentation and exit.")

    sys.exit(-1)

def printTime(datas, outputDir):
    """
    Save the time performance in outputDir/scalabilityTime.pdf
    This computes the 95% confidence interval linechart comparing CPU times (y-axis) and the number of low-level events (x-axis) for the progressive algorithm (depends on chunkSize)
    and the CPU times (y-axis) and the number of low-level events (x-axis) without any progressive features
    We pick "only" the times where threshold == 0

    @datas: The list of the benchmark's results.
    @outputDir: The output directory to save scalabilityTime.pdf in.
    """

    columnsProg    = ['CPUTime', 'chunkSize', 'nbEvents']
    columnsNonProg = ['CPUTime', 'nbEvents']
    timeProg       = pd.DataFrame(columns=columnsProg)
    timeNonProg    = pd.DataFrame(columns=columnsNonProg)

    #Gather the values we are interested in
    for data in datas:
        prog  = [x for x in data["Progressive"] if len(x['alignSequence']) == 0]
        nonProgTime = np.mean([x['time'] for x in data["NonProgressive"] if len(x['alignSequence']) == 0])
        nbLowLevel = data["NonProgressive"][0]["nbLowLevel"]
        for x in prog:
            timeProg = pd.concat([timeProg, pd.DataFrame([[x['totalTime'], x['chunk_size'], nbLowLevel]], columns=columnsProg)], ignore_index=True)
        timeNonProg = pd.concat([timeNonProg, pd.DataFrame([[nonProgTime, nbLowLevel]], columns=columnsNonProg)], ignore_index=True)

    timeProg  = timeProg.groupby(['chunkSize', 'nbEvents'], as_index=False).mean()
    uniqueChunkSize = pd.unique(timeProg["chunkSize"])

    outputFile = f"{outputDir}/scalabilityTime.pdf"
    print(f"Saving {outputFile}...\n")

    fig = plt.figure()
    fig.set_tight_layout(True)
    fig.set_size_inches(10, 4)
    ax1 = fig.add_subplot(aspect="auto")
    ax1.set(axisbelow=True,
            title='',#Total computation time per chunk size compared to the size of the datasets',
            xlabel='Number of low-level events',
            ylabel='Time (s)',
    )
    ax1.set_ylim(ymin=0, ymax=functools.reduce(lambda x, y: max(x, y), timeProg["CPUTime"]/1000.0)+0.25)
    ax1.yaxis.set_major_locator(ticker.MultipleLocator(0.25))
    ax1.set_xticks([int(x) for x in pd.unique(timeProg["nbEvents"])])
    ax1.set_xticklabels([f"{x:,}" for x in pd.unique(timeProg["nbEvents"])])
    for chunkSize in uniqueChunkSize:
        subData = timeProg[timeProg["chunkSize"] == chunkSize].sort_values("nbEvents")
        x = subData["nbEvents"]
        y = subData["CPUTime"]/1000
        ax1.plot(x, y, '-o', label=f"chunkSize = {chunkSize:,}")

    subData = timeNonProg.sort_values("nbEvents")
    ax1.plot(subData["nbEvents"], subData["CPUTime"]/1000, '-o', label=f"chunkSize = $\infty$")
    ax1.legend(loc='upper left')
    ax1.grid()
    fig.savefig(outputFile, format="pdf", transparent=True)


# ------------------------------------------------------------------------------
# -----------------------------------The Main-----------------------------------
# ------------------------------------------------------------------------------
if __name__ == '__main__':
    argv      = sys.argv[1:]
    outputDir = os.getcwd() + "/output"
    inFiles   = []
    datas     = []

    #Read argv
    i = 0
    while i < len(argv):
        arg = argv[i]
        if not (arg.startswith("--") or arg.startswith("-")):
            inFiles.append(arg)
            i+=1
        else:
            while i < len(argv):
                arg = argv[i]

                if arg == "-h" or arg == "--help":
                    printDocAndExit()

                elif arg == "--output":
                    if i < len(argv)-1:
                        if argv[i+1][0] == '-':
                            print(f"Missing the directory path value of the '--output' parameter. {argv[i+1]} is not a correct value. Exiting.")
                            sys.exit(-1)
                        outputDir = argv[i+1]
                        i+=1
                    else:
                        print(f"Missing the directory path value of the '--output' parameter. Exiting.")
                        sys.exit(-1)
                else:
                    print(f"Unknown parameter {arg}")
                    printDocAndExit()
                i+=1

    #Check that the input file exists and read it
    for inFile in inFiles:
        assert os.path.isfile(inFile)
        with open(inFile, 'r') as f:
            datas.append(json.load(f))

    #Create the output directory, if necessary
    if not os.path.exists(outputDir):
        os.mkdir(outputDir)

    plt.rcParams['axes.prop_cycle'] = cycler(color=cm.Dark2.colors)
    printTime(datas, outputDir)
