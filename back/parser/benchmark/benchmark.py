import sys
import os
import json
import logging
import copy
import time
import csv
import pdb
from functools import *
from itertools import *
from operator  import itemgetter, attrgetter

import parcoursprog

logging.basicConfig()

#General parameters
percentageAccuracy         = 0.02
screenSize                 = int(1080)
hysteresisInertia          = 20.0/screenSize
percentageAccuracyInPixels = percentageAccuracy * screenSize
maxDepth                   = 0xffffffff

def getNodeDataForPrinting(nodes):
    """Return a list of dict containing ids and counts, as those are what we are interested in in that benchmark"""
    return [{"id": x['id'], "count": x['count']} for x in nodes]

def printDocAndExit():
    """
    Print the documentation of how to use the script and exit the program.
    """
    print("Run ./benchmark.py [data_directory] [--output JsonOutput] [--onlyTime]")
    print("                   [-h] [--help]")
    print("data_directory: the data directory containing the preparsed data binary files that parcoursvis will read. Default: ${pwd}/tmp")
    print("--output: output path to save the benchmark results in a JSON format. Default: output.json")
    print("--onlyTime: should we benchmark only the time (scalability) of ParcoursVis?")
    print("-h --help: Print this documentation and exit.")
    print("""\nNote that ParcoursVis relies on OpenMP. To influence the number of threads OpenMP relies on,
change the environment variables OMP_NUM_THREADS and OMP_DYNAMIC as required (see also their respective documentation)
          """)

    sys.exit(-1)


class BenchmarkNodeMetrics:
    """
    Stores the data about the benchmark per nodes
    The class stores a list of BenchmarkIterationMetrics.IterationData for subsequent data analysis
    """
    class NodeData:
        """
        Metrics store per each node
        """
        def __init__(self):
            self.path              = []
            self.id                = -1
            self.count             = -1
            self.stableAtIteration = -1

    def __init__(self):
        self._nodes = []

    def __iter__(self):
        """
        Iterate over the nodes stored as NodeData in this object
        """
        return self._nodes.__iter__()

    def __len__(self):
        """
        The number of nodes stored
        """
        return len(self._nodes)

    def __getitem__(self, x):
        """
        Get the iteration x. 
        return: NodeData
            the NodeData element stored at emplacement x
        """
        return self._nodes[x]

    def insertNode(self):
        """
        Insert a new NodeData at the end of the list of stored nodes
        return: NodeData
            The new node created and append in this class
        """
        self._nodes.append(BenchmarkNodeMetrics.NodeData())
        return self._nodes[-1]

class BenchmarkIterationMetrics:
    """
    Stores the data about the benchmark per depth
    The class stores a list of BenchmarkIterationMetrics.IterationData for subsequent data analysis
    """
    class IterationData:
        """
        Metrics store per each depth
        """
        def __init__(self):
            self.permutations  = 0
            self.badStatistics = 0
            self.missingNodes  = 0

        def isStable(self, checkTopology):
            if checkTopology:
                return self.stableWithTopology
            return self.stableWithoutTopology

        stableWithTopology    = property(lambda self: self.permutations == 0 and self.badStatistics == 0 and self.missingNodes == 0)
        stableWithoutTopology = property(lambda self: self.permutations == 0 and self.badStatistics == 0)

    def __init__(self):
        self._iterations     = []

    def __iter__(self):
        """
        Iterate over the iterations stored as IterationData in this object
        """
        return self._iterations.__iter__()

    def __len__(self):
        """
        The number of iterations stored
        """
        return len(self._iterations)

    def __getitem__(self, x):
        """
        Get the iteration x. 
        return: IterationData
            the IterationData element stored at emplacement x
        """
        return self._iterations[x]

    def insertIteration(self):
        """
        Insert a new IterationData at the end of the list of stored iterations
        return: IterationData
            The new iteration created and append in this class
        """
        self._iterations.append(BenchmarkIterationMetrics.IterationData())
        return self._iterations[-1]

    def getStableAt(self, checkTopology):
        """
        Get at what iteration is the generated tree stable
        checkTopology: boolean
            should we check the topology (i.e., the number of missing Nodes)?
        return: int
            the iteration ID of when the tree can be considered stable

            This function ensures that:
            for x in range(getStableAt(self), len(self)):
                assert self[x].stable
        """
        ret = 0
        for y,x in enumerate(self._iterations[::-1]):
            if not x.isStable(checkTopology):
                ret = len(self._iterations)-y
                break

        for x in range(ret, len(self)):
            assert self[x].isStable(checkTopology)

        return ret

    stableWithTopologyAt    = property(lambda self: self.getStableAt(True))
    stableWithoutTopologyAt = property(lambda self: self.getStableAt(False))

class ProgressiveStats:
    """
    Class to benchmark the progressive algorithm of parcoursvis
    """
    def __init__(self):
        """
        Constructor. Initialize component but does not read the data yet. 
        Use self.processData to process the data

        outputFile: str
            The path to output the result of the algorithm via the "print" function
        """
        self._distributions     = list()
        self._computationTimes  = list()
        self.ctx                = parcoursprog.context()
        self.percentageAccuracy = percentageAccuracy
        self.hysteresisInertia  = hysteresisInertia
        self.chunkSize          = int(1e5)
        self.threshold          = 0

        self.alignSequence      = []
        self.alignSequenceBy    = 0

    def _areFloatsAlmostSimilar(self, f1, f2):
        """
        Are two float values similar at "self.percentageAccuracy"?
        f1, f2: float
            The two values to compare
        output: boolean
            True if f1 and f2 are close to each other, False otherwise.
        """
        return abs(f1-f2) <= self.percentageAccuracy

    def getNodeByID(self, distribution, i):
        """
        Find the node of a given tree by its ID
        distribution : dict
            The dict containing the tree. Should contain "id" (Integer) and an optional dict "children" which has the same dataset structure
            All IDs should be unique (i.e., there should be no duplication)
        i : Integer
            The ID of the node to look for
        return : dict or None
            If found, this function returns the node which has the corresponding ID. Else, it returns None. The node should have the same data structure as "distribution"
        """
        def find_rec(curNode):
            if curNode["id"] == i:
                return curNode
            elif curNode["id"] > i:
                return None #This is a specificity of our graph: Nodes are inserted in bigger order, so child[id] > this[id]

            if "children" in curNode:
                for _,child in curNode["children"].items():
                    value = find_rec(child)
                    if value is not None:
                        return value
            return None

        return find_rec(distribution)

    def sortDistribution(self, sortNodesFn):
        """
        Sort the stored distribution using sortNodesFn
        @param sortNodeFn the sorting method to use
        @return a list of distributions (nodes). The order follows self._distributionslen(result) == len(self._distributions).
        Each node possesses: an ID, count, and a LIST of children
        """
        def iterNode_rec(parentNode, curNode, previousDistribution):
            #Sort children in the new distribution
            previousChildren = previousDistribution["children"] if previousDistribution is not None else []
            children         = parentNode["children"].values() if 'children' in parentNode  else []
            children         = sortNodesFn(children, parentNode, previousDistribution, previousChildren)

            curNode['count']    = parentNode['count']
            curNode['id']       = parentNode['id']

            curNodeChildren = []
            for child in children:
                curChild = {}
                iterNode_rec(child, curChild, next((x for x in previousChildren if x['id'] == child['id']), None) if previousChildren is not None else None)
                curNodeChildren.append(curChild)
            curNode['children'] = curNodeChildren

        distributions = []
        for dist in self._distributions:
            curDistribution = {}
            iterNode_rec(dist, curDistribution, distributions[-1] if len(distributions) > 0 else None)
            distributions.append(curDistribution)

        return distributions

                    
    #The next three methods relies on the same signature because they are meant to be given by parameters
    #to the statistical functions (see print)
    def sortNodesByIDs(self, nodes, parent=None, previousParent=None, previousNodes=None):
        """
        Sort all the received nodes by their "id" from lowest to biggest 
        This will be then compatible with the fact that newly added nodes (and thus non-existing nodes) will be at the end of the list
        nodes: list of dict
            the nodes of the tree to sort
        parent: dict
            the parent owning the nodes. Not used.
        previousParent: dict:
            not used.
        previousNodes: list of dict
            not used.
        output: list of dict
            the nodes sorted
        """
        return sorted(nodes, key = itemgetter("id"))

    def sortNodesByCount(self, nodes, parent=None, previousParent=None, previousNodes=None):
        """
        Sort all the received nodes by their "count" 
        nodes: list of dict
            the nodes of the tree to sort
        parent: dict
            the parent owning the nodes. Not used.
        previousParent: dict:
            not used.
        previousNodes: list of dict
            not used.
        output: list of dict
            the nodes sorted in descending order
        """
        return sorted(nodes, key = itemgetter("count"), reverse=True)

    def sortNodesByCount_hysteresis(self, nodes, parent, previousParent=None, previousNodes=None):
        """
        Sort all the received nodes by their "count" taking into account the hysteresis.
        nodes: list of dict
            the nodes of the tree to sort.
        parent: dict
            the parent owning the nodes
        previousParent: dict:
            the parent owning the nodes in the previous iteration (if any). None if not applicable.
        previousNodes: list of dict
            the nodes of the tree sorted in the previous iteration (if any). None if not aplication
        output: list of dict
            the nodes sorted in descending order taking into account some late effects
        """
        parentID = parent["id"]
        parentCount = parent["count"]

        sortedNodes = list(nodes)
        #If first iteration -> sort in the usual way
        if previousParent is None:
            sortedNodes = self.sortNodesByCount(sortedNodes, parent) #Always sort by the count value if first iteration

        #Else: Ensure that the new children are within the same organization as the previous children
        #We need that here because of how C++ handles the printing of std::map
        else:
            assert parentID == previousParent['id']
            assert parentCount >= previousParent['count']
            assert len(previousNodes) <= len(sortedNodes)

            for i in range(len(previousNodes)):
                previousNodeID = previousNodes[i]["id"]
                if previousNodeID != sortedNodes[i]["id"]:
                    for j in range(i, len(sortedNodes)):
                        if sortedNodes[j]["id"] == previousNodeID:
                            sortedNodes[i], sortedNodes[j] = sortedNodes[j], sortedNodes[i]
                            break

            #And check the sorting with an inertia using a bubble sort
            #This "imperfect" sorting ensures the following equation: 
            #forall{i, j} : 0 < i < j < len(sortedNodes) -> weight(sortedNodes[j]) − weight(sortedNodes[i]) ≤ hysteresisInertia
            if len(sortedNodes) > 1:
                hasPermuted = True
                while hasPermuted:
                    hasPermuted     = False
                    minValue        = sortedNodes[0]["count"]
                    lastMinValueIDx = 0
                    for i in range(1, len(sortedNodes)): #We cannot optimize the start index because of the inertia condition
                        sortedNodeCount = sortedNodes[i]["count"]
                        if minValue < sortedNodeCount and (sortedNodeCount-minValue)/parentCount > self.hysteresisInertia:
                            for j in range(i, lastMinValueIDx, -1):
                                (sortedNodes[j],   sortedNodes[j-1]) = \
                                (sortedNodes[j-1], sortedNodes[j])
                            lastMinValueIDx += 1
                            hasPermuted = True
                        elif minValue >= sortedNodeCount: #>= ensures the minimum number of permutations
                            minValue = sortedNodeCount
                            lastMinValueIDx = i

        return sortedNodes

    def getIterationStatisticsPerNode(self, sortedDistributions):
        benchmarkMetrics = BenchmarkNodeMetrics()

        def getNodeByID(distribution, i):
            def find_rec(curNode):
                if curNode["id"] == i:
                    return curNode
                elif curNode["id"] > i:
                    return None #This is a specificity of our graph: Nodes are inserted in bigger order, so child[id] > this[id]

                if "children" in curNode:
                    for child in curNode["children"]:
                        value = find_rec(child)
                        if value is not None:
                            return value
                return None

            return find_rec(distribution)
                        
        def iterNode_rec(parentNode, minIT):
            """
            Iter over all nodes of the last distribution
            parentNode: The parent Node of the last iteration
            minIT: The minimal iteration at which curNode could be stable. This is useful because a node cannot be stable if the parent is not.
            """

            #Get the children. Handle the case where a node is a leaf
            children = parentNode["children"] if 'children' in parentNode else []
            stableIT = minIT

            #Check when the position of nodes is different
            for i in range(len(sortedDistributions)-1, minIT-1, -1):
                parentNodeCurIter = getNodeByID(sortedDistributions[i], parentNode["id"])
                if parentNodeCurIter is None:
                    break

                curChildren = parentNodeCurIter["children"] if 'children' in parentNodeCurIter  else []
                for j in range(len(curChildren)):
                    if curChildren[j]['id'] != children[j]['id']:
                        stableIT = i
                        break

            #Now that we have stableIT, propagate it
            for node in children:
                nodeBenchmark       = benchmarkMetrics.insertNode()
                nodeBenchmark.path  = [self.ctx.event_name(x) for x in self.ctx.graph.get_node_parent_and_self_types(node["id"])]
                nodeBenchmark.id    = node["id"]
                nodeBenchmark.count = node["count"]
                nodeBenchmark.stableAtIteration = stableIT
                iterNode_rec(node, nodeBenchmark.stableAtIteration)

        iterNode_rec(sortedDistributions[-1], 0)

        return benchmarkMetrics

    def getIterationStatisticsPerDepth(self, depth, sortedDistributions):
        """
        Generate the BenchmarkIterationMetrics of this benchmark at a given depth 

        depth: int
            The depth to check for stability. Depth should start at 1, as the level "0" corresponds to the "root" node.
        sortedDistributions: list
            The sorted distribution
        output: BenchmarkIterationMetrics
            Return the iteration statistics at depth == depth
            -Whether the nodes of both a given iteration and the whole tree (as processed entirely) possesses the same node
            in the same order (depends on the sorting method)
            -Whether the percentage distribution for all nodes are similar to the distribution of the whole tree (as processed entirely),
            see self.percentageAccuracy.
        """
        benchmarkMetrics = BenchmarkIterationMetrics()
        lastIteration    = sortedDistributions[-1]

        def countNbNodes_rec(node, curDepth):
            if curDepth == depth:
                return 0

            ret = 1
            if "children" in node:
                for x in node["children"]:
                    ret += countNbNodes_rec(x, curDepth+1)

            return ret


        #By construction of this algorithm (see the addition of None objects and the counting of permutations), we are dealing with pairs of
        #nodes that have the same IDs
        def iterNode_rec(parentNodeCurIter, totalCurDistribution, parentNodeLastIter, totalLastDistribution, curDepth, iterationData):
            if curDepth == depth:
                return

            assert parentNodeCurIter["id"] == parentNodeLastIter["id"]

            #Get the children. Handle the case where a node is a leaf
            curChildren  = list(parentNodeCurIter["children"])  if 'children' in parentNodeCurIter  else []
            lastChildren = list(parentNodeLastIter["children"]) if 'children' in parentNodeLastIter else []

            #Reorder the nodes and count the number of permutation done using a "bubble sort" algorithm
            for i in range(len(lastChildren)):
                if i < len(curChildren) and curChildren[i] != None and curChildren[i]["id"] == lastChildren[i]["id"]: #if no permutation -> happy ending
                    continue

                hasPermuted = False
                for j in range(i+1, len(curChildren)): #Else try to find the permutation to do
                    if curChildren[j] == None: 
                        continue

                    if curChildren[j]["id"] == lastChildren[i]["id"]:
                        curChildren[i], curChildren[j] = curChildren[j], curChildren[i] #Permut
                        hasPermuted = True
                        iterationData.permutations+=1
                        break

                if not hasPermuted: #No permutation? It means we did not find the ID -> create a "None" node
                    iterationData.missingNodes += countNbNodes_rec(lastChildren[i], curDepth)
                    curChildren.insert(i, None)

            assert len(curChildren) == len(lastChildren)

            for curNode, lastNode in zip(curChildren, lastChildren):
                curCount = 0 if curNode is None else curNode["count"]

                #Check the statistics. If curNode is None, it means that we did not find the corresponding node. We can, however, count the statistics as if this node
                #Exists for every iteration with a count of 0
                if not self._areFloatsAlmostSimilar(curCount         /totalCurDistribution,\
                                                    lastNode["count"]/totalLastDistribution):
                    iterationData.badStatistics += 1
                if curNode != None:
                    iterNode_rec(curNode, totalCurDistribution, lastNode, totalLastDistribution, curDepth+1, iterationData)

        for i in range(len(sortedDistributions)-1):
            distribution = sortedDistributions[i]
            iterNode_rec(distribution, distribution["count"], lastIteration, lastIteration["count"], 0, benchmarkMetrics.insertIteration())

        return benchmarkMetrics

    def getNbIterations(self):
        """
        return : Integer
            The number of iterations of the overall progressive algorithm
        """
        return len(self._distributions)

    def processData(self, dataDirectory):
        """
        Launch the progressive algorithm until its end by reading the data of "dataDirectory".
        This method saves all the needed data for later analysis
        dataDirectory: str
            The path containing the data for parcoursvis
        """
        print(f"# using data from directory {dataDirectory}")
        self.ctx.load(dataDirectory)
        self.ctx.align_sequence       = self.alignSequence
        self.ctx.align_sequence_by    = self.alignSequenceBy
        self.ctx.apply_filter         = False
        self.ctx.prune_threshold      = self.threshold
        self.ctx.chunk_size           = self.chunkSize
        self.ctx.max_depth            = maxDepth
        self.ctx.progressive_strategy = parcoursprog.ProgressiveStrategy.CHUNK_PATIENTS
        isFirst = True

        while not self.ctx.process_done():
            startTime = time.perf_counter_ns()
            self.ctx.process_next(isFirst)
            endTime = time.perf_counter_ns()
            isFirst = False
            self._computationTimes.append(endTime-startTime)
            self._distributions.append(self.ctx.tree())

    def print(self, jsonOutput, onlyTime=False):
        """
        Print both in the standard output and in self.outputFile the results of the analysis.
        jsonOutput : dict
            The JSON output data structure. Should contain the keys "ProgressiveProg" and "ProgressiveStats". See "initOutputDict" for more information
        onlyTime: boolean
            True if only the time metrics (speed) should be measured
        """

        performance = {"iterations": [], "stats": [], "statsPerNode": [], "totalTime": self.totalComputationTime/1000000, "alignSequence": [self.ctx.event_name(x) for x in self.ctx.align_sequence], "alignSequenceBy": self.ctx.align_sequence_by, "chunk_size": self.ctx.chunk_size, "threshold": self.ctx.prune_threshold}
        for i, time in enumerate(self._computationTimes):
            performance["iterations"].append({'time': time, 'nbProcessed': self._distributions[i]["nb_processed"]})
            print(f"# iteration {i}: {(time)/1000000:.2f}ms")
        print(f"# computation time of process_progressive: {(self.totalComputationTime/1000000):.2f}ms, number of iteration: {len(self._computationTimes)}")

        if not onlyTime:
            for sortNodes, sortMethodStr in zip((self.sortNodesByIDs, self.sortNodesByCount, self.sortNodesByCount_hysteresis), ("IDs", "Count", "Hysteresis")):
                print(f"# Check statistics by sorting nodes by {sortMethodStr}")
                sortedDistributions = self.sortDistribution(sortNodes)
                nodeData = self.getIterationStatisticsPerNode(sortedDistributions)
                performance["statsPerNode"].append({"sortingMethod": sortMethodStr, "data": [{"id": x.id, "count": x.count, "path": x.path, "stableAtIteration": x.stableAtIteration} for x in nodeData]})

                for depth in range(5):
                    iterData = self.getIterationStatisticsPerDepth(depth+1, sortedDistributions)
                    performance["stats"].append({"sortingMethod": sortMethodStr, "depth": depth, "stableWithTopologyAtIter": iterData.stableWithTopologyAt, "stableWithoutTopologyAtIter": iterData.stableWithoutTopologyAt,\
                                                 "data": [{"permutations": x.permutations, "badStatistics": x.badStatistics, "missingNodes": x.missingNodes} for x in iterData]})
                    print(f"# Iteration stable, for depth {depth} at: {iterData.stableWithTopologyAt}")
                print("\n")
        jsonOutput["Progressive"].append(performance)

    iterations           = property(lambda self : self._distributions)
    totalComputationTime = property(lambda self : sum(self._computationTimes))

def processAll_cache(dataDirectory):
    """
    Process the dataset given as an argument to force the system to cache the different files
    dataDirectory : str
        The absolute or relative path to the directory containing the files data.bin, data.map, and indi.bin containing the overall dataset
    """
    ctx = parcoursprog.context()
    ctx.load(dataDirectory)
    ctx.apply_filter = False
    ctx.process_all()

    return ctx

def printProcessAll(ctx, computationTime, output):
    ctx.update_view_data()
    tree = ctx.tree()
    output["NonProgressive"].append({"time": computationTime/1000000, "alignSequence": [ctx.event_name(x) for x in ctx.align_sequence], "alignSequenceBy": ctx.align_sequence_by,\
                                     "chunk_size": ctx.chunk_size, "threshold": ctx.prune_threshold, "nbPatients": tree['count'],\
                                     "nbLowLevel": ctx.get_nb_low_level_events(), "nbHighLevel": sum(tree['subcount'].values()),\
                                     "nbPathways": ctx.get_nb_distinct_pathways(), "nbNodes": ctx.get_nb_useful_nodes()})
    print(f"# Information for ProcessAll:")
    print(f"# computation time of process_all: {(computationTime/1000000):.2f}ms")
    print(f"# number of patients: {tree['count']}")
    print(f"# number of low_level events: {ctx.get_nb_low_level_events()}")
    print(f"# number of high_level events: {sum(tree['subcount'].values())}")
    print(f"# number of distinct pathways: {ctx.get_nb_distinct_pathways()}")
    print(f"# number of displayed nodes: {ctx.get_nb_useful_nodes()}")
    print("\n")

def benchmarkNonAlign(dataDirectory, output, onlyTime):
    """
    Benchmark the ParcoursVis' different algorithms for non-aligned views
    dataDirectory : str
        The absolute or relative path to the directory containing the files data.bin, data.map, and indi.bin containing the overall dataset
    output: dict
        The JSON dict output of the script. See initOutputDict
    onlyTime: boolean
        True if only the time metrics (speed) should be measured
    """
    print("# START THE BENCHMARK FOR NON-ALIGNED VIEWS\n")

    #Start to benchmark... 
    #...The non-progressive algorithm
    ctx = parcoursprog.context()
    ctx.load(dataDirectory)
    ctx.apply_filter = False
    ctx.chunk_size = 0
    ctx.prune_threshold = 0
    ctx.max_depth = maxDepth
    startTime = time.perf_counter_ns()
    ctx.process_all()
    endTime   = time.perf_counter_ns()
    printProcessAll(ctx, endTime-startTime, output)

    for chunkSize in [100000, 150000, 200000, 250000]:
        for threshold in [0, 25, 50]:
            print("\n# Information for ProcessProgressive:")
            #...The progressive algorithm
            progressiveStats = ProgressiveStats()
            progressiveStats.chunkSize = chunkSize
            progressiveStats.threshold = threshold
            progressiveStats.processData(dataDirectory)
            progressiveStats.print(output, onlyTime)
            print("\n")


def benchmarkAligned(dataDirectory, alignSequence, alignBy, output):
    """
    Benchmark the ParcoursVis' different algorithms for aligned views
    dataDirectory : str
        The absolute or relative path to the directory containing the files data.bin, data.map, and indi.bin containing the overall dataset
    alignSequence : list of str
        The sequence to be aligned with. The string should match the names of the events ParcoursVis handles.
    alignBy : Integer
        Set the "align_sequence_by" value of ParcoursVis.
    """
    print("# START THE BENCHMARK FOR ALIGNED VIEWS.\n")
    print(f"Align parameters: {alignSequence}:{alignBy}")

    #Initialize a default context to retrieve all the types of events ParcoursVis handles
    ctx = parcoursprog.context()
    ctx.load(dataDirectory)
    types = ctx.valid_type_combination
    for x in alignSequence:
        if not x in types:
            raise ValueError(f"Could not find event named {x}.")
    alignSequenceIDs = [types[x] for x in alignSequence]

    #Process all the data at once to get basic information
    ctx.apply_filter      = False
    print(alignSequenceIDs)
    ctx.align_sequence    = alignSequenceIDs
    ctx.align_sequence_by = alignBy
    startTime = time.perf_counter_ns()
    ctx.process_all()
    endTime   = time.perf_counter_ns()
    printProcessAll(ctx, endTime-startTime, output)

    #Start to benchmark the progressive algorithm
    progressiveStats = ProgressiveStats()
    progressiveStats.alignSequence   = alignSequenceIDs
    progressiveStats.alignSequenceBy = alignBy
    progressiveStats.processData(dataDirectory)
    print("\n# Information for ProcessProgressive:")
    progressiveStats.print(outputData)

def initOutputDict():
    data = {}
    data["GeneralParameters"] = {"hysteresisInertia": hysteresisInertia, "percentageAccuracy": percentageAccuracy, \
                                 "screenSize": screenSize, "percentageAccuracyInPixels": percentageAccuracyInPixels}
    data["NonProgressive"]   = []
    data["Progressive"]      = []

    return data

if __name__ == '__main__':
    argv          = sys.argv[1:]
    dataDirectory = "tmp"
    files         = []
    outputFile    = os.getcwd() + "/output.json"
    outputData    = initOutputDict()
    onlyTime      = False

    #Read argv
    i = 0
    while i < len(argv):
        arg = argv[i]
        if i == 0 and not (arg.startswith("--") or arg.startswith("-")):
            dataDirectory = arg
            i+=1
        else:
            while i < len(argv):
                arg = argv[i]

                if arg == "-h" or arg == "--help":
                    printDocAndExit()

                elif arg == "--output":
                    if i < len(argv)-1:
                        if argv[i+1][0] == '-':
                            print(f"Missing JSON file path value to the '--output' parameter. {argv[i+1]} is not a correct value. Exiting.")
                            sys.exit(-1)

                        outputFile = argv[i+1]
                        i+=1
                    else:
                        print("Missing JSON file path value to the '--output' parameter. Exiting.")
                        sys.exit(-1)
                elif arg == "--onlyTime":
                    onlyTime = True
                else:
                    print(f"Unknown parameter {arg}")
                    printDocAndExit()
                i+=1

    #Check that the data exists
    assert os.path.isfile(os.path.join(dataDirectory, "data.bin"))
    assert os.path.isfile(os.path.join(dataDirectory, "data.map"))
    assert os.path.isfile(os.path.join(dataDirectory, "indi.bin"))

    #Fill the file cache system linux systems have... Otherwise, the second benchmarking will be faster than the first one
    print("Start to parse twice the dataset to cache the memmap file...")
    for i in range(2):
        processAll_cache(dataDirectory)

    #Start the benchmark for non-aligned and aligned trees
    benchmarkNonAlign(dataDirectory, outputData, onlyTime)
    #for align in ["surgery", "phyto", "alphabloc", "fivealpha", "interruption", "no treatment"]:
    #    benchmarkAligned(dataDirectory, [align], 0, outputData)

    #Save the data into the file "outputFile" in a JSON format
    with open(outputFile, 'w') as f:
        json.dump(outputData, f, indent=2)
