import * as d3 from "d3";
const jsonData0 = require("./data0.json");
const jsonData1 = require("./data1.json");
const jsonData  = [jsonData0, jsonData1];

const NB_BINS = 40;
//------------------------------------------------------------------------------
//------------------------Declare common GUI components-------------------------
//------------------------------------------------------------------------------

//The SVG
var svgDepth = d3.select("#svgdepth").append("svg")
                 .attr("width", 1920)
                 .attr("height", 360);

var svgNodes = d3.select("#svgnodes").append("svg")
                 .attr("width", 1280)
                 .attr("height", 360)
                 .append("g")
                   .attr("transform", "translate(" + 60 + "," + 20 + ")");

//The widgets
var   curDatabaseBtn        = null;
var   curSortRadioBtn       = null;
const hysteresisInertiaTxt  = document.getElementById("hysteresisInertia");
const percentageAccuracyTxt = document.getElementById("percentageAccuracy");
const iterationIDBtn        = document.getElementById("iterationID");
const checkTopologyBtn      = document.getElementById("checkTopology");
const thresholdTxt          = document.getElementById("threshold");
const chunkSizeTxt          = document.getElementById("chunkSize");

const showDifferenceNodeBtn = document.getElementById("checkNodeDifference");
const showHeatmap         = document.getElementById("checkHeatmap");
const minimumNodeDepth      = document.getElementById("depthLabelMin");
const maxDepthNodeBtn       = document.getElementById("maxDepthNode");

var updateUI = function() {
  let data = jsonData[curDatabaseBtn.value];

  console.log(`Current sorting algorithm: ${curSortRadioBtn.value}`);
  console.log(`Results of iteration: ${iterationIDBtn.value}`);
  console.log(`Topology checked: ${checkTopologyBtn.checked}`);
  console.log('');

  //Update the metadata UI
  thresholdTxt.textContent = data.Progressive[iterationIDBtn.value].threshold;
  chunkSizeTxt.textContent = data.Progressive[iterationIDBtn.value].chunk_size;
};

var updateDepthVisualization = function() {
  console.log('Updating the visualization per depth in-progress...');
  console.log('');

  let data = jsonData[curDatabaseBtn.value];

  //Recreate the hierarchy
  let id = 0;
  let root = {
    name:          "root",
    method:        curSortRadioBtn.value,
    checkTopology: checkTopologyBtn.checked,
    children:      [],
    id:            id++,
  };

  for(let stats of data.Progressive[iterationIDBtn.value].stats) {
    if(stats.sortingMethod == curSortRadioBtn.value) {
      let x = 0;
      root.children.push({
        name:     "iteration",
        depth:    stats.depth,
        children: stats.data.map(function(s){ return {...s, id: id++, x: x++}; }),
        id:       id++,
        stableAt: [stats.stableWithoutTopologyAtIter, stats.stableWithTopologyAtIter],
      });
    }
  }

  let hierarchy = d3.hierarchy(root);
  let leaves    = hierarchy.leaves();
  let maxBadStatistics = Math.max(1, ...leaves.map((d) => d.data.badStatistics));
  let maxPermutations  = Math.max(1, ...leaves.map((d) => d.data.permutations));

  //Draw the hierarchy
  let outerWidthPerItem    = 40;
  let innerMaxWidthPerItem = 40;
  let innerMinWidthPerItem = 30;

  svgDepth.selectAll("g").remove();
  let depths = svgDepth.selectAll("g").data(hierarchy.children, d => `${curSortRadioBtn.value}${d.id}`).join(
    enter => {
      let g = enter.append("g")
        .attr('transform', d => `translate(5,${d.data.depth*outerWidthPerItem+5})`);

      g.selectAll("g").data(d => d.children, d => `${curSortRadioBtn.value}${d.id}`).join(
        enter => {
          var g = enter.append("g")
            .attr('transform', d => `translate(${d.data.x * outerWidthPerItem + (outerWidthPerItem-innerMaxWidthPerItem)/2},0)`);
          g.append("ellipse")
            .attr('cx', innerMaxWidthPerItem/2)
            .attr('cy', innerMaxWidthPerItem/2)
            .attr('rx', d => (innerMinWidthPerItem + d.data.permutations  * (innerMaxWidthPerItem-innerMinWidthPerItem)/maxPermutations)/2)
            .attr('ry', d => (innerMinWidthPerItem + d.data.badStatistics * (innerMaxWidthPerItem-innerMinWidthPerItem)/maxBadStatistics)/2)
            .attr('fill', d => (d.data.permutations == 0 && d.data.badStatistics == 0 && (root.checkTopology ? d.data.missingNodes == 0 : true)) ? 'green' : 'red')
            .attr('stroke', d => (d.parent.data.stableAt[root.checkTopology ? 1:0] == d.data.x) ? "black" : "")
            .attr('stroke-width', 3);
          g.append("text")
            .attr('x', innerMaxWidthPerItem/2).attr('y', innerMaxWidthPerItem/2)
            .attr('dominant-baseline', 'central')
            .attr('text-anchor', 'middle')
            .text(d => `${d.data.permutations}/${d.data.badStatistics}`)
        }
      );
    }
  );
};

var updateNodeVisualization = function() {
  let data  = jsonData[curDatabaseBtn.value];
  let nodes = null;

  svgNodes.selectAll("g").remove();

  //Safeguard
  if(data.Progressive[iterationIDBtn.value].statsPerNode == undefined)
    return;

  if(!showDifferenceNodeBtn.checked) {
    for(let stats of data.Progressive[iterationIDBtn.value].statsPerNode)
      if(stats.sortingMethod == curSortRadioBtn.value)
        nodes = stats.data;
  }
  else{
    let nodesByCount    = null;
    let nodesHysteresis = null;

    for(let stats of data.Progressive[iterationIDBtn.value].statsPerNode) {
      if(stats.sortingMethod == "Count")
        nodesByCount = stats.data;
      else if(stats.sortingMethod == "Hysteresis")
        nodesHysteresis = stats.data;
    }

    if(nodesByCount == null || nodesHysteresis == null)
      return;

    //Ensures nodes are sorted similarly
    nodesByCount.sort((a, b) => a.id - b.id);
    nodesHysteresis.sort((a, b) => a.id - b.id);

    nodes = [];
    for(let i = 0; i < nodesByCount.length; i++) {
      if(nodesByCount[i].id != nodesHysteresis[i].id || nodesByCount[i].count != nodesHysteresis[i].count) {
        console.log("nodes are sorted weirdly. Abort");
        return;
      }
      nodes.push({count: nodesByCount[i].count, stableAtIteration: nodesByCount[i].stableAtIteration - nodesHysteresis[i].stableAtIteration, path: nodesByCount[i].path});
    }
  }

  if(nodes == null)
    return;

  //Filter nodes based on their depth
  nodes = nodes.filter((n) => n.path.length <= maxDepthNodeBtn.value);

  var maxCount     = Math.max(...nodes.map((n) => n.count));
  var maxStability = Math.max(...nodes.map((n) => n.stableAtIteration));
  var minStability = Math.min(...nodes.map((n) => n.stableAtIteration));

  var width = 1280 - 20 - 120;
  var height = 360 - 50 - 20;


  if(!showHeatmap.checked) {
    //Add X axis
    var x = d3.scaleLog()
      .domain([1, maxCount])
      .range([ 0, width ]);
    svgNodes.append("g")
      .attr("transform", `translate(0, ${showDifferenceNodeBtn.checked ? height/2.0 : height})`)
      .call(d3.axisBottom(x))
      .append("text")
        .attr("dy", "0.71em")
        .attr("text-anchor", "start")
        .attr("transform", `translate(${width+5}, 0)`)
        .attr("fill", "black")
        .text("Node count (log)");

    //Add Y axis
    let maxY = Math.max(Math.abs(minStability), Math.abs(maxStability))+1;
    var y = d3.scaleLinear()
      .domain([(showDifferenceNodeBtn.checked ? -maxY : 0), maxY])
      .range([ height, 0]);
    svgNodes.append("g")
      .call(d3.axisLeft(y))
      .append("text")
        .attr("text-anchor", "middle")
        .attr("transform", `translate(0, -5)`)
        .attr("fill", "black")
        .text("Stable at Iteration");

    // Add dots
    svgNodes.append('g')
      .selectAll("dot")
      .data(nodes)
      .enter()
      .append("circle")
        .attr("cx", function (d) { return x(d.count); } )
        .attr("cy", function (d) { return y(d.stableAtIteration); } )
        .attr("r", 1.5)
        .style("fill", "#000000")
  }
  else {
    /** Init the heatmap*/
    let heatmap = [];
    for(let i = minStability; i <= maxStability; i++) {
      let xValues = [];
      for(let j = 0; j < NB_BINS; j++)
        xValues.push({y: i, x: j, value: 0});
      heatmap.push(xValues);
    }

    nodes.forEach((n) => heatmap[n.stableAtIteration-minStability][Math.min(NB_BINS-1, Math.floor(Math.log10(n.count)/Math.log10(maxCount) * NB_BINS))].value+=1);
    heatmap = heatmap.flat()
    let maxHeatmap = Math.max(...heatmap.map((n) => n.value));

    //Add X axis
    var xLog = d3.scaleLog()
      .domain([1, maxCount])
      .range([ 0, width]);

    let getX = function(x) {
      return xLog(Math.pow(maxCount, x/(NB_BINS-1)));
    }

    //Add Y axis
    let maxY = Math.max(Math.abs(minStability), Math.abs(maxStability))+1;
    let minY = minStability;//(showDifferenceNodeBtn.checked ? -maxY : 0);
    let heightQuantum = height/(maxY - minY);
    var y = d3.scaleLinear()
      .domain([minY, maxY])
      .range([ height, 0]);
    
    // Add rect
    svgNodes.append('g')
      .selectAll("square")
      .data(heatmap)
      .enter()
      .append("rect")
        .attr("x", function (d) 
          {
            let x = getX(d.x);
            return x;
          })
        .attr("y", function (d) { return y(d.y+0.5); } )
        .attr("width", function(d) { return getX(d.x + 0.50) - getX(d.x - 0.50)+1; })
        .attr("height", heightQuantum+1)
//        .attr("stroke", function(d) { return d3.interpolateBuGn(d.value/maxHeatmap);})
//        .attr("fill", function(d) { return d3.interpolateBuGn(d.value/maxHeatmap);});
        .attr("stroke", function(d) { return d3.interpolateBuGn(Math.log(1+d.value/maxHeatmap*(Math.E-1)));})
        .attr("fill", function(d) { return d3.interpolateBuGn(Math.log(1+d.value/maxHeatmap*(Math.E-1)));});

    let xScaleD3 = svgNodes.append("g")
      .attr("transform", `translate(0, ${height + minY/(maxY-minY)*height + heightQuantum/2})`)
      .call(d3.axisBottom(xLog));

    let yScaleD3 = svgNodes.append("g")
      .attr("transform", `translate(-1, 0)`)
      .call(d3.axisLeft(y));
    yScaleD3.append("text")
        .attr("text-anchor", "start")
        .attr("transform", `translate(-3, -5)`)
        .attr("fill", "black")
        .text("Stable at Iteration");

    xScaleD3.append("text")
      .attr("dy", "0.71em")
      .attr("text-anchor", "end")
      .attr("transform", `translate(${width}, 10)`)
      .attr("fill", "black")
      .text("Node count (log)");

    yScaleD3.selectAll("text").attr("fill", "black").style("font", "14px sans serif");
    xScaleD3.selectAll("text").attr("fill", "black").style("font", "14px sans serif");
    xScaleD3.selectAll("line").attr("stroke", "black");
    xScaleD3.selectAll("path").attr("stroke", "black");
  }
};

var updateDatabase = function() {
  let data = jsonData[curDatabaseBtn.value];

  //Database information
  hysteresisInertiaTxt.textContent  = data.GeneralParameters.hysteresisInertia;
  percentageAccuracyTxt.textContent = data.GeneralParameters.percentageAccuracy;

  //Iteration IDs
  const iterationLabelMin = document.getElementById("iterationLabelMin");
  const iterationLabelMax = document.getElementById("iterationLabelMax");

  iterationLabelMin.textContent = 0;
  iterationLabelMax.textContent = data.Progressive.length-1;

  iterationIDBtn.min   = 0;
  iterationIDBtn.max   = data.Progressive.length-1;
  if(parseInt(iterationIDBtn.value) > iterationIDBtn.max)
      iterationIDBtn.value = iterationIDBtn.max;

  //Depth IDs
  const maxDepth = Math.max(...data.Progressive[iterationIDBtn.value].statsPerNode[0].data.map((n) => n.path.length));
  const depthLabelMin = document.getElementById("depthLabelMin");
  const depthLabelMax = document.getElementById("depthLabelMax");

  depthLabelMin.textContent = 0;
  depthLabelMax.textContent = maxDepth;

  maxDepthNodeBtn.min   = 0;
  maxDepthNodeBtn.max   = maxDepth;
  if(parseInt(maxDepthNodeBtn.value) > maxDepthNodeBtn.max)
      maxDepthNodeBtn.value = maxDepthNodeBtn.max;

  updateUI();
  updateDepthVisualization();
  updateNodeVisualization();
};


//------------------------------------------------------------------------------
//----------------------------Handle GUI components-----------------------------
//------------------------------------------------------------------------------

//Database
const dataBtns = document.querySelectorAll('input[name="data"]');

for(let btn of dataBtns) {
  if(btn.checked)
    curDatabaseBtn = btn;
  btn.addEventListener('change', function() {
    if(this !== curDatabaseBtn)
      curDatabaseBtn = this;
    updateDatabase();
  });
}

//Sorting algorithm
const sortRadioBtns = document.querySelectorAll('input[name="sort"]');

for(let btn of sortRadioBtns) {
  if(btn.checked)
    curSortRadioBtn = btn;
  btn.addEventListener('change', function() {
    if(this !== curSortRadioBtn)
      curSortRadioBtn = this;
    updateUI();
    updateDepthVisualization();
    updateNodeVisualization();
  });
}

//Iteration IDs
iterationIDBtn.value = 0;
iterationIDBtn.addEventListener('change', function() {
  if(parseInt(iterationIDBtn.value) > iterationIDBtn.max)
    iterationIDBtn.value = iterationIDBtn.max;
  else if(iterationIDBtn.value < 0)
    iterationIDBtn.value = 0;
  updateUI();
  updateDepthVisualization();
  updateNodeVisualization();
});


//Check Topology
checkTopologyBtn.addEventListener('change', function() {
  updateUI();
  updateDepthVisualization();
});

//Check difference of sorting techniques
showDifferenceNodeBtn.addEventListener('change', function() {
  updateUI();
  updateNodeVisualization();
});

//Check Heatmap
showHeatmap.addEventListener('change', function() {
  updateUI();
  updateNodeVisualization();
});


//Iteration IDs
maxDepthNodeBtn.value = Number.MAX_SAFE_INTEGER;
maxDepthNodeBtn.addEventListener('change', function() {
  if(parseInt(maxDepthNodeBtn.value) > maxDepthNodeBtn.max)
    maxDepthNodeBtn.value = maxDepthNodeBtn.max;
  else if(iterationIDBtn.value < 0)
    maxDepthNodeBtn.value = 0;
  updateUI();
  updateNodeVisualization();
});

//Update the visualization at least once
updateDatabase();
