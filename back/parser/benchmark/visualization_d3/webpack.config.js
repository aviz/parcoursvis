const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");

var glob = require('glob');
var path = require('path');

module.exports = 
{
  context: __dirname,
  mode: 'development',
  devtool: 'inline-source-map',

  entry:
  {
    'src/index.js' : './src/index.js'
  },

  devServer:
  {
    port: 8080
  },

  module: {
    rules: 
    [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.css$/,
        loader: 'css-loader'
      }
    ],
  },

  resolve:
  {
    extensions: [ '.js', '.json' ],
  },

  plugins: 
  [
    new HtmlWebpackPlugin(
    {
      template: './public/index.html',
      inject: true,
      chunks: ['src/index.js'],
      filename: 'index.html'
    }),
    //new CopyPlugin({
    //  patterns: 
    //  [
    //    { from: "data", to: "data" },
    //  ],
    //})
  ]
};
