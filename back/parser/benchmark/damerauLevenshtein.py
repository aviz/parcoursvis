#Check https://en.wikipedia.org/wiki/Damerau%E2%80%93Levenshtein_distance
def optimalStringAlignment(l1, l2):
    d = list()
    for i in range(len(l1)+1):
        d.append([0 for j in range(len(l2)+1)])
        d[i][0] = i

    for j in range(len(l2)+1):
        d[0][j] = j

    for i in range(1, len(l1)+1):
        for j in range(1, len(l2)+1):
            if l1[i-1] == l2[j-1]:
                costSubstitution = 0
            else:
                costSubstitution = 1

            d[i][j] = min(d[i-1][j]   + 1,                #Delete new caracter\
                          d[i]  [j-1] + 1,                #Insert in d1 new caracter\
                          d[i-1][j-1] + costSubstitution) #Substitution

            if i > 1 and j > 1 and l1[i-1] == l2[j-1-1] and l1[i-1-1] == l2[j-1]:
                d[i][j] = min(d[i][j], d[i-2][j-2] + 1)

    return d[len(l1)][len(l2)]

def damerauLevenshteinDistance(a, b):
    da = {}
    for i in a:
        da[i] = 0
    for j in b:
        da[j] = 0
    
    d = list()
    for i in range(len(a)+2):
        d.append([0 for j in range(len(b)+2)])
    
    maxdist = len(a) + len(b)
    d[-1+1][-1+1] = maxdist
    for i in range(0, len(a)+1):
        d[i+1][-1+1] = maxdist
        d[i+1][0+1]  = i
    for j in range(0, len(b)+1):
        d[-1+1][j+1] = maxdist
        d[0+1][ j+1] = j
    
    for i in range(1, len(a)+1):
        db = 0
        for j in range(1, len(b)+1):
            k = da[b[j-1]]
            l = db
            if a[i-1] == b[j-1]:
                cost = 0
                db = j
            else:
                cost = 1
            d[i+1][j+1] = min(d[i-1+1][j-1+1] + cost,  #substitution
                              d[i+1][  j-1+1] + 1,     #insertion
                              d[i-1+1][j+1  ] + 1,     #deletion
                              d[k-1+1][l-1+1] + (i-k-1) + 1 + (j-l-1)) #transposition
        da[a[i-1]] = i
    return d[len(a)+1][len(b)+1]
