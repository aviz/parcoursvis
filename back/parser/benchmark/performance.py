import sys
import os
import json
import logging
import time
from functools import *
from itertools import *
from operator  import itemgetter, attrgetter
from benchmark import printProcessAll, initOutputDict

import parcoursprog

logging.basicConfig()

quantum   = 50
threshold = 0

if __name__ == '__main__':
    listDir = sys.argv[1:]
    if len(listDir) == 0:
        print("Call performance.py <dataset1> <dataset2> ... <datasetn>")
        sys.exit(-1)

    output = initOutputDict()

    for data in listDir:
        assert os.path.isfile(os.path.join(data, "data.bin"))
        assert os.path.isfile(os.path.join(data, "data.map"))
        assert os.path.isfile(os.path.join(data, "indi.bin"))

        ctx = parcoursprog.context()
        ctx.load(data)
        ctx.apply_filter = False
        ctx.quantum = quantum
        ctx.prune_threshold = threshold
        startTime = time.time()
        ctx.process_all()
        endTime   = time.time()
        printProcessAll(ctx, endTime-startTime, output)


