NTHREADS=6

echo "Performing the speed benchmark on nThreads == $NTHREADS"

echo "Start producing stability benchmark for Qing_2M"
#for i in ../../generate_data/*M.csv; do f=$(basename -- $i); OMP_NUM_THREADS=$NTHREADS OMP_PROC_BIND=true python benchmark.py ../../generate_data/${f%%.*} --output time${f%%.*}_${NTHREADS}.json; done
OMP_NUM_THREADS=$NTHREADS OMP_PROC_BIND=true python benchmark.py ../../qing_2m --output qing_2m_${NTHREADS}.json

echo "Then speed benchmark for all generated dataset"
for j in $(seq $NTHREADS); do for i in ../../generate_data/*M.csv; do f=$(basename -- $i); OMP_NUM_THREADS=$j OMP_PROC_BIND=true python benchmark.py ../../generate_data/${f%%.*} --output time${f%%.*}_${j}.json --onlyTime; done; done

echo "Performing statistical data analyses..."
echo "First on scalability..."
python scalability.py ./time*M_6.json --output output/
python scalability_threads.py ./time*M_*.json --output output/ --nThreads $NTHREADS

echo "Then on stability..."
python analyses.py qing_2m_${NTHREADS}.json --output output
python stability.py qing_2m_${NTHREADS}.json --output output
