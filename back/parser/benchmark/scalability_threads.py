from cycler import cycler
import sys
import os
import json
import math
import scipy
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.ticker as ticker
import functools

def printDocAndExit():
    """
    Print the documentation of how to use the script and exit the program.
    """
    print("Run ./scalability.py inFile1 [...inFiles] [--nThreads int] [--output directoryPath]")
    print("                     [-h] [--help]")
    print("inFiles: the json files to analyse from the scalability point of view. Should be a multiple of nThreads. See benchmark.py outputs.")
    print("--nThreads: the number of maximal threads. Files should be ordered per thread results THEN per datasets. We expect nThreads files per datasets. Default: 8")
    print("--output: output directory to save the different PDF data analyses results. Default: ${pwd}/output/")
    print("-h --help: Print this documentation and exit.")

    sys.exit(-1)

def printTime(datas, nThreads, outputDir):
    """
    Save the time performance in outputDir/scalabilityTime_threads.pdf
    This computes the 95% confidence interval linechart comparing CPU times (y-axis) and the number of low-level events (x-axis) for the progressive algorithm (depends on the number of threads)
    and the CPU times (y-axis) and the number of low-level events (x-axis) without any progressive features

    @datas: The list of the benchmark's results.
    @nThreads: The maximum number of threads
    @outputDir: The output directory to save scalability_threads.pdf and scalabilityTime_nbEvents in.
    """

    columnsNonProg = ['CPUTime', 'nbEvents', "threadID"]
    timeNonProg    = pd.DataFrame(columns=columnsNonProg)
    columnsProg = ['CPUTime', 'chunkSize', 'nbEvents', 'threadID']
    timeProg    = pd.DataFrame(columns=columnsProg)

    #Gather the values we are interested in
    for i, data in enumerate(datas):
        threadID = i % nThreads + 1
        nonProgTime = np.mean([x['time'] for x in data["NonProgressive"] if len(x['alignSequence']) == 0])
        nbLowLevel = data["NonProgressive"][0]["nbLowLevel"]
        timeNonProg = pd.concat([timeNonProg, pd.DataFrame([[nonProgTime, nbLowLevel, threadID]], columns=columnsNonProg)], ignore_index=True)

        prog  = [x for x in data["Progressive"] if len(x['alignSequence']) == 0]
        for x in prog:
            timeProg = pd.concat([timeProg, pd.DataFrame([[x['totalTime'], x['chunk_size'], nbLowLevel, threadID]], columns=columnsProg)], ignore_index=True)

    timeProg  = timeProg.groupby(['chunkSize', 'nbEvents', 'threadID'], as_index=False).mean() #Apply a mean for CPUTime that depends on the threshold variable
    maxChunkSize = int(max(timeProg['chunkSize']))
    timeProg = timeProg[timeProg['chunkSize'] == maxChunkSize]
    assert len(timeProg) == len(timeNonProg)

    uniqueNbEvents = pd.unique(timeNonProg["nbEvents"])
    uniqueNbEvents.sort()
    uniqueThreadID  = pd.unique(timeNonProg["threadID"])
    uniqueThreadID.sort()

    outputFile = f"{outputDir}/scalabilityTime_threads.pdf"
    print(f"Saving {outputFile}...\n")

    fig = plt.figure()
    fig.set_tight_layout(True)
    fig.set_size_inches(10, 4)
    ax1 = fig.add_subplot(aspect="auto")
    ax1.set(axisbelow=True,
            title='',#Total computation time per size of the datasets\ncompared to the number of threads',
            xlabel='Number of threads',
            ylabel='Time (s)',
    )
    ax1.yaxis.set_major_locator(ticker.MultipleLocator(0.25))
    ax1.set_ylim(ymin=0, ymax=functools.reduce(lambda x, y: max(x, y), timeNonProg["CPUTime"]/1000.0)+0.25)
    ax1.set_xticks([int(x) for x in pd.unique(timeNonProg["threadID"])])
    ax1.set_xticklabels([f"{x:,}" for x in pd.unique(timeNonProg["threadID"])])
    for nbEvents in uniqueNbEvents:
        subData = timeNonProg[timeNonProg["nbEvents"] == nbEvents].sort_values("threadID")
        x = subData["threadID"]
        y = subData["CPUTime"]/1000
        ax1.plot(x, y, '-o', label=f"number of low-level events = {nbEvents:,}")
    ax1.legend(loc='upper right')
    ax1.grid()
    fig.savefig(outputFile, format="pdf", transparent=True)

    outputFile = f"{outputDir}/scalabilityTime_threadsRatio.pdf"
    print(f"Saving {outputFile}...\n")

    fig = plt.figure()
    fig.set_size_inches(10, 3)
    ax1 = fig.add_subplot(aspect="auto")
    ax1.set(axisbelow=True,
            title='',#Increase of computational speed per size of the dataset\ncompared to single-threaded computation',
            xlabel='Number of threads',
            ylabel='Speed Ratio',
    )
    ax1.set_ylim(ymin=0.7, ymax=nThreads+0.3)
    ax1.set_xlim(xmin=0.7, xmax=nThreads+0.3)
    ax1.set_yticks([x for x in range(1, nThreads+1, 1)])
    ax1.set_xticks([int(x) for x in pd.unique(timeNonProg["threadID"])])
    ax1.set_xticklabels([f"{x:,}" for x in pd.unique(timeNonProg["threadID"])])
    slopes = []

    for nbEvents in uniqueNbEvents:
        subData = timeNonProg[timeNonProg["nbEvents"] == nbEvents].sort_values("threadID")
        assert len(subData[subData['threadID'] == 1]['CPUTime']) == 1
        x = subData["threadID"]
        y = np.asarray(subData[subData['threadID'] == 1]['CPUTime'])[0] / subData["CPUTime"]
        ax1.plot(x, y, '-o', label=f"number of low-level events = {nbEvents:,}")
        slopes.append(np.asarray(y))
    ax1.legend(loc='upper left')
    ax1.grid()
    fig.savefig(outputFile, format="pdf", transparent=True)

    linregresses = [scipy.stats.linregress(range(1, len(x)+1), x) for x in slopes]
    gmSlopes     = scipy.stats.mstats.gmean([x.slope for x in linregresses])
    rSquares     = [x.rvalue**2 for x in linregresses]
    linregressesANOVA = scipy.stats.f_oneway(*slopes)

    outputFile = f"{outputDir}/scalabilityTime_nbEvents.pdf"
    print(f"Saving {outputFile}...\nGeometrical mean of slopes: {gmSlopes}\nSlopes: {[x.slope for x in linregresses]}\nR²: {rSquares}\nANOVA p-value: {linregressesANOVA.pvalue}")

    fig = plt.figure()
    fig.set_tight_layout(True)
    fig.set_size_inches(10, 4)
    ax1 = fig.add_subplot(aspect="auto")
    ax1.set(axisbelow=True,
            title='',#Total computation time per number of threads\ncompared to the size of the datasets for chunkSize = $\infty$',
            xlabel='Number of low-level events',
            ylabel='Time (s)',
    )
    ax1.yaxis.set_major_locator(ticker.MultipleLocator(0.25))
    ax1.set_ylim(ymin=0, ymax=functools.reduce(lambda x, y: max(x, y), timeNonProg["CPUTime"]/1000.0)+0.25)
    ax1.set_xticks([int(x) for x in pd.unique(timeNonProg["nbEvents"])])
    ax1.set_xticklabels([f"{x:,}" for x in pd.unique(timeNonProg["nbEvents"])])

    propCycle = plt.rcParams["axes.prop_cycle"].by_key()
    for threadID in uniqueThreadID:
        subData = timeNonProg[timeNonProg["threadID"] == threadID].sort_values("nbEvents")
        x = subData["nbEvents"]
        y = subData["CPUTime"]/1000
        ax1.plot(x, y, '-o', color=propCycle['color'][threadID-1], label=f"nThreads={threadID:,}")

    for threadID in uniqueThreadID:
        subData = timeProg[timeProg["threadID"] == threadID].sort_values("nbEvents")
        x = subData["nbEvents"]
        y = subData["CPUTime"]/1000
        ax1.plot(x, y, '--o', color=propCycle['color'][threadID-1])
    ax1.legend(loc='upper left')
    ax1.grid()
    fig.savefig(outputFile, format="pdf", transparent=True)

    outputFile = f"{outputDir}/scalabilityTime_maxChunkSize_nbEvents.pdf"
    print(f"Saving {outputFile}...\nMax chunk size: {maxChunkSize}")

    fig = plt.figure()
    fig.set_tight_layout(True)
    fig.set_size_inches(10, 4)
    ax1 = fig.add_subplot(aspect="auto")
    ax1.set(axisbelow=True,
            title='',#f'Total computation time per number of threads\ncompared to the size of the datasets for chunkSize = {maxChunkSize}',
            xlabel='Number of low-level events',
            ylabel='Time (s)',
    )
    ax1.set_ylim(ymin=0, ymax=functools.reduce(lambda x, y: max(x, y), timeProg["CPUTime"]/1000.0)+0.25)
    ax1.yaxis.set_major_locator(ticker.MultipleLocator(0.25))
    ax1.set_xticks([int(x) for x in pd.unique(timeProg["nbEvents"])])
    ax1.set_xticklabels([f"{x:,}" for x in pd.unique(timeProg["nbEvents"])])
    for threadID in uniqueThreadID:
        subData = timeProg[timeProg["threadID"] == threadID].sort_values("nbEvents")
        x = subData["nbEvents"]
        y = subData["CPUTime"]/1000
        ax1.plot(x, y, '-o', label=f"threadID = {threadID:,}")
    ax1.legend(loc='upper left')
    ax1.grid()
    fig.savefig(outputFile, format="pdf", transparent=True)
    
    outputFile = f"{outputDir}/scalabilityTime_numerics.ini"
    print(f"Saving numerical values in the external file {outputFile}")
    with open(outputFile, 'w') as f:
        f.write(f"scalability_slope_threads_gm: {gmSlopes}")
        f.write(f"\nscalability_slope_threads_gm: {[x.slope for x in linregresses]}")
        f.write(f"\nscalability_rsquared_threads: {rSquares}")
        f.write(f"\nANOVA_pvalue_on_models: {linregressesANOVA.pvalue}")
        medianTimes = []

        for i in range(nThreads-1, len(datas), nThreads):
            data = datas[i]
            prog = [x for x in data["Progressive"] if len(x['alignSequence']) == 0 and x['chunk_size'] == maxChunkSize and x['threshold'] == 0]
            assert len(prog) == 1
            prog = prog[0]
            medianTimes.append(np.median([x['time'] for x in prog['iterations']]))
        f.write(f"\nmedian_{maxChunkSize}_{nThreads}: {[str(x/1000000.0)+'ms' for x in medianTimes]}") #those times are in nanoseconds

# ------------------------------------------------------------------------------
# -----------------------------------The Main-----------------------------------
# ------------------------------------------------------------------------------
if __name__ == '__main__':
    argv      = sys.argv[1:]
    outputDir = os.getcwd() + "/output"
    nThreads  = 8
    inFiles   = []
    datas     = []

    #Read argv
    i = 0
    while i < len(argv):
        arg = argv[i]
        if not (arg.startswith("--") or arg.startswith("-")):
            inFiles.append(arg)
            i+=1
        else:
            while i < len(argv):
                arg = argv[i]

                if arg == "-h" or arg == "--help":
                    printDocAndExit()

                elif arg == "--output":
                    if i < len(argv)-1:
                        if argv[i+1][0] == '-':
                            print(f"Missing the directory path value of the '--output' parameter. {argv[i+1]} is not a correct value. Exiting.")
                            sys.exit(-1)
                        outputDir = argv[i+1]
                        i+=1
                    else:
                        print(f"Missing the directory path value of the '--output' parameter. Exiting.")
                        sys.exit(-1)

                elif arg == "--nThreads":
                    if i < len(argv)-1:
                        if argv[i+1][0] == '-':
                            print(f"Missing the maximum thread value of the '--nThreads' parameter. {argv[i+1]} is not a correct value. Exiting.")
                            sys.exit(-1)
                        nThreads = int(argv[i+1])
                        if nThreads <= 0:
                            print(f"nThreads should be greater than 0. {argv[i+1]} is an invalid parameter")
                        i+=1
                    else:
                        print(f"Missing the nThreads value of the '--nThreads' parameter. Exiting.")
                        sys.exit(-1)
                else:
                    print(f"Unknown parameter {arg}")
                    printDocAndExit()
                i+=1

    #Check that the input file exists and read it
    for inFile in inFiles:
        assert os.path.isfile(inFile)
        with open(inFile, 'r') as f:
            datas.append(json.load(f))

    assert len(inFiles)%nThreads == 0

    #Create the output directory, if necessary
    if not os.path.exists(outputDir):
        os.mkdir(outputDir)

    printTime(datas, nThreads, outputDir)
