from cycler import cycler
import pdb
import sys
import os
import json
import math
import scipy
import pandas as pd
import numpy as np
import numpy.random 
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.ticker as ticker
import functools

import random

random.seed(0)
numpy.random.seed(0)

def printDocAndExit():
    """
    Print the documentation of how to use the script and exit the program.
    """
    print("Run ./stability.py inFile1 [...inFiles] [--output directoryPath]")
    print("                     [-h] [--help]")
    print("inFiles: the json files to analyse from the scalability point of view. See benchmark.py outputs.")
    print("--output: output directory to save the different PDF data analyses results. Default: ${pwd}/output/")
    print("-h --help: Print this documentation and exit.")

    sys.exit(-1)

def printNodeDistributions(datas, outputDir):
    """
    @datas: The list of the benchmark's results.
    @outputDir: The output directory to save distribution.pdf in.
    """

    columnsProg = ['id', 'count', 'stableIT', 'stableITNormalized', 'depth', 'method', 'threshold', 'chunkSize', 'nbEvents']
    nodeProg    = pd.DataFrame(columns=columnsProg)

    #Gather the values we are interested in
    for data in datas:
        prog  = [x for x in data["Progressive"] if len(x['alignSequence']) == 0]
        nbLowLevel = data["NonProgressive"][0]["nbLowLevel"]
        for x in prog:
            for stats in x["statsPerNode"]:
                if stats['sortingMethod'] == 'IDs':
                    continue
                for node in stats['data']:
                    nodeProg = pd.concat([nodeProg, pd.DataFrame([[node['id'], node["count"], node["stableAtIteration"],\
                                          float(node['stableAtIteration']/(len(x['iterations']))), \
                                          len(node["path"]), stats['sortingMethod'], x['threshold'], x['chunk_size'], nbLowLevel]], columns=columnsProg)], ignore_index=True)

    nodeProg = nodeProg.astype({'count': 'int', 'id': 'int', 'depth': 'int', 'chunkSize': 'int', 'threshold': 'int', 'nbEvents': 'int', 'stableIT': 'int', 'stableITNormalized': 'float'})
    nodeProg = nodeProg[nodeProg['depth'] <= 5]
    nodeProg["depth"] = nodeProg["depth"]-1 #Start at depth == 0
    byCountStats      = nodeProg[nodeProg['method'] == "Count"]
    byHysteresisStats = nodeProg[nodeProg['method'] == "Hysteresis"]
    print(byCountStats)
    pairwiseStats     = pd.merge(byCountStats, byHysteresisStats, on=["id", "count", "chunkSize", "threshold", "depth", "nbEvents"], suffixes=["_x", "_y"])
    pairwiseStats["stableITNormalized"] = pairwiseStats["stableITNormalized_x"] - pairwiseStats["stableITNormalized_y"]
    pairwiseStats["method"]             = "Count-Hysteresis"
    
    maxStableIT = functools.reduce(lambda x, y : max(x, y), nodeProg["stableIT"])
    uniqueNbEvents = pd.unique(nodeProg["nbEvents"])
    uniqueSort     = pd.unique(nodeProg["method"])

    uniqueNbEvents.sort()
    uniqueSort.sort()

    print("####### NODES PER DEPTH ########")
    width = (len(datas)-1.0)/len(datas) if len(datas) > 1 else 0.8
    fig = plt.figure()
    fig.set_tight_layout(True)
    ax1 = fig.add_subplot(aspect="auto")
    multiplier = 0

    nodePerDepth = nodeProg[(nodeProg['method']=='Count')]
    maxValue     = 0
    for nbEvents, subDataNbEvents in nodePerDepth.groupby('nbEvents'):
        values = []
        depths = []
        offset = multiplier*width
        for depth, subData in subDataNbEvents.groupby('depth'):
            maxChunkSize = int(max(subData['chunkSize']))
            subData      = subData[(subData['chunkSize'] == maxChunkSize) & (subData['threshold'] == 0)] #Avoid duplications
            values.append(len(subData.index))
            depths.append(depth+offset)

        maxValue = max(maxValue, max(values))
        bars = ax1.bar(depths, values, width, label=f"nbEvents={nbEvents:,}")
        ax1.bar_label(bars, padding=1)
        multiplier+=1

    if len(datas) > 1:
        ax1.legend(loc='upper left', ncols=4)
        ax1.set_xticks(np.arange(0, 5) + (len(datas)-1.0)/2.0*width, np.arange(0,5))
        ax1.set(axisbelow=True,
            title='',
            xlabel='Depth',
            ylabel='Number of nodes',
    )
    ax1.set_ylim(0, maxValue+40)
    ax1.yaxis.set_major_locator(ticker.MultipleLocator(40))
    fig.set_size_inches(10, 3)
    outputFile = f"{outputDir}/nbNodesPerDepth.pdf"
    print(f"Saving {outputFile}...\n")
    fig.savefig(outputFile, format="pdf", transparent=True)

    print("####### STABILITY IN COUNT COMPUTATION ########")
    for groupByInd,groupByData in nodeProg.groupby(['chunkSize', 'threshold']):
        histograms = []
        maxCount   = 0

        chunkSize, threshold = groupByInd
        for (nbEvents,method),data in groupByData.groupby(['nbEvents', 'method']):
            histogram = [0 for x in range(maxStableIT+1)]
            for index, x in data.iterrows():
                histogram[x['stableIT']] += 1
            histograms.append({"method": str(method), "nbEvents": nbEvents, "value": histogram})
            print(histogram)
            maxCount = max(maxCount, functools.reduce(lambda x, y : max(x, y), histogram))

        fig = plt.figure()
        fig.set_tight_layout(True)
        ax1 = fig.add_subplot(aspect="auto")
        ax1.xaxis.set_major_locator(ticker.MultipleLocator(1))
        ax1.set(axisbelow=True,
                title='',
                xlabel='Iteration',
                ylabel='Number of nodes stable',
        )
        ax1.set_ylim(ymin=0, ymax=maxCount+1)
        ax1.set_xlim(xmin=0, xmax=maxStableIT+1)

        for ind, hist in enumerate(histograms):
            data = hist["value"]
            ax1.plot(range(len(data)), data, label=hist['method'])


        ax1.legend(loc='upper right')
        ax1.grid()
        outputFile = f"{outputDir}/stability_chunkSize_{chunkSize}_threshold_{threshold}.pdf"
        print(f"Saving {outputFile}...\n")
        fig.savefig(outputFile, format="pdf", transparent=True)

    print('####### STABILITY PER DEPTH VALUES #######')

    def drawErrorbarsFigure(fig, ax, data, legend, offset=0):
        """
        Helper function to analyze and draw the statistics of the stability of the current dataframe.
        The dataframe should associate a stableAt column with a depth column. This function will not
        perform any filtering on the original dataframe.
        """
        depths = pd.unique(data['depth'])
        values = []
        confidenceIntervals = [[], []]

        for depth, depthSubStats in data.groupby("depth"):
            value     = np.mean(depthSubStats['stableITNormalized'])
            if np.all(depthSubStats['stableITNormalized'] == value):
                confidenceInterval = [value, value]
            else:
                bootstrap = scipy.stats.bootstrap((depthSubStats['stableITNormalized'],), np.mean, n_resamples=10000, confidence_level=0.95, method='bca', random_state=0)
                confidenceInterval = bootstrap.confidence_interval
            values.append(value)
            confidenceIntervals[0].append(value-confidenceInterval[0])
            confidenceIntervals[1].append(confidenceInterval[1]-value)
            
            print(f"Method: {legend}, depth: {depth}, value: {value}, confidence_intervals: {confidenceInterval}")
        ax.errorbar(x=values, y=depths+offset, xerr=confidenceIntervals, fmt='o', label=legend)
        ax.set_yticks(depths)
        ax.set_yticklabels(depths)

    fig = plt.figure()
    fig.set_tight_layout(True)
    fig.set_size_inches(10, 2)
    ax1 = fig.add_subplot(aspect="auto")
    ax1.spines['top'].set_visible(False)
    ax1.spines['right'].set_visible(False)
    ax1.spines['bottom'].set_visible(False)
    ax1.spines['left'].set_visible(False)
    ax1.set(axisbelow=True,
           title='',
           xlabel='Iteration Ratio (between 0.0 and 1.0)',
           ylabel='Depth',
    )
    for method, data, offset in zip(['Count', 'Hysteresis'], [byCountStats, byHysteresisStats], [-0.1, +0.1]):
        drawErrorbarsFigure(fig, ax1, data, method, offset)
    ax1.legend(fancybox=True)
    outputFile = f"{outputDir}/stability_CI_node_per_depth.pdf"
    print(f"Saving {outputFile}...")
    fig.savefig(outputFile, format="pdf", bbox_inches='tight')

    #Draw the pairwise comparison figure
    fig = plt.figure()
    fig.set_tight_layout(True)
    fig.set_size_inches(10, 2)
    ax1 = fig.add_subplot(aspect="auto")
    ax1.spines['top'].set_visible(False)
    ax1.spines['right'].set_visible(False)
    ax1.spines['bottom'].set_visible(False)
    ax1.spines['left'].set_visible(False)
    ax1.set(axisbelow=True,
            title='',
            xlabel='Iteration Ratio (between 0.0 and 1.0)',
            ylabel='Depth',
    )
    drawErrorbarsFigure(fig, ax1, pairwiseStats, "Count-Hysteresis")
    ax1.axvline(x=0.0, color="black")
    ax1.legend(fancybox=True)
    outputFile = f"{outputDir}/PWstability_CI_node_per_depth.pdf"
    print(f"Saving {outputFile}...\n")
    fig.savefig(outputFile, format="pdf", bbox_inches='tight')

# ------------------------------------------------------------------------------
# -----------------------------------The Main-----------------------------------
# ------------------------------------------------------------------------------
if __name__ == '__main__':
    argv      = sys.argv[1:]
    outputDir = os.getcwd() + "/output"
    inFiles   = []
    datas     = []

    #Read argv
    i = 0
    while i < len(argv):
        arg = argv[i]
        if not (arg.startswith("--") or arg.startswith("-")):
            inFiles.append(arg)
            i+=1
        else:
            while i < len(argv):
                arg = argv[i]

                if arg == "-h" or arg == "--help":
                    printDocAndExit()

                elif arg == "--output":
                    if i < len(argv)-1:
                        if argv[i+1][0] == '-':
                            print(f"Missing the directory path value of the '--output' parameter. {argv[i+1]} is not a correct value. Exiting.")
                            sys.exit(-1)
                        outputDir = argv[i+1]
                        i+=1
                    else:
                        print(f"Missing the directory path value of the '--output' parameter. Exiting.")
                        sys.exit(-1)
                else:
                    print(f"Unknown parameter {arg}")
                    printDocAndExit()
                i+=1

    #Check that the input file exists and read it
    for inFile in inFiles:
        assert os.path.isfile(inFile)
        with open(inFile, 'r') as f:
            datas.append(json.load(f))

    #Create the output directory, if necessary
    if not os.path.exists(outputDir):
        os.mkdir(outputDir)

    plt.rcParams['axes.prop_cycle'] = cycler(color=cm.Dark2.colors)
    printNodeDistributions(datas, outputDir)
