#include <iostream>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <json/value.h>
#include <json/reader.h>
#include "pparser.hpp"

class ContextTest : public testing::Test {
    protected:
        ContextTest() : eventTypes({"fivealpha", "alphabloc", "phyto", "surgery", "interruption", "no treatment", "death"}) {} //Death should be added in load
        void SetUp() override {
            ctx.load("./tests");
        }

        const uint64_t dataTypes[5]         = {(1U << 0), //fivealpha
                                               (1U << 3), //surgery
                                               (1U << 1), //alphabloc
                                               (1U << 5), //no treatment
                                               (1U << 1) | (1U << 2)}; //alphabloc + phyto
        const char*    diseases[2]          = {"Diabete", "Hypertension"};
        const size_t   nbTotalDiseases      = sizeof(diseases)/sizeof(diseases[0]);
        const size_t   nbIlledPerDisease[2] = {2, 1};

        const size_t   nbDataTypes          = sizeof(dataTypes)/sizeof(dataTypes[0]);
        const size_t   nbEvents             = 11+1-2; //eleven entries + one no treatment - (alphabloc and phyto are merged)*2.
        const size_t   nbLowLevelEvents     = 11; //eleven entries
        const size_t   nbPatients           = 3; //Testdata.csv describes three users
        const size_t   rootNbChildren       = 2; //Surgery + alphabloC
        
        context ctx;
        std::vector<std::string> eventTypes;
};

//Test event names
TEST_F(ContextTest, read_dicts){
    //Those are stored in tests/data.map
    for(size_t i = 0; i < eventTypes.size(); i++)
        EXPECT_EQ(eventTypes[i], ctx.event_name(1UL << i));

    //Special cases where the MSB == 1 (i.e., negative numbers)
    EXPECT_EQ(ctx.event_name(context::dual_sequence_type), "dual_sequence");
    EXPECT_EQ(ctx.event_name(context::before_type)       , "before");
    EXPECT_EQ(ctx.event_name(context::after_type)        , "after");

    //No event
    EXPECT_EQ(ctx.event_name(0), "");

    //Combined event
    EXPECT_EQ(ctx.event_name(0b11), eventTypes[0] + " " + eventTypes[1]);

    //Hierarchical types
    const TypeHierarchy& type_hierarchy = ctx.type_hierarchy;
    EXPECT_EQ(type_hierarchy.nodes.size(), 1 /* root */ + eventTypes.size() + 2 /* medication + pause */ + 3 /* alphabloc phyto; fivealpha alphabloc; fivealpha alphabloc phyto */);
    EXPECT_EQ(type_hierarchy.nodes.size()-1/* remove root */, ctx.valid_type_combination.size() - 3 /* before, after, dual_sequence*/);
    EXPECT_EQ(type_hierarchy.get_root()->get_children().size(), 4); //medication + pause + surgery + death
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("medication")->get_parent(), type_hierarchy.node_id(type_hierarchy.get_root()));
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("pause")->get_parent()     , type_hierarchy.node_id(type_hierarchy.get_root()));
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("surgery")->get_parent()   , type_hierarchy.node_id(type_hierarchy.get_root()));
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("death")->get_parent()     , type_hierarchy.node_id(type_hierarchy.get_root()));
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("medication")->get_children().size(), 6);
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("pause")->get_children().size(), 2);
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("surgery")->get_children().size(), 0);
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("death")->get_children().size(), 0);

    uint64_t medication_id = type_hierarchy.node_id(type_hierarchy.find_first_node_by_name("medication"));
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("alphabloc")->get_parent()                , medication_id);
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("phyto")->get_parent()                    , medication_id);
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("fivealpha")->get_parent()                , medication_id);
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("alphabloc phyto")->get_parent()          , medication_id);
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("fivealpha alphabloc")->get_parent()      , medication_id);
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("fivealpha alphabloc phyto")->get_parent(), medication_id);
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("alphabloc")->get_children().size()                , 0);
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("phyto")->get_children().size()                    , 0);
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("fivealpha")->get_children().size()                , 0);
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("alphabloc phyto")->get_children().size()          , 0);
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("fivealpha alphabloc")->get_children().size()      , 0);
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("fivealpha alphabloc phyto")->get_children().size(), 0);
}

TEST_F(ContextTest, valid_event) {
    //Test that every singular event is a valid valid
    for(size_t i = 0; i < eventTypes.size(); i++)
        EXPECT_TRUE(ctx.is_valid_type_combination(1UL << i));

    //Test that everything including death, except death along, is invalid
    //Same for "surgery", "interruption", and "no treatment"
    const size_t deathIdx        = 6;
    const size_t surgeryIdx      = 3;
    const size_t interruptionIdx = 4;
    const size_t noTreatmentIdx  = 5;
    const size_t invalidComb[] = {deathIdx, surgeryIdx, interruptionIdx, noTreatmentIdx};
    
    for(size_t i = 0; i < eventTypes.size(); i++)
        for(size_t j : invalidComb)
            if(i != j) {
                EXPECT_FALSE(ctx.is_valid_type_combination((1UL << i) | (1UL << j)));
            }

    //Test that special eventTypes are valid eventTypes
    const auto specialEvents = {context::dual_sequence_type, context::before_type, context::after_type};
    for(auto i : specialEvents)
        EXPECT_TRUE(ctx.is_valid_type_combination(i));
}


TEST_F(ContextTest, clear) {
    size_t nbNodes = ctx.graph.nodes.size();
    EXPECT_EQ(nbNodes, 1); //root

    Node* n = ctx.graph.new_node();
    EXPECT_EQ(n, &ctx.graph.nodes.back());
    EXPECT_EQ(nbNodes+1, ctx.graph.nodes.size());

    ctx.align_sequence_set({dataTypes[0], dataTypes[1]});

    ctx.clear();
    EXPECT_EQ(nbNodes, ctx.graph.nodes.size());
    EXPECT_EQ(ctx.align_sequence_get().size(), 2); //align_sequence should not get cleared
                            
    ctx.process_all();
    ctx.update_view_data();

    //Test a clear after having processed the context
    ctx.clear();
    EXPECT_EQ(ctx.graph.nodes.size(), 1); //Root
    EXPECT_EQ(ctx.graph.nodes[0].children.size(), 0);
    EXPECT_EQ(ctx.graph.nodes[0].subcount.size(), 0);
    EXPECT_EQ(ctx.graph.nodes[0].count, 0);
}

TEST_F(ContextTest, align_sequence) {
    //First set the align sequence and process it
    ctx.align_sequence_set({dataTypes[2], dataTypes[2]});
    EXPECT_EQ(ctx.align_sequence.size(), 2);
    ctx.process_all();
    ctx.update_view_data();
    //This sequence is invalid. 
    EXPECT_EQ(ctx.graph.nodes[0].count, 0);

    //Perform another align_sequence that is valid
    std::vector<uint64_t> align_sequence = {dataTypes[2], dataTypes[1]};
    ctx.align_sequence_set(align_sequence);
    ctx.clear();
    ctx.process_all();
    ctx.update_view_data();

    //Check the outcome
    Node& root = ctx.graph.nodes[0];

    //Test the counting
    EXPECT_EQ(root.count, 2*2); //Only two patients have alphabloc + surgery (in that order). We, however, have "before" and "after" trees, hence 2*2.

    //Test that all types are there
    const uint64_t types[] = {context::before_type,
                              context::after_type,
                              context::dual_sequence_type,
                              (1U << 1), //alphabloc
                              (1U << 3), //surgery
                              (1U << 1) | (1U << 2)}; //alphabloc + phyto
    const size_t nbTypes = sizeof(types)/sizeof(types[0]);

    EXPECT_EQ(root.subcount.size(), nbTypes); //Both patients are similar: they both have alphabloc -> surgery -> alphabloc+phyto. We then need "before", "after", and "dual sequence" types
    for(auto t : types)
        EXPECT_TRUE(root.subcount.find(t) != root.subcount.end());
    
    //Check that we do have dual_sequence -> before
    //                                    -> after
    EXPECT_EQ(root.children.size(), 1); //dual_sequence
    EXPECT_EQ((*root.children.begin()).first, context::dual_sequence_type);

    Node& dual_sequence = ctx.graph.nodes[(*root.children.begin()).second];
    EXPECT_EQ(dual_sequence.children.size(), 2); //before + after
    for(uint64_t type : {context::before_type, context::after_type}){
        std::cout << type << std::endl;
        EXPECT_TRUE(dual_sequence.children.find(type) != dual_sequence.children.end());
    }

    //Check that the printing shows the sequence
	Json::Reader jReader;
	Json::Value  jRoot;
    ctx.prune_threshold = 0;
    std::string s = ctx.print_string(true);
    ASSERT_TRUE(jReader.parse(s.c_str(), jRoot));

    EXPECT_EQ(jRoot["align_sequence"].size(), 2); 
    EXPECT_EQ(jRoot["align_sequence"][0].asInt(), align_sequence[0]);
    EXPECT_EQ(jRoot["align_sequence"][1].asInt(), align_sequence[1]);
}

TEST_F(ContextTest, progressive) {
    ctx.prune_threshold = 0;
    ctx.process_all();
    ctx.update_view_data();
    const std::string original_output = ctx.print_string(true);
    for(auto strat : {ProgressiveStrategy::QUANTUM_TIME, ProgressiveStrategy::CHUNK_PATIENTS}) {
        ctx.progressive_strategy = strat;
        ctx.chunk_size = 2;
        uint32_t i = 0;
        do{
            ctx.process_next((i++)==0);
            ctx.update_view_data();
        }while(!ctx.process_done()); 
        EXPECT_EQ(original_output, ctx.print_string(true));        
    }
}

TEST_F(ContextTest, align_sequence_first_last) {
    for(const int i : {-1, 0}) {
        //Perform an align_sequence from a given position for surgery
        ctx.align_sequence_set({dataTypes[1]});
        ctx.align_sequence_by_set(i); //last occurence
        EXPECT_EQ(ctx.align_sequence_by_get(), i);
        ctx.clear();
        ctx.process_all();
        ctx.update_view_data();

        //Check the outcome
        Node& root = ctx.graph.nodes[0];

        //Test the counting.
        EXPECT_EQ(root.count, 3*2); // All patients have surgery in their sequence. We, however, have "before" and "after" trees, hence 3*2.

        if(i == -1) {
            //Check that a surgery is BEFORE another surgery (patient ID 2)
            Node& beforeNode = ctx.graph.nodes[ctx.graph.nodes[ctx.graph.nodes[root.children[context::dual_sequence_type]].children[context::before_type]].children[(1U << 3)]];
            Node& afterNode = ctx.graph.nodes[ctx.graph.nodes[ctx.graph.nodes[root.children[context::dual_sequence_type]].children[context::after_type]].children[(1U << 3)]];

            EXPECT_TRUE(beforeNode.subcount.find(1U << 3) != beforeNode.subcount.end());
            EXPECT_EQ(beforeNode.subcount[(1U << 3)], 1+3); //3 because three people have surgery (the root is also part of the subcount).
            for(auto& it : afterNode.children) { //No child should have a surgery in their "after" tree
                Node& node = ctx.graph.nodes[it.second];
                EXPECT_TRUE(node.subcount.find(1U << 3) == node.subcount.end());
            }
        }
        else if(i == 0) {
            //Check that a surgery is BEFORE another surgery (patient ID 2)
            Node& beforeNode = ctx.graph.nodes[ctx.graph.nodes[ctx.graph.nodes[root.children[context::dual_sequence_type]].children[context::before_type]].children[(1U << 3)]];
            Node& afterNode = ctx.graph.nodes[ctx.graph.nodes[ctx.graph.nodes[root.children[context::dual_sequence_type]].children[context::after_type]].children[(1U << 3)]];

            EXPECT_TRUE(afterNode.subcount.find(1U << 3) != afterNode.subcount.end());
            EXPECT_EQ(afterNode.subcount[(1U << 3)], 1+3); //3 because three people have surgery (the root is also part of the subcount).
            for(auto& it : beforeNode.children) { //No child should have a surgery in their "before" tree
                Node& node = ctx.graph.nodes[it.second];
                EXPECT_TRUE(node.subcount.find(1U << 3) == node.subcount.end());
            }
        }
    }
}

TEST_F(ContextTest, push_info) {
    Stack stack;
    Info info;
    info.type = 0x01; //Corresponds to eventTypes[1]
    info.days = 10;   //Event started at day == 10
    ctx.push_info(stack, info);
    EXPECT_EQ(stack.back(), Event(info));

    //Test merging rule with the same info.
    //As the type and days coincide, this info will be merged with the previous event
    Info info2 = info;
    info2.days = 20; //Same event but at day == 20 --> 10 days of events in total
    ctx.push_info(stack, info2);
    EXPECT_EQ(stack.size(), 1);
    EXPECT_EQ(stack.back().end_date - stack.back().start_date, 10);
    EXPECT_EQ(stack.back().start_date, info.days);

    //Merge two info with different types but similar days
    info2.type = 2;
    ctx.push_info(stack, info2);
    EXPECT_EQ(stack.size(), 1);
    EXPECT_EQ(stack.back().end_date - stack.back().start_date, 10);
    EXPECT_EQ(stack.back().type, (1 << 2) + (1 << 1));

    //Merge one info with type == 1 with a previous event which has type == (1 && 2)
    info2.days = 30;
    info2.type = 1;
    ctx.push_info(stack, info2);
    EXPECT_EQ(stack.size(), 1);
    EXPECT_EQ(stack.back().end_date - stack.back().start_date, 20); //Normally the end_date corresponds to info2.days == 30
    EXPECT_EQ(stack.back().type, (1 << 2) + (1 << 1));

    //Put an info with a type different than before, and for which the day is far away
    Info info3 = info;
    info3.days = 50; // > info3.days - info2.days > 15
    ctx.push_info(stack, info3);

    EXPECT_EQ(stack.size(), 2);
    EXPECT_EQ(stack.back().end_date - stack.back().start_date, 0);
    EXPECT_EQ(stack.back().start_date, info3.days);

    //TODO test the other cases
}

TEST_F(ContextTest, process) {
    const auto checkDiseaseInegality = [&](const auto& self, Node& node) -> void {
        for(const auto& it : node.children) {
            Node& child = ctx.graph.nodes[it.second];
            for(uint32_t i = 0; i < nbTotalDiseases; i++) {
                EXPECT_LE(child.indi_diseases[i], node.indi_diseases[i]);
            }
            self(self, child);
        }
    };

    ctx.process_all();
    ctx.update_view_data();
    EXPECT_TRUE(ctx.process_done());
    Node& root = ctx.graph.nodes[0];

    //Test the counting
    EXPECT_EQ(root.count, nbPatients); //Three users
    EXPECT_EQ(root.subcount.size(), nbDataTypes);

    //Test that all types are there
    for(size_t i = 0; i < nbDataTypes; i++)
        EXPECT_TRUE(root.subcount.find(dataTypes[i]) != root.subcount.end());

    //Test that we have the correct number of registered event
    size_t totalSubcount = 0;
    for(auto& it : root.subcount)
        totalSubcount += it.second;
    EXPECT_EQ(totalSubcount, nbEvents);

    //Test that we have the correct number of registered diseases
    uint8_t nbDiseases = 0;
    for(const auto& it : root.indi_diseases)
        nbDiseases += (it > 0);
    EXPECT_EQ(nbDiseases, nbTotalDiseases);
    for(uint8_t i = 0; i < nbTotalDiseases; i++) {
        uint64_t diseaseKey = std::find(ctx.diseases.begin(), ctx.diseases.end(), std::string(diseases[i])) - ctx.diseases.begin();
        ASSERT_LT(diseaseKey, ctx.diseases.size());
        EXPECT_EQ(root.indi_diseases[diseaseKey], nbIlledPerDisease[i]); //Root contains, normally, all the diseases
    }
    checkDiseaseInegality(checkDiseaseInegality, root);

    Node& alphablocNode = ctx.graph.nodes[root.children[1U << 1]]; //Get alphabloc subtree
    uint64_t diseaseKey = std::find(ctx.diseases.begin(), ctx.diseases.end(), std::string(diseases[0])) - ctx.diseases.begin(); //Get Diabete ID
    EXPECT_EQ(alphablocNode.indi_diseases[diseaseKey], 2); //Two patients have Diabete

    Node& surgeryNode = ctx.graph.nodes[root.children[1U << 4]]; //Get surgery subtree
    diseaseKey        = std::find(ctx.diseases.begin(), ctx.diseases.end(), std::string(diseases[1])) - ctx.diseases.begin(); //Get Diabete ID
    EXPECT_EQ(surgeryNode.indi_diseases[diseaseKey], 1); //One patient has Diabete
}

TEST_F(ContextTest, print) {
    ctx.max_filter_depth = 1;
    ctx.process_all();
    ctx.update_view_data();

    //Those filters are at the printing time -> it can be done AFTER the processing
    ctx.prune_threshold  = 0;
    ctx.max_filter_depth = 10;
	Json::Reader jReader;
	Json::Value  jRoot;
    std::string s = ctx.print_string(true);

    //Check that the JSON compiles correctly
    ASSERT_TRUE(jReader.parse(s.c_str(), jRoot));

    auto checkRoot = [&](){
        //The root should have the same value as the number of parsed persons
        EXPECT_EQ(jRoot["total_count"].asInt(), nbPatients);
        EXPECT_EQ(jRoot["nb_processed"].asInt(), nbPatients);
        EXPECT_EQ(jRoot["count"].asInt(), nbPatients);

        size_t totalSubcount = 0;
        for(const auto& key : dataTypes) {
            const std::string eventName = ctx.event_name(key);
            EXPECT_TRUE(jRoot["subcount"].isMember(eventName));
                totalSubcount += jRoot["subcount"][eventName].asInt();
        }
        EXPECT_EQ(totalSubcount, nbEvents);

        //Check that every type is set. Greater than because of composed types that we do not check
        //TODO check composed types
        EXPECT_GT(jRoot["types"].size(), eventTypes.size());
        auto jrootTypes = jRoot["types"];
        for(const auto& ev : eventTypes)
            EXPECT_TRUE(std::find_if(jrootTypes.begin(), jrootTypes.end(), [&](const auto& x){return x["name"] == ev;}) != jrootTypes.end());
    };
    checkRoot();

    EXPECT_EQ(jRoot["children"].size(), rootNbChildren);
    auto alphabloc_node = std::find_if(jRoot["children"].begin(), jRoot["children"].end(), [&](const auto& x){return x["name"] == "alphabloc";});
    auto surgery_node   = std::find_if(jRoot["children"].begin(), jRoot["children"].end(), [&](const auto& x){return x["name"] == "surgery";});
    EXPECT_NE(alphabloc_node, jRoot["children"].end());
    EXPECT_NE(surgery_node,   jRoot["children"].end());
    EXPECT_EQ((*alphabloc_node)["children"].size(), 1);
    EXPECT_EQ((*surgery_node)["children"].size(), 1);


    //Change prune_threshold
    ctx.prune_threshold  = 2;
    s = ctx.print_string(true);
    ASSERT_TRUE(jReader.parse(s.c_str(), jRoot));
    checkRoot();

    EXPECT_EQ(jRoot["children"].size(), 1); //Only alphabloc
    alphabloc_node = std::find_if(jRoot["children"].begin(), jRoot["children"].end(), [&](const auto& x){return x["name"] == "alphabloc";});
    surgery_node   = std::find_if(jRoot["children"].begin(), jRoot["children"].end(), [&](const auto& x){return x["name"] == "surgery";});
    EXPECT_NE(alphabloc_node, jRoot["children"].end());
    EXPECT_EQ(surgery_node,   jRoot["children"].end());
    EXPECT_EQ((*alphabloc_node)["children"].size(), 1);


    //Change depth
    ctx.max_filter_depth = 1;
    s = ctx.print_string(true);
    ASSERT_TRUE(jReader.parse(s.c_str(), jRoot));
    checkRoot();

    EXPECT_EQ(jRoot["children"].size(), 1); //Only alphabloc
    alphabloc_node = std::find_if(jRoot["children"].begin(), jRoot["children"].end(), [&](const auto& x){return x["name"] == "alphabloc";});
    surgery_node   = std::find_if(jRoot["children"].begin(), jRoot["children"].end(), [&](const auto& x){return x["name"] == "surgery";});
    EXPECT_NE(alphabloc_node, jRoot["children"].end());
    EXPECT_EQ(surgery_node,   jRoot["children"].end());
    EXPECT_EQ((*alphabloc_node)["children"].size(), 0); //Depth == 1 -> Nothing after alphabloc
}

TEST_F(ContextTest, low_level) {
    ctx.process_all();
    ctx.update_view_data();
    EXPECT_EQ(ctx.get_nb_low_level_events(), nbLowLevelEvents);
    uint32_t sumLowLevel = 0;
    for(uint32_t i = 0; i < 3; i++)
        sumLowLevel += ctx.get_nb_low_level_events(i);
    EXPECT_EQ(sumLowLevel, nbLowLevelEvents);

    //Those values are the number of entries in testdata.csv
    EXPECT_EQ(ctx.get_nb_low_level_events(0), 4);
    EXPECT_EQ(ctx.get_nb_low_level_events(1), 4);
    EXPECT_EQ(ctx.get_nb_low_level_events(2), 3);
}

TEST_F(ContextTest, low_level_hierarchy) {
    ctx.process_all();
    ctx.update_view_data();
    ctx.aggregate_coarse_level_set(1);

    Node& root = ctx.output_graph.nodes[0];
    const auto medication = ctx.type_hierarchy.find_first_node_by_name("medication");

    //Test the counting
    EXPECT_EQ(root.count, nbPatients); //Three users
    const std::vector<std::string> types = {"medication", "surgery", "pause"};
    for(const auto& data_type : types)
        EXPECT_TRUE(root.subcount.find(ctx.type_hierarchy.find_first_node_by_name(data_type)->type_combination) != root.subcount.end());
    EXPECT_EQ(root.subcount.size(), types.size());

    EXPECT_EQ(root.children.size(), 2);

    Node& first_medication_node = ctx.output_graph.nodes[root.children[medication->type_combination]];
    EXPECT_EQ(first_medication_node.children.size(), 1);
    EXPECT_EQ(first_medication_node.durations.size(), 2);
}

TEST_F(ContextTest, duration_type) {
    std::vector<std::string> types = {"mean", "max", "min", "median"};
    EXPECT_EQ(ctx.duration_type_get(), "mean");
    ctx.duration_type_set("average");
    EXPECT_EQ(ctx.duration_type_get(), "mean");
    for(auto& t : types){
        ctx.duration_type_set(t);
        EXPECT_EQ(ctx.duration_type_get(), t);
    }
}

TEST_F(ContextTest, number_of_nodes) {
    ctx.process_all();
    ctx.update_view_data();
    EXPECT_EQ(ctx.get_nb_distinct_pathways(), 2);
    EXPECT_EQ(ctx.get_nb_useful_nodes(), 7); //alphabloc and phyto are merged, but we have a no_treatment that is added
}

TEST_F(ContextTest, print_histogram) {
    ctx.process_all();
    ctx.update_view_data();
    for(size_t i = 0; i < ctx.graph.nodes.size(); i++) {
        for(auto type : {HistogramToPrint::DURATION, HistogramToPrint::AGE}) {
            Json::Reader jReader;
            Json::Value  jRoot;
            std::string  jsonStr = ctx.print_histogram(type, ctx.graph, i);
            ASSERT_TRUE(jReader.parse(jsonStr.c_str(), jRoot));
        }
    }

    EXPECT_THAT([&]() {ctx.print_histogram(HistogramToPrint::DURATION, ctx.graph, ctx.graph.nodes.size());}, 
                testing::Throws<std::runtime_error>());

}

TEST_F(ContextTest, print_barchart) {
    ctx.process_all();
    ctx.update_view_data();
    for(size_t i = 0; i < ctx.graph.nodes.size(); i++) {
        Json::Reader jdiseaseReader;
        Json::Value  jdiseaseRoot;
        std::string  jsonStr = ctx.print_barchart(BarchartToPrint::DISEASE, i);

        //Check that the JSON compiles correctly
        ASSERT_TRUE(jdiseaseReader.parse(jsonStr.c_str(), jdiseaseRoot));
        EXPECT_EQ(jdiseaseRoot.size(), nbTotalDiseases);
    }

    EXPECT_THAT([&]() {ctx.print_barchart(BarchartToPrint::DISEASE, ctx.graph, ctx.graph.nodes.size());}, 
                testing::Throws<std::runtime_error>());
}

int main(int argc, char **argv) 
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
