#include <iostream>
#include <iomanip>
#include <random>
#include <algorithm>    // std::shuffle
#include <chrono>
#include "roaring.hh"

using namespace std;

int main() {
    std::default_random_engine generator;
    std::uniform_int_distribution<uint32_t> distribution(0,1U<<24);
    Roaring bitmap;
    const int loops = 100000;

    while (bitmap.cardinality() < loops) {
        uint32_t number = distribution(generator);
        bitmap.addChecked(number);
    }

    cout << "Generated " << bitmap.cardinality() << " values in bitmap" << endl;

    std::clock_t c_start = std::clock();
    auto t_start = std::chrono::high_resolution_clock::now();
    
    while (! bitmap.isEmpty()) {
        uint32_t rank = uint32_t(generator()) % bitmap.cardinality();
        uint32_t element;
        if (! bitmap.select(rank, &element)) {
            cerr << "Invalid rank " << rank 
                 << " for a bitmap of cardinality " << bitmap.cardinality() << endl;
        }
        if (! bitmap.removeChecked(element)) {
            cerr << "Invalid element " << element << endl;
        }
        // else {
        //     cout << element << endl;
        // }
    }
    std::clock_t c_end = std::clock();
    auto t_end = std::chrono::high_resolution_clock::now();
    std::cout << std::fixed << std::setprecision(2) << "CPU time used: "
              << 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC << " ms\n"
              << "Wall clock time passed: "
              << std::chrono::duration<double, std::milli>(t_end-t_start).count()
              << " ms\n";
    
    uint32_t indices[loops];
    for (size_t i = 0; i < loops; i++)
        indices[i] = i;

    c_start = std::clock();
    t_start = std::chrono::high_resolution_clock::now();

    shuffle(indices, indices+loops, std::default_random_engine(42));
    c_end = std::clock();
    t_end = std::chrono::high_resolution_clock::now();
    std::cout << std::fixed << std::setprecision(2) << "CPU time used: "
              << 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC << " ms\n"
              << "Wall clock time passed: "
              << std::chrono::duration<double, std::milli>(t_end-t_start).count()
              << " ms\n";    
    // for (auto x : indices)
    //     cout << x << endl;
    return 0;
}
