#ifndef  PPARSER_INC
#define  PPARSER_INC

// Progressive Parser
#include <cassert>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <exception>
#include <random>
#include <algorithm>    // std::shuffle
#include <limits>
#include <chrono>
#include <numeric>
#include "roaring.hh"

#include "event.hpp"
#include "histogram.hpp"
#include "barchart.hpp"
#include "mmap_mem.hpp"
using namespace roaring;

static const size_t       INIT_NODE_CAPACITY       = 200000; // number of pre allocated nodes. This is used to avoid unnecessary reallocation at the beginning of the algorithm
static const size_t       INIT_NB_INDI_PER_QUANTUM = 100000;
static const unsigned int MAX_DISEASES             = 24; //Currently: Diabete and Hypertension
 
/*----------------------------------------------------------------------------*/
/*----Constant to satisfy the CNIL requirements concerning aggregated data----*/
/*----------------------------------------------------------------------------*/
static const unsigned int K_ANONIMITY              = 10;
static const unsigned int L_DIVERSITY              = 2;
static const unsigned int R_REPRESENTATIVE         = 80;
static const unsigned int S_THRESHOLD              = 3;
typedef unsigned node_id;

enum class DiseaseFilterMode { YES, NO, BOTH }; //The different filtering mode for disease entries
enum class HistogramToPrint {AGE, DURATION}; //The different histograms ParcoursVis handles
enum class BarchartToPrint {DISEASE}; //The different barchats ParcoursVis handles
enum class ProgressiveStrategy {QUANTUM_TIME, CHUNK_PATIENTS}; //The different strategies to pause the progressive algorithm

/** \brief  Unused for the moment. This should help to read an individual in a specific order */
struct IndiState {
    unsigned int cursor_forward;
    unsigned int cursor_backward;
};

/** \brief  Encapsulation of a std::vector<unsigned>, useful for, e.g., Histograms.
 * If you modify the vector by any other means than what is presented in this class, please, set sorted to false*/
struct Values : public std::vector<unsigned> {
    bool sorted = true; /*!< Is the vector sorted? */

    /** \brief  Reset the whole vector */
    void clear() {
        resize(0);
        sorted = true;
    }

    /** \brief  Sort the data if it is not already sorted*/
    void sort() {
        if (!sorted) {
            std::sort(begin(), end());
            sorted = true;
        }
    }

    /** \brief  What is the minimum value of the vector?
     * \return    the minimum value presented in the vector */
    unsigned min() const {
        if (empty())
            return 0;
        if (sorted)
            return front();
        return *std::min_element(begin(), end());
    }

    /** \brief  What is the maximum value of the vector?
     * \return    the maximum value presented in the vector */
    unsigned max() const {
        if (empty())
            return 0;
        if (sorted)
            return back();
        return *std::max_element(begin(), end());
    }

    /** \brief What is the median value of the vector?
     * \return the median value presented in the vector */
    unsigned median() {
        if (empty())
            return 0;
        sort(); 
        if(size() % 2 == 1)
            return *(begin()+(size()-1)/2);
        return ((*(begin()+(size()/2-1))) + (*(begin()+size()/2)))/2;
    }

    /** \brief What is the mean value of the vector?
     * \return the mean value presented in the vector. The mean is truncated (i.e., this function returns a int) */
    unsigned mean() const {
        auto sum = std::accumulate(begin(), end(), 0);
        if (sum == 0)
            return 0;
        return sum/size();
    }

    void merge(const Values& v) {
        size_t old_size = size();
        insert(end(), v.begin(), v.end());
        if(v.sorted && sorted)
            std::inplace_merge(begin(), begin()+old_size, end());
        else
            sorted = false;
    }

    /** \brief Add a new value to the class. If the vector is already sorted, we perform a sort by insertion.
     * \param value the value to add
     * \param keep_sorted should we keep this vector sorted? Note that sort by insertion is heavy*/
    void add(unsigned value, bool keep_sorted=false) { 
        if(!sorted || !keep_sorted) {
            push_back(value);
            sorted = false;
        }
        else {
            auto it = std::lower_bound(begin(), end(), value);
            insert(it, value);
        }
    }
};

/** \brief  Type of a "Sequence" used to be aligned */
struct Sequence{
    uint64_t type; /*!< The type of the event*/
    uint64_t drug; /*!< TODO UNUSED the drug associated to the event "type"*/
};

/** \brief  The different types of aggregated methods we use to determine the final "duration" a distribution has*/
enum duration_type {
    duration_min,   /*!< Use the minimum value found in the distribution*/
    duration_max,   /*!< Use the maximum value found in the distribution*/
    duration_mean,  /*!< Use the mean of the distribution*/
    duration_median /*!< Use the median of the distribution*/
};

typedef histogram<unsigned>   Histogram;
typedef std::vector<Sequence> StackSequence;
typedef std::vector<Event>    Stack;
typedef std::vector<Values>   NodeValues;
typedef std::vector<barchart> NodeBarcharts;


struct context;
struct Graph;

/** \brief  Basic structure for our tree */
struct Node {
    unsigned                     count;    //How large is this node? (e.g., number of people)
    std::map<uint64_t, node_id>  children; //All the children. key == type, value == node ID
    std::map<uint64_t, unsigned> subcount; //Count the total of occurences per key (similar to children + this) this node is attached to   
    unsigned                     indi_diseases[MAX_DISEASES]; //Array of individuals having a particular disease
    Roaring                      indi_list;        // List of individual inside this node 
    Histogram                    ages;             //Distribution of ages associated to this node
    Values                       durations;        //Distribution of durations associated to this node

    /** \brief  Constructor. Initialize an empty need */
    Node() : count(0), ages(64, 0, 128) { clear(); }

    Node(Node&& mvt) = default;

    Node(const Node& cpy) = default;

    Node& operator=(const Node& cpy) = default;

    /** \brief  Has this node a child of type "index"?
     * \param index the type of the child to look for. See also the keys in Node::children.
     * \return  true if this node has a child typed "index", false otherwise */
    bool has_child(uint64_t index) const {
        return children.find(index) != children.end(); 
    }

    /** \brief  Reset the node to its default value */
    void clear() {
        count = 0;
        children.clear();
        subcount.clear();
        for(auto& r : indi_diseases)
            r = 0;
        indi_list.clear();
    }

    /** \brief Prepare this node for printing and filtering
     * We use this design paradigm for optimization purposes, especially w.r.t histograms and memory footprints (which are included in the context object)
     *
     * \param ctx the overall context object that owns this Node */
    void prepare(context& ctx); 

    /** \brief  Should this node be filtered out for printing? Call the method "prepare" before calling filter.
     * \param ctx the overall context object that owns this Node.
     * \return   true if this node should not be printed (i.e., because calling print), false otherwise. Note that this function IS NOT called by print, but should be called because calling it.
     */
    bool filter(context& ctx);

    /** \brief  Print this node in a JSON format. Call the method "prepare" before calling this method
     *
     * \param out the output stream where this node will be printed
     * \param type the type of this node (see also the keys in Node::children)
     * \param ctx the overall context object that own this Node
     * \param graph the Graph object where this Node is part of
     * \param indent the current indentation to use for the JSON object for better readability
     * \param cum_duration the cumulative duration (in, e.g., days) from root to this node. See also Info::days
     * \param print_histogram should we print all the histograms as well?
     * \return a reference to "out" */
    std::ostream& print(std::ostream& out,
                   uint64_t type,
                   context& ctx,
                   Graph& graph,
                   size_t indent=0,
                   unsigned cum_duration=0,
                   bool print_histogram=false,
                   size_t curdepth = 0);
};

struct Graph {
    Graph(size_t capacity = INIT_NODE_CAPACITY);

    Graph(Graph&& mvt) = default;

    Graph(const Graph& cpy) = default;

    Graph& operator=(const Graph& cpy) = default;

    /*\brief  Concatenate two graphs together
     * \param graph the graph to concatenate with 'this'
     * \return   a reference on 'this' */
    Graph& operator+=(const Graph& graph);

    std::vector<Node> nodes;

    /** \brief  Get the node ID of a node. Perform arithmetic pointer
     * \param node a pointer to the node to evaluate
     * \return   the node ID of the node*/
    node_id id(const Node * node) const {
        int id = (node - nodes.data());
        assert(id >= 0 && id < long(nodes.size()));
        return node_id(id);
    }

    /** \brief  Get the node ID of a node. Perform arithmetic pointer
     * \param node the node to evaluate
     * \return   the node ID of the node*/
    node_id id(const Node& node) const {
        return id(&node);
    }

    /** \brief Create a new node
     * \return the node newly created*/
    Node * new_node() {
        nodes.emplace_back();
        return &nodes.back();
    }

    Node* get_root() {
        return &nodes.front();
    }

    const Node* get_root() const {
        return &nodes.front();
    }

    void clear() {
        nodes.clear();
        new_node();
    }

    void insert_sequence(const Stack& stack, const IndiInfo* info=nullptr, size_t indi_cursor=-1, bool is_special_sequence=true, bool verbose=false);

    std::ostream& print(std::ostream& out,
                   context& ctx,
                   size_t indent=0,
                   bool print_histogram=false);
};

struct TypeHierarchy;

/** \brief  A node in the "Type Hierarchy" structure. To be used with TypeHierarchy. */
struct TypeHierarchyNode {
    friend struct TypeHierarchy;

    /** \brief  The "NO_PARENT" constant value to say that a node has no particular parent. */
    static constexpr uint64_t NO_PARENT = (uint64_t)-1;

    private:
        /** \brief  The list of children this node has */
        std::vector<uint64_t> children;  

        /** \brief  The parent of this node. Normally, for a given hierarchy, only the ROOT has no parent */
        uint64_t              parent         = NO_PARENT;
    public:
        /** \brief  What is the event type this node refers to? Note that "root" has no valid type_combination*/
        uint64_t              type_combination = 0;

        /** \brief  The number of occurences this type appears in the Graph */
        uint64_t              nb_occurences = 0; 

        /** \brief  The name of this node, e.g., the name of the type it represents */
        std::string name;

        /** \brief  Constructor
         * \param _name the name of this node.*/
        TypeHierarchyNode(const std::string& _name="") : name(_name) {}
        TypeHierarchyNode(TypeHierarchyNode&& mvt) = default;
        TypeHierarchyNode(const TypeHierarchyNode& cpy) = default;
        TypeHierarchyNode& operator=(const TypeHierarchyNode& cpy) = default;

        /** \brief  Get the parent node ID
         * \return   the parent node ID. Check with "NO_PARENT" to know if this node as a parent or not. */
        uint64_t get_parent() const {return parent;}

        /** \brief Get the node IDs of all the children linked with this node
         * \return   the list of children */
        const std::vector<uint64_t>& get_children() const {return children;}

        std::ostream& print(const TypeHierarchy& hierarchy, std::ostream& out, unsigned ind) const;
};

/** \brief  Handle the hierarchy of types to handle coarse/grain aggregation 
 * All changes in the hierarchy or when reading it transversally (parent / children) should use this class as a proxy */
struct TypeHierarchy {
    /** \brief  The nodes of the hierarchy */
    std::vector<TypeHierarchyNode> nodes;

    /** \brief  Constructor, initialize the hierarchy */
    TypeHierarchy() {
        nodes.emplace_back("root");
    }

    TypeHierarchy(TypeHierarchy&& mvt) = default;
    TypeHierarchy(const TypeHierarchy& cpy) = default;
    TypeHierarchy& operator=(const TypeHierarchy& cpy) = default;

    void clear() {
        nodes.clear();
        nodes.emplace_back("root");
    }

    /** \brief  Add a new node to the hierarchy and returns its pointer value
     * \param parent of the newly created node. It cannot be nullptr as, except for the root, every child should have a parent.
     * \param name the name of the node to create.
     * \return  The newly added node. */
    TypeHierarchyNode* new_node(TypeHierarchyNode* parent, const std::string& name="") {
        //emplace_back may change the pointer value and thus invalidate the old pointer...
        uint64_t parent_id = node_id(parent);
        nodes.emplace_back(name);
        parent = get_node(parent_id);
        if(parent)
            set_relationship(parent, &nodes.back());
        return &nodes.back();
    }

    /*\brief  Set the relationship between two nodes 
     * \param parent the parent of the relationship (cannot be nullptr)
     * \param child the child of the relationship
     * Note that a child can only have one parent and must have a parent, except for the root. The old relationship this child may have had would then be removed*/
    void set_relationship(TypeHierarchyNode* parent, TypeHierarchyNode* child) {
        size_t parentID = node_id(parent);
        size_t childID  = node_id(child);

        assert(parentID < nodes.size());
        assert(childID  < nodes.size());

        //Nothing to do
        if(child->parent == parentID)
            return;

        //Remove old child--parent relationship
        if(child->parent != TypeHierarchyNode::NO_PARENT) {
            TypeHierarchyNode& old_parent = nodes[child->parent];
            for(auto it = old_parent.children.begin(); it != old_parent.children.end(); it++)
                if(*it == childID) {
                    old_parent.children.erase(it);
                    break;
                }
        }

        child->parent = parentID;
        if(parent != nullptr)
            parent->children.push_back(childID);
    }

    /** \brief  Get the number of registered nodes
     * \return   the number of registered nodes.  */
    size_t get_nb_nodes() const {return nodes.size();}

    /** \brief  Get the "node_id" of a node
     * \param node a pointer to the node to look into
     * \return   the node_id. Note that this function does not check that the ID is correct. The caller must ensure that either this call was valid to begin with, or that the returned value is inferior than get_nb_nodes() */
    uint64_t node_id(const TypeHierarchyNode* node) const {
        return (node - nodes.data());
    }

    /*\brief  Get the node at position "id"
     * \param id the ID of the node
     * \return the node that has this ID, of nullptr if not found. Note that this pointer is invalid after a call to "new_node". Prefer to store "node_id" instead.*/
    const TypeHierarchyNode* get_node(uint64_t id) const {
        if(id < nodes.size())
            return nodes.data()+id;
        return nullptr;
    }

    /*\brief  Get the node at position "id"
     * \param id the ID of the node
     * \return the node that has this ID, of nullptr if not found. Note that this pointer is invalid after a call to "new_node". Prefer to store "node_id" instead.*/
    TypeHierarchyNode* get_node(uint64_t id) {
        return const_cast<TypeHierarchyNode*>(const_cast<const TypeHierarchy*>(this)->get_node(id)); //Remove the const constrain. This is valid in C++ (even if not elegant)
    }

    /** \brief  Get the root of the hierarchy
     * \return  The pointer of the root */
    const TypeHierarchyNode* get_root() const {
        return get_node(0);
    }

    /** \brief  Get the root of the hierarchy
     * \return  The pointer of the root */
    TypeHierarchyNode* get_root() {
        return get_node(0);
    }

    /** \brief  Find the first node named "name"
     * \param name the name to look into
     * \return   a pointer to the first node (in addition order) containing this name, nullptr otherwise */
    const TypeHierarchyNode* find_first_node_by_name(const std::string& name) const {
        for(const auto& it : nodes)
            if(it.name == name)
                return &it;
        return nullptr;
    }

    /** \brief  Find the first node named "name"
     * \param name the name to look into
     * \return   a pointer to the first node (in addition order) containing this name, nullptr otherwise */
    TypeHierarchyNode* find_first_node_by_name(const std::string& name) {
        return const_cast<TypeHierarchyNode*>(const_cast<const TypeHierarchy*>(this)->find_first_node_by_name(name)); //Remove the const constrain. This is valid in C++ (even if not elegant)
    }

    /** \brief  Find the first node typed "type"
     * \param type the type to look into
     * \return   a pointer to the first node (in addition order) containing this type, nullptr otherwise */
    const TypeHierarchyNode* find_first_node_by_type_combination(uint64_t type) const {
        for(const auto& it : nodes)
            if(it.type_combination == type)
                return &it;
        return nullptr;
    }

    /** \brief  Find the first node typed "type"
     * \param type the type to look into
     * \return   a pointer to the first node (in addition order) containing this type, nullptr otherwise */
    TypeHierarchyNode* find_first_node_by_type_combination(uint64_t type) {
        return const_cast<TypeHierarchyNode*>(const_cast<const TypeHierarchy*>(this)->find_first_node_by_type_combination(type)); //Remove the const constrain. This is valid in C++ (even if not elegant)
    }

    /** \brief  Is the super_type an aggregation (i.e., the same type, a parent, a grand parent, etc.) of coarse_type?
     * \param super_type the type_combination of the parent to test. Note that it can be the same value as coarse_type, in which case this function returns true.
     * \param coarse_type the type_combination of the child to test.
     * \return   true if super_type is an aggregation of a coarse_type, false otherwise */
    bool is_aggregation_of(uint64_t super_type, uint64_t coarse_type) const {
        const TypeHierarchyNode* parent = find_first_node_by_type_combination(super_type);
        const TypeHierarchyNode* child  = find_first_node_by_type_combination(coarse_type);

        if(child == nullptr || parent == nullptr)
            return false;

        for(; child != nullptr && child != parent && child->parent != TypeHierarchyNode::NO_PARENT; child = get_node(child->parent));
        return child == parent;
    }

    std::ostream& print(std::ostream& out, unsigned ind) const;

    void update_nb_occurences(const Graph& g);
};

struct context {
    /*----------------------------------------------------------------------------*/
    /*---Types used to create abstract trees that will contain "regular" trees----*/
    /*----------------------------------------------------------------------------*/
    static constexpr uint64_t root_type          = 0;
    static constexpr uint64_t dual_sequence_type = (uint64_t)-1; //0xffffffff ffffffff;
    static constexpr uint64_t before_type        = (uint64_t)-2; //0xffffffff fffffffe;
    static constexpr uint64_t after_type         = (uint64_t)-3; //0xffffffff fffffffd;

    Graph                    output_graph; /*!< The graph used as output results. Note that this graph differs from "graph" as, here, we consider the hierarchy of events*/
    Graph                    graph; /*!< The low-level aggregated graph*/
    std::vector<std::string> names;
    std::vector<std::string> types; /*!< All the types found in the data, plus those that are necessary for the app (e.g., no treatment, interruption, death, etc.)*/
    std::vector<std::string> drugs; /*!< All the drugs found in the data*/
    std::vector<std::string> diseases; /*!< All the diseases found in the data*/

    /*----------------------------------------------------------------------------*/
    /*----------------------------Type of valid events----------------------------*/
    /*----------------------------------------------------------------------------*/
    size_t                 no_drug_index      = 0; /*!< The index in "drugs" for "no_drug"*/
    size_t                 notreatment_index  = 0; /*!< The index in "types" for "no treatment"*/
    size_t                 interruption_index = 0; /*!< The index in "types" for "interruption"*/
    std::vector<uint64_t>  valid_type_combination; /*!< All the valid type combinations this application supports*/
    TypeHierarchy          type_hierarchy;         /*!< The hierarchy for aggregations*/

    /*----------------------------------------------------------------------------*/
    /*Grouped list of events, ordered per time per individual. To know which sequences of events correspond to which individuals, see indi_table*/
    /*----------------------------------------------------------------------------*/
    const mmap_mem *         info_mem           = nullptr;
    const Info *             info_table         = nullptr; 
    size_t                   info_size          = 0;

    /*----------------------------------------------------------------------------*/
    /* List of individuals. This list give metadata information to read the events (see info_table) of each individual*/
    /*----------------------------------------------------------------------------*/
    const mmap_mem *         indi_mem            = nullptr;
    const IndiInfo *         indi_table          = nullptr;
    size_t                   indi_size           = 0;  /*!< total # of individuals */
    size_t                   indi_cursor         = 0;  /*!< # of indi. processed to far */
    size_t                   indi_filtered       = 0;  /*!< # of indi. filtered to far */
    size_t                   nb_indi_per_quantum = INIT_NB_INDI_PER_QUANTUM; /*!< # of individuals parseable within the quantum */

    /*----------------------------------------------------------------------------*/
    /*-----------------------------State of the tree------------------------------*/
    /*----------------------------------------------------------------------------*/
    size_t                   max_duration = 0; /*!< The maximum sequence duration */
    std::vector<IndiState>   indi_state;
    StackSequence            align_sequence; /*!< The sequence of events to which the tree should be aligned with*/
    Histogram                hist_duration;  /*!< The histogram corresponding to a given node of Node::duration*/
    Histogram                hist_age;       /*!< The histogram corresponding to a given node of Node::age*/

    /*----------------------------------------------------------------------------*/
    /*----------------------------Filtering sequences-----------------------------*/
    /*----------------------------------------------------------------------------*/
    bool          apply_filter          = true; /*!< Should the filter be applied for each sequence? 
                                                     Note that apply_filter is there only for sequence insertion, and not the output of the tree itself (e.g., prune_threshold)*/
    int           interruption_duration = 180;  /*!< What is the minimal intervals (in days) between events to define an interruption of treatment?*/
    int           notreatment_duration  = 365;  /*!< What is the minimal intervals (in days) between events to state that an individual has no treatment?*/
    int           max_filter_duration   = 2048; /*!< What is the maximum length (in days) of all the events of a sequence?*/
    int           min_filter_duration   = 0;    /*!< What is the minimum length (in days) of all the events of a sequence?*/
    int           align_sequence_by     = 0;    /*!< On which occurence should the sequence be aligned? negative number --> last occurence */
    unsigned int  min_filter_age        = 0;    /*!< What is the minimum age of individuals?*/
    unsigned int  max_filter_age        = 128;  /*!< What is the maximum age of individuals?*/
    unsigned int  max_filter_depth      = 15;   /*!< What is the maximum number of events in a sequence?*/

    /*----------------------------------------------------------------------------*/
    /*----------------------------Filter the tree/nodes---------------------------*/
    /*----------------------------------------------------------------------------*/
    bool          use_cnil_requirement  = false; /*!< Apply the CNIL regulations if true. It checks/applies:
                                                    - k-anonimity (minimum: 10). If prune_threshold < 10, the printing of the nodes will apply k-anonimity == 10
                                                    - i-diversity (minimum: 2). Each class of values should at least contains i-diversity bins/possible values 
                                                    - r-representative (0.80). For each class of values, if those are sensible, check that each value is less than 80%
                                                    - s-threshold (3). Each value should represent at least n==3 individuals.*/
    unsigned int          prune_threshold       = 50;    /*!< What is the minimum number of individuals in an event?*/
    unsigned int          aggregate_coarse_level = 0;    /*!< At what coarse level should the aggregation happen? 0 means no higher-level aggregation to perform*/
    std::vector<uint64_t> hidden_items;                  /*!< All the items in the tree that should be ignored*/
    duration_type duration_filter_type  = duration_mean; /*!< How do we "merge" events? Using the mean? The median? (etc.)*/
    std::vector<DiseaseFilterMode> disease_filter;       /*!< What are the diseases filter per individual?*/

    /*----------------------------------------------------------------------------*/
    /*-----------------------------Context Parameters-----------------------------*/
    /*----------------------------------------------------------------------------*/
    ProgressiveStrategy progressive_strategy = ProgressiveStrategy::QUANTUM_TIME;
    unsigned int        chunk_size           = 100000; /*!< The number of patients to treat in the CHUNK_PATIENTS strategy */
    bool                verbose              = false;  /*!< Does the application print additional information in standard output?*/

    /** \brief  Constructor
     * \param capacity the initial size of nodes to allocate */
    context(size_t capacity = INIT_NODE_CAPACITY);

    /** \brief  Destructor */
    ~context();

    /** \brief Reset when a new tree should be computed */
    void clear();

    /** \brief  Is the type combination (e.g., type1 + type2) a valid composed type?
     * \param comb bitmap of the combination to evaluate
     * \return   true if yes, false otherwise*/
    bool is_valid_type_combination(uint64_t comb) const; 

    /** \brief  Init the hierarchy structure based on the information presents in the file "path"
     * \param path the path of the file to look into. The path must be valid */
    void init_hierarchy(const std::string& path);

    /** \brief  Init the hierarchy as its default value: Only fine-grain aggregations are available.
     * This method will create a one-level hierarchy using all the type combination available*/
    void init_default_hierarchy();

    /** \brief  Load the dataset presented in "dir". Dir must contain the following files: data.map, data.bin, and indi.bin. The method can throw a runtime_error if the files are not available, or if they do not have the expected format.
     * \param dir the directory containing the dataset to load */
    void load(const char * dir);

    /** \brief  Get the string name of a (composed) type
     * \param type the bitmap of the composed type. Use is_valid_type_combination to know if the type is valid or not
     * \return    the string representing the composed type of events */
    std::string event_name(uint64_t type) const;
    
    /** \brief  Insert a sequence of events in a node
     * \param graph the graph to insert the sequence into
     * \param stack the sequence of events to-be inserted 
     * \param indi_id the id of the individual linked to the sequence. -1 if the sequence is not associated to an individual.
     * \param is_special_sequence true if this sequence is a special sequence (and thus should be known but does not represent individuals), false otherwise*/
    void insert_sequence(Graph& graph, Stack& stack, size_t indi_id, bool is_special_sequence=false);

    /** \brief  See if the Stack (surely to_be inserted) has to be filtered out
     * \param stack the stack of events
     * \param info the individual information associated to the Stack
     * \param sizeSpecialSeq[out] an output value telling you the size of continuous special events at the beginning of the sequence
     * \return  the reason of the filter if it exists */
    std::string filter(const Stack& stack, const IndiInfo& info, size_t* sizeSpecialSeq=nullptr);

    /** \brief  Get the duration (in days) of a node of a given graph. The type of the duration depends on duration_type (mean, median, etc., of all registered events)
     * \param graph the graph object to use
     * \param id the node ID to evaluate
     * \return   the duration in days.  */
    unsigned duration(Graph& graph, node_id id);

    /** \brief  Get the duration (in days) of a node of the default graph. The type of the duration depends on duration_type (mean, median, etc., of all registered events)
     * \param graph the graph object to use
     * \param id the node ID to evaluate
     * \return   the duration in days.  */
    unsigned duration(node_id id) {return duration(graph, id);}

    /** \brief  Get the duration type in a string format, useful for JSON formatting. The duration type changes how the "duration" function behave
     * \return   the duration type */
    std::string duration_type_get() const;

    /** \brief Set the duration type in a string format, useful from JSON formatting or the python binding. The duration type changes how the "duration" function behave
     * \param type the new duration type */
    void duration_type_set(std::string type);

    /** \brief  Get the maximum time interval that the next batch of data processed by the progressive visualization should take
     * \return   The time in milliseconds*/
    unsigned int quantum_get() const {return quantum;}

    /** \brief  Set the maximum time interval that the next batch of data processed by the progressive visualization should take
     * \param q The new time in milliseconds*/
    void quantum_set(unsigned int q) {nb_indi_per_quantum = (quantum == 0 ? INIT_NB_INDI_PER_QUANTUM : (nb_indi_per_quantum * q / quantum)); quantum = q;}

    uint64_t aggregate_coarse_level_get() const {return aggregate_coarse_level;}
    void aggregate_coarse_level_set(uint64_t v) {aggregate_coarse_level = v; create_output_graph();}

    std::vector<uint64_t> hidden_items_get() const {return hidden_items;}
    void hidden_items_set(const std::vector<uint64_t>& v) {hidden_items = v; create_output_graph();}

    /** \brief  Get the sequence by which the tree is aligned with. If there is a sequence aligned, the tree adds two additional "meta-sequence": dual_sequence_type -> before_type, and dual_sequence_type -> after_type.
     * \return the sequence by which the tree is aligned with. Each value should be a valid_type_combination value.*/
    std::vector<uint64_t> align_sequence_get() const;

    /** \brief  Set the sequence by which the tree is aligned with. If there is a sequence aligned, the tree adds two additional "meta-sequence": dual_sequence_type -> before_type, and dual_sequence_type -> after_type.
     * \param sequence the new sequence by which the tree is aligned with. Each value should be a valid_type_combination value.*/
    void align_sequence_set(const std::vector<uint64_t>& sequence);

    /** \brief  What is the rank order to which the align_sequence is defined? Should the sequence be aligned with the first (value == 0), second (value == 1), ..., last (value < 0) times the align_sequence is seen?
     * \return    the rank of the alignment */
    int align_sequence_by_get() const;

    /** \brief  Set to which occurence (first, second, ..., last) the align_sequence should be set. See also align_sequence_by_get
     * \param occurence negative number for last, any other for the rank*/
    void align_sequence_by_set(int occurence);

    /** \brief  Process (add in the graph) the data of the individual ID "indi".
     * \param indi the ID of the individual to process 
     * \param graph the tree to modify*/
    void process(size_t indi, Graph& graph);

    /** \brief  Process (add in the default graph) the data of the individual ID "indi".
     * \param indi the ID of the individual to process */
    void process(size_t indi) {process(indi, graph);}

    /** Read the individual "i" and returns its event stack
     * \param i the individual ID
     * \param stack the stack to populate
     * \return the event stack given in parameter.*/
    Stack* read_individual(size_t i, Stack& stack);

    /** \brief  Get the number of low_level events (i.e., all the events, without considering merging rules) of the dataset for all the patients. We do not consider filtering here
     * \return  the number of low level events */
    size_t get_nb_low_level_events() const;

    /** \brief  Get the number of low_level events (i.e., all the events, without considering merging rules) of the dataset for a given individual. We do not consider filtering here
     * \param indi the individual ID
     * \return  the number of low level events for the individual "indi" */
    size_t get_nb_low_level_events(size_t indi) const;

    /** \brief get the number of distinct sequence of events
     * \return  the number of distinct sequence of events. */
    size_t get_nb_distinct_pathways() const;

    /** \brief  Get the number of nodes the tree contains, without counting special nodes that have metadata purposes
     * \return  the number of useful nodes */
    size_t get_nb_useful_nodes() const;

    /** \brief  Push the information of an individual into an existing Stack of events.
     * \param stack the stack to update with the new individual info. Extracted events will be pushed at the end of the stack.
     * \param info the individual data from which the events are extracted.*/
    void push_info(Stack& stack, const Info& info);

    /** \brief  Update all the data about what is exported. Currently, creates the output graph and update the hierarchical occurrences */
    void update_view_data();

    /** \brief after clearing, process all the individuals at once. This algorithm is not progressive: It parse everything until it returns  */
    void process_all();

    void create_output_graph();

    /** \brief  Process a new chunk of individuals. The "chunk size" depends on the time the computation takes. This function returns after a maximum duration of context::quantum (can be less if this function finished to parsed all the individuals).
     * This is the main function of the progressive algorithm
     * \param reset should we reset the process from the first individual. If reset==false, the computation starts from where it has paused before */
    void process_next(bool reset=false);

    /** \brief  Did we process all the data (individuals)?
     * \return   true if yes, false otherwise */
    bool process_done() const;

    /** \brief  Print the status of the context in a JSON format. By status, we mean everything except the tree itself. The printing is alongside (usually) the data of the root of the tree
     * \param out the output stream to print into
     * \param indent the indentation (JSON) to use for better readability
     * \return the parameter "out" */
    std::ostream& print_state(std::ostream& out, unsigned indent);

    /** \brief  Print, in an output stream, the corresponding JSON describing the whole context
     * \param out the output stream to print into
     * \param print_histograms should the histograms of all nodes be printed?
     * \return the parameter "out"*/
    std::ostream& print(std::ostream& out, bool print_histograms=false);

    /** \brief  Print, in an output stream, JSON describing a mapping between events and numerical data (e.g., the number of events per event type)
     * \param out the output stream to print into
     * \param container the data to print in a JSON format
     * \return the parameter "out"*/
    std::ostream& print(std::ostream& out,
                   const std::map<uint64_t,unsigned>& container);

    /** \brief  Return, in a string, the corresponding JSON describing the whole context
     * \param print_histograms should the histograms of all nodes be printed?
     * \return   The JSON string*/
    std::string print_string(bool print_histograms=false);

    /** \brief Print a histogram related to a node of a given graph. By default, we use nbBins == 32 for the histogram. The range of the histogram depends on the current filters
     * \param name the name of the histogram
     * \param graph the graph to look into
     * \param id the ID of the node to look at
     * \return   the JSON string describing the histogram */
    std::string print_histogram(HistogramToPrint name, const Graph& graph, node_id id) const;

    /** \brief Print a histogram related to a node of the default output graph. By default, we use nbBins == 32 for the histogram. The range of the histogram depends on the current filters
     * \param name the name of the histogram
     * \param id the ID of the node to look at
     * \return   the JSON string describing the histogram */
    std::string print_histogram(HistogramToPrint name, node_id id) const {return print_histogram(name, output_graph, id);}

    /** \brief Get the string, in a JSON format, of a barchart related to a node of a given graph
     * \param name the name (ID) of the barchart.
     * \param graph the graph to look into
     * \param id the id of the node to look at
     * \return the JSON string describing the barchart */
    std::string print_barchart(BarchartToPrint name, const Graph& graph, node_id id) const;

    /** \brief Get the string, in a JSON format, of a barchart related to a node of the default output graph
     * \param name the name (ID) of the barchart.
     * \param id the id of the node to look at
     * \return the JSON string describing the barchart */
    std::string print_barchart(BarchartToPrint name, node_id id) const {return print_barchart(name, output_graph, id);}

    private:
        unsigned int quantum               = 1000;   /*!< Time intervals in milliseconds used for the progressive algorithm in the QUANTUM_TIME strategy*/

        /** \brief  Read the list of types of events, drugs, etc. included in a file
         * \param filename the filename to look at */
        void read_dicts(const char * filename);

        /** \brief Prepare default state for printing the JSON object describing the context.*/
        void print_prepare();

        /** \brief  Utility function to reset the whole computation */
        void reset_process();

};

#endif
