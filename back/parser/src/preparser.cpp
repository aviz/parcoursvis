#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <sys/stat.h>
#include <string.h>
#include "info.hpp"
#include "date_delta.hpp"

using namespace std;

vector<string> names, drugs;
vector<string> types = {"death"}; //Enforce death as a valid event
vector<string> diseases;
vector<IndiInfo> individuals;

static unsigned
value_of(const string& val, vector<string>& values) {
    auto it = std::find(values.begin(), values.end(), val);
    if (it != values.end())
        return it-values.begin();

    size_t index = values.size();
    values.push_back(val);
    return index;
}

struct CSVRow : vector<string> {
    istream& readline(istream& str) {
        string line;
        getline(str, line);
        if (line.size() != 0 && line[line.size()-1]=='\r')
            line.resize(line.size()-1);
        istringstream lineStream(line);
        string cell;
        clear();
        while(getline(lineStream, cell, ',')) {
            push_back(cell);
        }
        if (!lineStream && cell.empty())
            push_back(cell);
        return str;
    }
};

int preparse(istream& fin, ostream& fout, ostream& indiout) {
    if (value_of("", drugs) != 0) {
        cerr << "No drugs is not index 0" << endl;
    }
    CSVRow row;
    // ignore first CSV line
    if (! row.readline(fin) || row.size() < 5) {
        cerr << "Error reading CSV file " << endl;
        return 1;
    }
    IndiInfo indi; indi.clear();
    bool IndiErr = false;
    string lastname;
    
    fout.write((const char*)&INFO_V1, sizeof(Info));
    indiout.write((const char*)&INDI_INFO_V1, sizeof(indi));
    for (int counter = 0; fin; counter++ ) {
        if (counter && counter % 1000000 == 0) {
            cerr << counter << " lines have been parsed." << endl;
        }
        try {
            row.readline(fin);
            if (row.size() < 5) {
                if (! fin) break;
                cerr << "Incomplete line " << counter << endl;
                continue;
            }

            string name = row[0];
            int age = (int)stof(row[1]);

            if (age > 150) {
                cerr << "Illegal age value for line #" << counter
                     << " with age of " << age << endl;
                IndiErr = true;
                continue;
            }

            if (lastname != name) {
                lastname = name;
                if (counter != 0 && !IndiErr) {
                    indiout.write((const char*)&indi, sizeof(indi));
                }
                names.push_back(name);
                IndiErr = false;
                indi.clear();
                indi.name = (uint32_t)names.size()-1;
                indi.start_index = counter;
                indi.start_age = (uint8_t)age;
                indi.diseases = 0;
            }
            indi.end_index = counter+1;

            Info info;
            info.clear();
            info.name = indi.name;
            info.age = (uint8_t)age;
            string type = row[2];
            info.type = (uint8_t)value_of(type, types);
            int year, month, day;
            char delim;
            istringstream is(row[3]);
            is >> year >> delim >> month >> delim >> day;
            if (! is  || delim != '-') {
                cerr << "Error reading date as year-month-day" << endl;
                IndiErr = true;
                continue;
            }
            int days = date_to_days(year, month, day);
            if (days < 0 || days > 65535) {
              cerr << "Cannot encode date " << row[3] << " with 16 bits, ignored" << endl;
              days = 0;
            }
            info.days = (uint16_t)days;
            
            string drug = row[4];
            info.drug = (uint32_t)value_of(drug, drugs);
            
            if (row.size() > 5 && ! row[5].empty()) {
                const string& field = row[5];
                size_t start = 0;
                int disease_status = 0;
                do {
                    string disease;
                    auto index = field.find(" ", start);
                    if (index == string::npos)
                        index = field.length();
                    disease = field.substr(start, index);
                    start = index+1;
                    while (start < field.length() && field[start] == ' ')
                        start++; // skip white spaces
                
                    disease_status |= (1 << value_of(disease, diseases));
                } while (start < field.length());
                indi.diseases |= (uint8_t)disease_status;
            }
            
            fout.write((const char*)&info, sizeof(Info));
        }
        catch(const logic_error& e) {
            cerr << "Error reading line " << counter << endl;
            IndiErr = true;
        }
    }
    if (indi.end_index != 0) {
        indiout.write((const char*)&indi, sizeof(indi));
    }
    return 0;
}


int main(int argc, char** argv) {
    istream * in = &cin;
    string data_directory("tmp");
    if (argc > 1 && string("-") != argv[1]) {
        in = new ifstream(argv[1]);
        if (! *in) {
            cerr << "Error opening input file " << argv[1] << endl;
            return 1;
        }
    }
    if (argc > 2)
        data_directory = argv[2];
    mkdir(data_directory.c_str(), 0777);
    ofstream fout((data_directory+"/data.bin").c_str(), ios::binary);
    if (! fout.is_open()) {
        cerr << "Error opening output file" << endl;
        return 1;
    }
    ofstream indiout((data_directory+"/indi.bin").c_str(), ios::binary);
    if (! indiout.is_open()) {
        cerr << "Error opening indi output file" << endl;
        return 1;
    }
    if (preparse(*in, fout, indiout)) return 1;
    
    ofstream fdict((data_directory+"/data.map").c_str(), ios::binary);
    if (! fdict.is_open() || ! fout.is_open()) {
        cerr << "Error opening output file(s)" << endl;
        return 1;
    }
    write_dicts(fdict, types, drugs, names, diseases);

    cout << "finish" << endl;
    if (in != &cin)
        delete in;
    return 0;
}
