#include <iostream>
#include <random>
#include <algorithm>
#include "histogram.hpp"

std::default_random_engine generator;
//std::uniform_int_distribution<int> distribution(0,100);
std::poisson_distribution<int> distribution(41);

histogram<int>
generate_random_vector(unsigned samples) {
  std::vector<int> vec;

  for (size_t i=0; i < samples; ++i) {
    int number = distribution(generator);
    vec.push_back(number);
  }
  size_t size = vec.size();
  const auto [min, max] = std::minmax_element(begin(vec), end(vec));

  histogram<int> hist(32, 0, 100);
  hist.compute(vec);
  size_t n = size / 2;
  std::nth_element(vec.begin(), vec.begin()+n, vec.end());
  int exact_median = vec[n];
  int approx_median = hist.median();
  int approx_median2 = hist.median_old();
  int error1 = std::abs(exact_median - approx_median);
  int error2 = std::abs(exact_median - approx_median2);
  if (error2 < error1) {
    std::cout << "min = " << *min << " max = " << *max << " ";
    std::cout << "max_bin_count = " << *std::max_element(begin(hist.bins), end(hist.bins))
              << " exact = " << exact_median
              << " approx = " << approx_median
              << " approx2 = " << approx_median2 << " ";
    std::cout << "old median better than new: " << error1 << " vs. " << error2 << std::endl;
    // std::cout << "old median better than new for vector: " << std::endl;
    // for (auto x : vec)
    //   std::cout << x << ' ';
    // std::cout << std::endl;
  }
  // else {
  //   std::cout << "new median better than old: " << error1 << " vs. " << error2 << std::endl;
  // }
  return hist;
}  

int main() {
  size_t samples = 10000;
  histogram<int> hist(32, 0, 100);

  for (int i = 0; i < 100; i++) {
    auto other = generate_random_vector(samples);
    int cumsum = hist.cumsum;
    int count = hist.count;
    hist += other;
    if (cumsum+other.cumsum != hist.cumsum)
      std::cout << "cumsum disagree" << std::endl;
    if (count+other.count != hist.count)
      std::cout << "count disagree" << std::endl;
  }
  std::cout << "No other output means everything works as expected" << std::endl;
  return 0;
}
