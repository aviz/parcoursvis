#include "pparser.hpp"
#include "CSVParser.hpp"
#include <fstream>
#include <omp.h>

#ifndef NO_PYTHON
#include "pybind11/pybind11.h"
#include "pybind11/stl.h"
#include "pybind11/stl_bind.h"
#endif

constexpr uint64_t context::dual_sequence_type;
constexpr uint64_t context::before_type;
constexpr uint64_t context::after_type;
constexpr uint64_t TypeHierarchyNode::NO_PARENT;

using namespace std;
using namespace chrono;

static bool file_exists(const string& fileName)
{
    std::ifstream infile(fileName.c_str());
    return infile.good();
}

ostream& indent(ostream& out, size_t indent) {
    while (indent--) out << ' ';
    return out;
}

template <typename C>
static ostream&
quote(ostream& out, const C& x) {
    return out << x;
}

static ostream&
quote(ostream& out, const string& x) {
    return out << '"' << x << '"';
}

size_t dict_index(vector<string>& dict, const string& name) {
    size_t index;
    auto it = find(dict.begin(), dict.end(), name);
    if (it == dict.end()) {
        index = dict.size();
        dict.push_back(name);
    }
    else {
        index = it - dict.begin();
    }
    return index;
}

/*----------------------------------------------------------------------------*/
/*-----------------------------------Node::-----------------------------------*/
/*----------------------------------------------------------------------------*/

void Node::prepare(context& ctx) {
    ctx.hist_age = ages;
    ctx.hist_duration.compute(durations);
}

bool Node::filter(context& ctx) {
    //Prune_threshold is applied no matter if the CNIL filtering is on.
    if(count < ctx.prune_threshold)
        return true;

    //Apply the filter of the CNIL
    if(ctx.use_cnil_requirement) {
        //k-anonimity
        if(count < K_ANONIMITY)
            return true;

        //The histograms are computed just before calling this function via Node::prepare
        //Check l-diversity, r-representative, and s-threshold on each histogram
        for(const Histogram* h : {&ctx.hist_age, &ctx.hist_duration}) {
            uint32_t nbRelevantBins = 0;
            for(auto v : h->bins) {
                if(v == 0)
                    continue;
                nbRelevantBins++;
                //r-representative
                if(count*R_REPRESENTATIVE < v*100) //this assume that R_REPRESENTATIVE is an int (in percentage) and not a float. Does this to avoid float computation for optimization
                    return true;

                //s-threshold
                if(v < S_THRESHOLD) //We do not care about singular values as they represent no people.
                    return true;
            }

            //l-diversity
            if(nbRelevantBins < L_DIVERSITY)
                return true;
        }

        ////////////////////////////////////////
        //Apply the same filters on the diseases
        ////////////////////////////////////////

#ifdef MICKAELINTERPRETATION
        //l-diversity -> should be false by design
        if(MAX_DISEASES*2 < L_DIVERSITY) //times two because either you have a disease, or you don't, making for a total of twice as much values.
                                         //Moreover, because of r-representative, there is no need to test if we actually have someone in the "have disease"
                                         //and "do not have disease"
            return true; 
#else
        //jdf: my interpretation is that, if a disease is a Boolean
        //variable, its aggregated count should not be 100% of the node
        //otherwise it would not have a value False. However, for
        //Boolean values, this is already covered by R_REPRESENTATIVE.
#endif

        for(unsigned int i = 0; i < MAX_DISEASES; i++) {
            unsigned r = indi_diseases[i];

            //r-representative
            if(count * R_REPRESENTATIVE       < r * 100 ||
               count * (100-R_REPRESENTATIVE) > r * 100) //100-R_REPRESENTATIVE -> people that does not have that particular disease
                return true;

            //s-threshold
            if(r < S_THRESHOLD)
                return true;
        }

    }

    return false;
}

ostream&
Node::print(ostream& out, uint64_t type, context& ctx, Graph& graph,
            size_t ind, unsigned cum_duration, bool print_histogram, size_t curdepth) {
    node_id id = graph.id(this);
    if(!(type & 0x80000000) && id != 0)
        curdepth++;
    out << "{" 
        << "\"id\":" << id << "," 
        << "\"count\":" << count << ","
        << "\"name\":" << "\"";
    if (id == 0) {
        out << "_root\"" << ",";
    }
    else {
        out << ctx.event_name(type) << "\"" << ",";
    }
    out << endl;

    unsigned duration = ctx.duration(graph, id);
    indent(out, ind+1) << "\"duration\":"
                       << duration << ',' << endl;
    cum_duration += duration;
    if (cum_duration > ctx.max_duration)
        ctx.max_duration = cum_duration;
    indent(out, ind+1) << "\"cum_duration\":"
                       << cum_duration << ',' << endl;    

    indent(out, ind+1) << "\"subcount\":";
    ctx.print(out, subcount) << ',' << endl;

    indent(out, ind+1) << "\"children\": [";
    unsigned children_counts = 0;
    bool first = true;
    if (! children.empty() && curdepth < ctx.max_filter_depth) {
        for (auto child : children) {
            Node& node = graph.nodes[child.second];

            node.prepare(ctx);
            if (!(child.first & 0x80000000) && //Not a special type
                node.filter(ctx))
                continue;
            children_counts += node.count;
            if(!first){
                out << "," <<  endl;
            }
            first = false;
            indent(out, ind+2); // << "\"" << ctx.event_name(child.first) << "\":";
            node.print(out, child.first, ctx, graph, ind+3, cum_duration, print_histogram, curdepth);
        }
    }
    out << "]," << endl;
    indent(out, ind+1) << "\"count_stopped\":"
                       << count - children_counts;
    if (id==0) { // root node
        out << ',' << endl;
        ctx.print_state(out, ind+1);
        out << endl;
    }
    else if(print_histogram) {
        out << ',' << endl;
        indent(out, ind+1) << "\"age_histogram\": "
                           << ctx.print_histogram(HistogramToPrint::AGE, graph, id)
                           << ',' << endl;
        indent(out, ind+1) << "\"duration_histogram\": "
                           << ctx.print_histogram(HistogramToPrint::DURATION, graph, id)
                           << ',' << endl;
        indent(out, ind+1) << "\"disease_barchart\": "
                           << ctx.print_barchart(BarchartToPrint::DISEASE, graph, id)
                           << endl;
    }
    out << "}";
    return out;
}

/*----------------------------------------------------------------------------*/
/*-----------------------------------Graph------------------------------------*/
/*----------------------------------------------------------------------------*/

Graph::Graph(size_t capacity) {
   nodes.reserve(capacity);
   new_node();
}

std::ostream& Graph::print(std::ostream& out, context& ctx, size_t indent, bool print_histogram) {
    if(nodes.size()) {
        //Do not filter out root...
        nodes[0].print(out, context::root_type, ctx, *this, indent, 0, print_histogram, 0);
    }
    return out;
}

Graph& Graph::operator+=(const Graph& graph) {
    //If nothing to merge with
    if(graph.nodes.empty())
        return *this;

    //Check that we have at least a root node
    //This should never occur (we do that "just in case")
    if(nodes.empty())
        new_node();

    //Recursive code
    auto rec_push_node = [this, &graph](uint64_t node_l_id, const Node& node_r, const auto& rec_push_node_fct) -> void {
        //Merge count
        Node* node_l = &nodes[node_l_id];
        node_l->count += node_r.count;

        //Merge subcount
        for(const auto& subcount_r: node_r.subcount) {
            auto subcount_l = node_l->subcount.find(subcount_r.first);
            if(subcount_l == node_l->subcount.end()) {
                node_l->subcount[subcount_r.first] = subcount_r.second;             
            }
            else {
                (*subcount_l).second += subcount_r.second;             
            }
        }

        //Merge indi_diseases
        for(uint32_t i = 0; i < MAX_DISEASES; i++)
            node_l->indi_diseases[i] += node_r.indi_diseases[i];

        // Merge indi_list
        node_l->indi_list |= node_r.indi_list;
        //Merge age and durations distributions
        node_l->ages += node_r.ages;
        node_l->durations.merge(node_r.durations);

        //Merge children
        for(const auto& child_r : node_r.children) {
            auto child_l = node_l->children.find(child_r.first);
            uint64_t child_id;
            if(child_l == node_l->children.end()) {
                child_id = id(new_node());
                node_l->children[child_r.first] = child_id;
                node_l = &nodes[node_l_id];
            }
            else
                child_id = child_l->second;
            rec_push_node_fct(child_id,
                              graph.nodes[child_r.second],
                              rec_push_node_fct);
        }
    };
    
    rec_push_node(id(get_root()), *graph.get_root(), rec_push_node);
    return *this;
}

void Graph::insert_sequence(const Stack& stack, const IndiInfo* info, size_t indi_cursor, bool is_special_sequence, bool verbose){
    if (stack.empty()) return;

    //The event normally occurs for a given individual. 
    //Insert disease information of this individual to all nodes associated with the event
    auto addDisease = [info, &indi_cursor](Node* node) {
        if(indi_cursor == (uint32_t)-1)
            return;
        node->indi_list.add(indi_cursor);
        for(unsigned j = 0; j < MAX_DISEASES; j++) //Iterate over each bit of diseases
            if(info->diseases & (1 << j))
                node->indi_diseases[j]++;
    };
    Node * node = &nodes[0]; //Root node
    if(!is_special_sequence) {
        node->count++;
        addDisease(node);
    }

    // Accumulate subcounts
    if(!is_special_sequence)
        for (size_t j = 0; j < stack.size(); ++j)
            node->subcount[stack[j].type]++;
    else
        for (size_t j = 0; j < stack.size(); ++j)
            if(node->subcount.find(stack[j].type) == node->subcount.end())
                node->subcount[stack[j].type] = 0;


    for (size_t i = 0; i < stack.size(); ++i) {
        if (!node->has_child(stack[i].type)) {
            // Creating a node can move the table and invalide "node"
            // so deal with it before it happens
            node_id xid = nodes.size(); // id(x);
            node->children[stack[i].type] = xid;
            node = new_node();
            assert(id(node)==xid);
            if (verbose) 
                cerr << "creating node " << xid << endl;
        }
        else
            node = &nodes[node->children[stack[i].type]];
        if(!is_special_sequence) {
            node->count++;
            addDisease(node);
        }

        unsigned dur = unsigned(stack[i].end_date - stack[i].start_date);
        node->ages.compute(stack[i].age);
        node->durations.add(dur);

        //Accumulate subcount
        if(!is_special_sequence)
            for (size_t j = i; j < stack.size(); ++j)
                node->subcount[stack[j].type]++;
        else
            for (size_t j = 0; j < stack.size(); ++j)
                if(node->subcount.find(stack[j].type) == node->subcount.end())
                    node->subcount[stack[j].type] = 0;
    }
}

/*----------------------------------------------------------------------------*/
/*---------------------------------context::----------------------------------*/
/*----------------------------------------------------------------------------*/

context::context(size_t capacity) : graph(capacity), hist_duration(32, 0, 2048), hist_age(32, 0, 128) {
}

context::~context(){
    cerr << "# Deleting context" << endl;
    if (info_mem) {
        delete info_mem;
        info_mem = nullptr;
        info_table = nullptr;
    }
    if (indi_mem) {
        delete indi_mem;
        indi_mem = nullptr;
        indi_table = nullptr;
    }
}

void 
context::clear(){
    graph.clear();
    output_graph.clear();
    indi_cursor = 0;
    indi_filtered = 0;
    max_duration = 0;
    hist_duration.clear();
    hist_age.clear();
}

void 
context::print_prepare() {
    max_duration = 0;
}

void 
context::read_dicts(const char * filename) {
    ifstream fdict(filename, ios::binary);
    if (! fdict) {
      throw runtime_error(string("File ") + filename + " does not exist");
    }
    ::read_dicts(fdict, types, drugs, names, diseases);

    notreatment_index  = dict_index(types, "no treatment");
    interruption_index = dict_index(types, "interruption");
    no_drug_index      = dict_index(drugs, "");

    if (no_drug_index != 0) {
        cerr << "# Index of 'no drug' is not 0 as expected" << endl;
    }

    //NON GENERIC. "Hardcode" valid treatment combinations
    const vector<vector<const char*>> comb = {{"interruption"}, 
                                              {"death"}, 
                                              {"surgery"}, 
                                              {"no treatment"},
                                              {"alphabloc"},
                                              {"fivealpha"},
                                              {"phyto"},
                                              {"fivealpha", "alphabloc"},
                                              {"fivealpha", "alphabloc", "phyto"},
                                              {"alphabloc", "phyto"}};

    valid_type_combination.clear();
    valid_type_combination.reserve(comb.size()+3); //3 special case
    for(const auto& row : comb){
        uint64_t type = 0x00;
        for(const auto& col : row) {
            int ind = dict_index(types, col);
            if(ind >= 0 && ind <= 63)
                type |= (1U << ind);
        }
        valid_type_combination.push_back(type);
    }
    valid_type_combination.push_back(dual_sequence_type);
    valid_type_combination.push_back(before_type);
    valid_type_combination.push_back(after_type);
    std::sort(valid_type_combination.begin(), valid_type_combination.end());

    disease_filter.resize(diseases.size(), DiseaseFilterMode::BOTH);
}

bool 
context::is_valid_type_combination(uint64_t comb) const {
    return std::find(valid_type_combination.begin(), valid_type_combination.end(), comb) != valid_type_combination.end();
}

void
context::init_hierarchy(const std::string& path) {
    //Create all the nodes based on names
    ifstream csvFile(path, ios::in); 
    CSVRange csvReader(csvFile);
    CSVRange::CSVIterator csv_it = csvReader.begin();
    std::vector<TypeHierarchyNode*> new_types; //The new types to register once we ensured that all requirements are fulfilled (e.g., no duplication). Can be pointers BECAUSE we set new_types once the hierarchy is created and frozen.
    std::vector<std::pair<std::string, uint64_t>> event_names; //All the known event names in an easy-to-use data structure
    std::string err_msg;
    auto get_type_combination = [this, &event_names](const std::string& name) {
        for(auto it = event_names.begin(); it != event_names.end(); it++)
            if(it->first == name)
                return it;
        return event_names.end();
    };


    if(csv_it != csvReader.end())
        csv_it++; //Skip header
    for(; csv_it != csvReader.end(); csv_it++) {
        assert(csv_it->size() >= 2);
        const string& name        = (*csv_it)[0];
        const string& parent_name = (*csv_it)[1];
    
        TypeHierarchyNode* parent = nullptr;
        if(parent_name == "")
            parent = type_hierarchy.get_root();
        else
            parent = type_hierarchy.find_first_node_by_name(parent_name);
        if(parent == nullptr) {
            err_msg = "Issue with the CSV hierarchy file: a parent or leaf node was not found in hierarchical CSV file. We will use a default hierarchy instead.";
            goto error;
        }
    
        type_hierarchy.new_node(parent, name);
    }

    //Create a list of all event type names. Would be useful to determine leaves
    for(const auto& it : valid_type_combination) {
        if(!(it & 0x80000000)) //Do not consider special types
            event_names.emplace_back(event_name(it), it);
    }

    for(auto& node : type_hierarchy.nodes) {
        if(&node == type_hierarchy.get_root()) //Do not consider root here
            continue;
        //Now set as leaves what should be leaves. If a leaf has not a correct name, cancel.
        //Note that we do not allow duplications
        if(node.get_children().size() == 0) {
            auto node_type_combination = get_type_combination(node.name);
            if(node_type_combination == event_names.end()) {
                err_msg = "Issue with the CSV hierarchy file: a leaf node was not found in the known types of data. We will use a default hierarchy instead.";
                goto error;
            }
            node.type_combination = node_type_combination->second;
            event_names.erase(node_type_combination); //No need to look for that event
        }
        //Store the "super types" in order to save them as actual real types
        //Note that we do not allow for duplications
        else {
            //But, this type MUST NOT be found in the already known types. Otherwise, cancel.
            auto node_type_combination = get_type_combination(node.name);
            if(node_type_combination != event_names.end()) {
                err_msg = "Issue with the CSV hierarchy file: we expect that coarse hierarchical types are different than the already known fine-grained event types. We will use a default hierarchy instead.";
                goto error;
            }
            else if(std::find_if(new_types.begin(), new_types.end(), [&node](const auto& t) {return t->name == node.name;}) != new_types.end()) {
                err_msg = "Issue with the CSV hierarchy file: we expect that coarse hierarchical types are not duplicated. We will use a default hierarchy instead.";
                goto error;
            }
            new_types.emplace_back(&node);
        }
    }

    //Set as "no hierarchical" types that were not found
    for(const auto& it : event_names) {
        TypeHierarchyNode* node = type_hierarchy.new_node(type_hierarchy.get_root(), it.first);
        node->type_combination = it.second;
    }

    //As types are stored as 64 bitmask values, with the most-significant bit being used for "meta types", we handle only up to 63 (included) types of data.
    if(new_types.size() + types.size() > 63) {
        err_msg = "Issue with the CSV hierarchy file: we can handle only up to 63 different types of events (low-level and high-level ones). We will use a default hierarchy instead.";
        goto error;
    }

    //If no problem arose so far, add the newly created "hierarchical types" into the known types of data
    for(auto it : new_types) {
        types.push_back(it->name);
        it->type_combination = (1 << (types.size()-1));
        valid_type_combination.push_back(it->type_combination);
    }

    return;
error:
    cerr << err_msg << std::endl;
    type_hierarchy.clear();
    init_default_hierarchy();
    return;
}

void
context::init_default_hierarchy() {
    for(auto& t : valid_type_combination) {
        if(!(t & 0x80000000)) { //Do not consider special types
            TypeHierarchyNode* node = type_hierarchy.new_node(type_hierarchy.get_root(), event_name(t));
            node->type_combination = t;
        }
    }
}

void 
context::load(const char * dir) {
    string string_file(dir);
    cerr << "# Loading from data directory '" << dir << "'" << endl;

    //data.map
    string_file += "/data.map";
    if (! file_exists(string_file))
      throw runtime_error(string("File ")+string_file+" does not exist");
    read_dicts(string_file.c_str());

    //data.bin
    string info_file(dir);
    info_file += "/data.bin";
    if (! file_exists(info_file))
        throw runtime_error(string("File ")+info_file+" does not exist");

    //indi.bin
    string indi_file(dir);
    indi_file += "/indi.bin";
    if (! file_exists(indi_file))
        throw runtime_error(string("File ")+indi_file+" does not exist");
    info_mem = new mmap_mem(info_file.c_str());
    info_table = reinterpret_cast<const Info *>(info_mem->data());
    if (info_table[0] != INFO_V1)
        throw runtime_error("Invalid info file version");
    else
        cerr << "# info table have version 1" << endl;
    info_table++;
    info_size = info_mem->size() / sizeof(Info) - 1; //-1: Remove the version of the info
    indi_mem = new mmap_mem(indi_file.c_str());
    indi_table = reinterpret_cast<const IndiInfo *>(indi_mem->data());
    if (indi_table[0] != INDI_INFO_V1) 
        throw runtime_error("Invalid individual info file version");
    else
        cerr << "# individual info table have version 1" << endl;
    indi_table++;
    indi_size = indi_mem->size() / sizeof(IndiInfo) - 1; //-1: Remove the version of the info
    indi_state.resize(indi_size);

    //hierarchies
    string hierarchy_file(dir);
    hierarchy_file += "/hierarchy.csv";
    if (! file_exists(hierarchy_file)) {
        cerr << "# no hierarchy file is found at " << hierarchy_file << ". Only fine-grain aggregations are available" << endl;
        init_default_hierarchy();
    }
    else
        init_hierarchy(hierarchy_file);

    cerr << "# context loaded from "<< dir << endl;
}

string 
context::event_name(uint64_t type) const {
    switch(type) {
        case dual_sequence_type:
            return "dual_sequence";
        case before_type:
            return "before";
        case after_type:
            return "after";
        default: {
            string ret;
            for (size_t i = 0; i < types.size(); ++i) {
                if (type&1) {
                    if (! ret.empty()) ret += " ";
                    ret += types[i];
                }
                type >>= 1;
            }
            return ret;
        }
    }
    return "";
}

unsigned
context::duration(Graph& g, node_id id) {
    Values& values = g.nodes[id].durations;

    switch(duration_filter_type) {
        case duration_min: return values.min();
        case duration_max: return values.max();
        case duration_mean: return values.mean();
        case duration_median: return values.median();
    }
    return 0;
}

string
context::duration_type_get() const {
    switch(duration_filter_type) {
    case duration_min: return "min";
    case duration_max: return "max";
    case duration_mean: return "mean";
    case duration_median: return "median";
    }
    throw runtime_error("invalid duration time");
}

void
context::duration_type_set(string type) {
    if (type=="min")
        duration_filter_type = duration_min;
    else if (type=="max")
        duration_filter_type = duration_max;
    else if (type=="mean" || type=="average")
        duration_filter_type = duration_mean;
    else if (type=="median")
        duration_filter_type = duration_median;
    else
        throw runtime_error(string("Invalid duration type ")+type);
}

vector<uint64_t>
context::align_sequence_get() const {
    vector<uint64_t> res;
    res.reserve(align_sequence.size());
    for(const auto& seq: align_sequence)
        res.push_back(seq.type);
    return res;
}

void
context::align_sequence_set(const vector<uint64_t>& sequence){
    align_sequence.clear();
    for(const auto& seqType : sequence)
        align_sequence.push_back({ seqType, 0 });
}

int 
context::align_sequence_by_get() const {
    return align_sequence_by;
}

void 
context::align_sequence_by_set(int occurence) {
    align_sequence_by = occurence;
}

string
context::filter(const Stack& stack, const IndiInfo& info, size_t* sizeSpecialSeq) {
    size_t sizeSpecialSeq_temp = 0;
    if(!sizeSpecialSeq)
        sizeSpecialSeq = &sizeSpecialSeq_temp;

    //Check that the sequence is only composed of negative events. If yes, do not filter anything
    for(*sizeSpecialSeq = 0; (*sizeSpecialSeq) < stack.size(); ++(*sizeSpecialSeq)) {
        const Event& ev = stack[*sizeSpecialSeq];
        if(!(ev.type & 0x80000000)) //not a negative type which have special effects
            goto normalSequence;
    }
    return ""s;

normalSequence:

    if(!apply_filter)
        return ""s;

    //Check the individual data
    for(size_t i = 0; i < std::min(size_t(MAX_DISEASES), disease_filter.size()); i++)
        if(((  info.diseases & (1 << i))  && disease_filter[i] == DiseaseFilterMode::NO)  ||
           ((!(info.diseases & (1 << i))) && disease_filter[i] == DiseaseFilterMode::YES))
           return "disease"s;

    for (size_t i = 0; i < stack.size(); ++i) {
        const Event& ev = stack[i];
        if(ev.type & 0x80000000) //not a negative type which have special effects
            continue;
        if (ev.age > max_filter_age)
            return "max age"s;
        else if (ev.age < min_filter_age)
            return "min age"s;
        else if ((ev.end_date - ev.start_date) > max_filter_duration)
            return "max duration"s;
        else if ((ev.end_date - ev.start_date) < min_filter_duration)
            return "min duration"s;
    }
    return ""s;
}

void
context::insert_sequence(Graph& g, Stack& stack, size_t indi_id, bool is_special_sequence) {
    const IndiInfo& info = indi_table[indi_id];
    if (verbose)
        cerr << "insert_sequence(" << stack.size() << ")" << endl;
    if (stack.empty()) return;
    size_t sizeSpecialSeq = 0;
    string reason = filter(stack, info, &sizeSpecialSeq);
    if (! reason.empty()) {
        indi_filtered++;
        if (verbose)
            cerr << "Sequence filtered out because: " << reason << endl;
        return;
    }

    stack.resize(stack.size());
    g.insert_sequence(stack, &info, indi_id, is_special_sequence, verbose);
}

size_t context::get_nb_low_level_events() const {
    size_t res = 0;
    for (size_t i = 0; i < indi_size; ++i)
        res += get_nb_low_level_events(i);
    return res;
}

size_t context::get_nb_low_level_events(size_t indi) const {
    const IndiInfo& info    = indi_table[indi];
    unsigned forwardi       = info.start_index;
    unsigned forward_length = (forwardi >= info.end_index) ? 0 : info.end_index - forwardi;

    return forward_length;
}

size_t context::get_nb_distinct_pathways() const {
    auto count_rec = [this](size_t acc, const Node& node, const auto& count_rec_fn) -> size_t {
        size_t strict_subcount = std::accumulate(std::begin(node.children), std::end(node.children), 0UL, [this](uint64_t value, const std::map<uint64_t, node_id>::value_type& p) { return value + graph.nodes[p.second].count; });
        if(strict_subcount < node.count)
            acc++;

        for(auto& it : node.children)
            acc = count_rec_fn(acc, graph.nodes[it.second], count_rec_fn);
        return acc;
    };
    return count_rec(0, graph.nodes[0], count_rec);
}

size_t context::get_nb_useful_nodes() const {
    auto count_rec = [this](const Node& node, uint64_t type, const auto& count_rec_fn) -> size_t {
        size_t acc = 0;
        if(!(type & 0x80000000))
            acc++;

        for(auto& it : node.children)
            acc += count_rec_fn(graph.nodes[it.second], it.first, count_rec_fn);
        return acc;
    };
    size_t acc = 0;
    for(auto& it : graph.nodes[0].children)
        acc += count_rec(graph.nodes[it.second], it.first, count_rec);
    return acc;
}

void
context::process(size_t i, Graph& g) {
    //First read the whole individual
    Stack stack;
    read_individual(i, stack);
    if(stack.empty()) //nothing to do
        return;

    //Check how we should proceed the reading
    if(!align_sequence.empty()) {
        if(align_sequence_by >= 0) {
            int foundNbTimes = -1;

            //Search in "stack" where align_sequence exists
            for(unsigned int ind = 0; ind < stack.size(); ind++) {
                unsigned int j = 0;
                for(unsigned int k = ind; j < align_sequence.size() && k < stack.size() && type_hierarchy.is_aggregation_of(align_sequence[j].type, stack[k].type); j++, k++); //count consecutive occurrences
                if(j == align_sequence.size()){ //find a perfect match
                    foundNbTimes++;
                    if(foundNbTimes == align_sequence_by){
                        //Recreate the sequence in the "abstract tree"    empty |- before -- regular tree
                        //                                                      |- after  -- regular tree
                        
                        //First backward
                        {
                            Stack subStack;
                            subStack.emplace_back();   //The empty "root"
                            subStack.back().type = dual_sequence_type; 

                            subStack.emplace_back();   //The "before" subtree
                            subStack.back().type = before_type;

                            for(int k = ind+j-1; k >= 0; k--) //Reconstruct backward the events
                                subStack.push_back(stack[k]);
                            insert_sequence(g, subStack, i);
                        }

                        //The forward
                        {
                            Stack subStack;
                            subStack.emplace_back();   //The empty "root"
                            subStack.back().type = dual_sequence_type; 

                            subStack.emplace_back();   //The "after" subtree
                            subStack.back().type = after_type;

                            for(size_t k = ind; k < stack.size(); k++) //construct forward the events
                                subStack.push_back(stack[k]);
                            insert_sequence(g, subStack, i);
                        }
                        break; //Stop at align_sequence_by occurence
                    }
                }
            }
        }

        else {
            //Search in "stack" where align_sequence exists in reverse
            for(int ind = stack.size()-1; ind >= 0; ind--) {
                int j = align_sequence.size()-1;
                for(int k = ind; j >= 0 && k >= 0 && type_hierarchy.is_aggregation_of(align_sequence[j].type, stack[k].type); j--, k--); //count consecutive occurrences in reverse order
                if(j == -1){ //find a perfect match
                    //Recreate the sequence in the "abstract tree"    empty |- before -- regular tree
                    //                                                      |- after  -- regular tree
                    
                    //First backward
                    {
                        Stack subStack;
                        subStack.emplace_back();   //The empty "root"
                        subStack.back().type = dual_sequence_type; 

                        subStack.emplace_back();   //The "before" subtree
                        subStack.back().type = before_type;

                        for(int k = ind; k >= 0; k--) //Reconstruct backward the events
                            subStack.push_back(stack[k]);
                        insert_sequence(g, subStack, i);
                    }

                    //The forward
                    {
                        Stack subStack;
                        subStack.emplace_back();   //The empty "root"
                        subStack.back().type = dual_sequence_type; 

                        subStack.emplace_back();   //The "after" subtree
                        subStack.back().type = after_type;

                        for(size_t k = ind-align_sequence.size()+1; k < stack.size(); k++) //construct forward the events
                            subStack.push_back(stack[k]);
                        insert_sequence(g, subStack, i);
                    }
                    break; //Stop at first occurence from backward
                }
            }
        }
    }
    else {
        insert_sequence(g, stack, i);
    }
}

Stack*
context::read_individual(size_t i, Stack& stack) {
    IndiState& state = indi_state[i];
    const IndiInfo& indi = indi_table[i];
    unsigned forwardi = indi.start_index + state.cursor_forward;
    unsigned forward_length = (forwardi >= indi.end_index) ? 0 : indi.end_index - forwardi;

    //Check that "start_index" corresponds to the beginning of an individual
    //i.e., start_index-1 should be another person
    if (indi.start_index != 0)
        assert(   info_table[indi.start_index].name 
               != info_table[indi.start_index-1].name);

    if (forward_length != 0) {
        for (;forwardi < indi.end_index; forwardi++) {
            push_info(stack, info_table[forwardi]);
        } 
        return &stack;
    }

    return nullptr;
}

void context::reset_process() {
    //Insert special nodes if the system needs to align the sequence
    if(!align_sequence.empty()) {
        Stack subStack;
        subStack.emplace_back();   //The empty "root"
        subStack.back().type = dual_sequence_type; 
        subStack.emplace_back();   //The "before" subtree
        subStack.back().type = before_type;
        insert_sequence(graph, subStack, -1, true);

        subStack = Stack();
        subStack.emplace_back();   //The empty "root"
        subStack.back().type = dual_sequence_type; 
        subStack.emplace_back();   //The "after" subtree
        subStack.back().type = after_type;
        insert_sequence(graph, subStack, -1, true);
    }
}

void
context::update_view_data() {
    create_output_graph();
    type_hierarchy.update_nb_occurences(graph);
}

void
context::process_all() {
    clear();
    reset_process();

    uint32_t nb_threads = 1; 
    std::vector<Graph*> local_graphs(omp_get_max_threads(), nullptr);

    #pragma omp parallel
    {
        #pragma omp master
		{
            nb_threads = omp_get_num_threads();
            if(verbose) {
                cerr << "Application running on " << nb_threads << " threads" << endl;
            }
		}
        Graph* local_graph = nullptr;
        auto thread_id = omp_get_thread_num();
        if(thread_id == 0) //Avoid the creation of a graph object if we can reuse the final graph context:: will yield
            local_graph = &graph;
        else {
            local_graph = new Graph(256);
            local_graphs[thread_id] = local_graph;
        }
        #pragma omp for schedule(static)
        for(uint32_t i = 0; i < indi_size; ++i) {
            process(i, (*local_graph));
        }
    }
    for(uint32_t i = 1; i < nb_threads; i++) {
        Graph* local_graph = local_graphs[i];
        graph += (*local_graph);
        delete local_graph;
    }
    indi_cursor = indi_size;
}

void
context::process_next(bool reset) {
    if (reset) {
        clear();
        reset_process();
//        cerr << "# reset iterator, indi_cursor = " << indi_cursor << endl;
    }
    else {
//        cerr << "# continue iterator, indi_cursor = " << indi_cursor << endl;        
    }

    decltype(steady_clock::now()) begin;
    size_t max_cursor = indi_size;
    if(progressive_strategy == ProgressiveStrategy::QUANTUM_TIME) {
        begin = steady_clock::now();
        max_cursor = std::min(indi_cursor+nb_indi_per_quantum, indi_size);
    }
    else if(progressive_strategy == ProgressiveStrategy::CHUNK_PATIENTS)
        max_cursor = std::min(indi_cursor+chunk_size, indi_size);

    uint32_t nb_threads = 1;
    std::vector<Graph*> local_graphs(omp_get_max_threads(), nullptr);

    #pragma omp parallel
    {
        #pragma omp master
		{
            nb_threads = omp_get_num_threads();
            if(verbose) {
                cerr << "Application running on " << nb_threads << " threads" << endl;
            }
		}
        Graph* local_graph = nullptr;
        auto thread_id = omp_get_thread_num();
        if(thread_id == 0) //Avoid the creation of a graph object if we can reuse the final graph context:: will yield
            local_graph = &graph;
        else {
            local_graph = new Graph(256);
            local_graphs[thread_id] = local_graph;
        }

        #pragma omp for schedule(static)
        for(uint32_t i = indi_cursor; i < max_cursor; i++) {
            process(i, (*local_graph));
        }
    }

    for(uint32_t i = 1; i < nb_threads; i++) {
        Graph* local_graph = local_graphs[i];
        graph += (*local_graph);
        delete local_graph;
    }
    indi_cursor = max_cursor;

    if(progressive_strategy == ProgressiveStrategy::QUANTUM_TIME) {
        auto diff_ms = std::chrono::duration_cast<microseconds>(steady_clock::now()-begin).count();
        if(diff_ms == 0)
            diff_ms = 1;
        nb_indi_per_quantum = (nb_indi_per_quantum * quantum * 1000)/diff_ms;
        if(nb_indi_per_quantum < nb_threads)
            nb_indi_per_quantum = nb_threads;
    }
}

void
context::create_output_graph() {
    output_graph = graph;

    auto rec_remove_hidden_items = [this](Graph& dest, uint64_t cur_node_id, const auto& fn) -> void {
        Node& cur_node = dest.nodes[cur_node_id];

        for(auto child_it = cur_node.children.begin(); child_it != cur_node.children.end(); child_it++) {
            //Check the type of the original node
            auto hidden_item_it = std::find(hidden_items.begin(), hidden_items.end(), child_it->first);

            //If it should be hidden -> move up the children of that particular node and repeat
            if(hidden_item_it != hidden_items.end()) {
                Node& child_node_to_remove = dest.nodes[child_it->second];
                cur_node.children.erase(child_it);
                for(auto& child_node_src_it : child_node_to_remove.children) {
                    auto child_node_dest_it = cur_node.children.find(child_node_src_it.first);
                    if(child_node_dest_it == cur_node.children.end())
                        cur_node.children[child_node_src_it.first] = child_node_src_it.second;
                    else
                    {
                        Node* child_node_dest = &dest.nodes[child_node_dest_it->second];

                        //Merge the values             
                        //count
                        Node& child_node_src = dest.nodes[child_node_src_it.second];
                        child_node_dest->count += child_node_src.count;

                        //subcount
                        for(auto& subcount_src_it : child_node_src.subcount) {
                            auto subcount_dest_it = child_node_dest->subcount.find(subcount_src_it.first);
                            if(subcount_dest_it != child_node_dest->subcount.end())
                                subcount_dest_it->second += subcount_src_it.second;
                            else
                                child_node_dest->subcount[subcount_src_it.first] = subcount_src_it.second;
                        }

                        //Merge age and durations distributions
                        child_node_dest->ages += child_node_src.ages;
                        child_node_dest->durations.merge(child_node_src.durations);
                    }
                }
                fn(dest, cur_node_id, fn);
                return;
            }
            fn(dest, child_it->second, fn);
        }        
    };

    auto rec_aggregate = [this](Graph& dest, uint64_t dest_cur_node_id, const Node& src_cur_node, bool src_filtered_out, std::vector<uint64_t>& parents, const auto& fn) -> void {
        Node* dest_cur_node = &dest.nodes[dest_cur_node_id]; //Work with IDs due to how reallocation works with std::vectors
        if(!src_filtered_out) {//Pay attention to hidden_items
            parents.push_back(dest_cur_node_id);

            /*----------------------------------------------------------------------------*/
            /*---------------------Aggregate distributions that can be--------------------*/
            /*----------------------------------------------------------------------------*/
            //Merge count
            dest_cur_node->count += src_cur_node.count;

            //Merge indi_diseases
            for(uint32_t i = 0; i < MAX_DISEASES; i++)
                dest_cur_node->indi_diseases[i] += src_cur_node.indi_diseases[i];

            //Merge age and durations distributions
            dest_cur_node->ages += src_cur_node.ages;
            dest_cur_node->durations.merge(src_cur_node.durations);
        }

        /*----------------------------------------------------------------------------*/
        /*-----------------------------Aggregate children-----------------------------*/
        /*----------------------------------------------------------------------------*/

        for(const auto& child : src_cur_node.children) {
            const Node& src_child_node = output_graph.nodes[child.second];

            uint64_t child_type;
            bool add_child = false;
            /*-----------------------------------------------------------------*/
            /*get or create a new node in the dest graph based on the hierarchy*/
            /*-----------------------------------------------------------------*/
            TypeHierarchyNode* child_hierarchy = type_hierarchy.find_first_node_by_type_combination(child.first);
            Node* dest_child_node = nullptr;

            if(child_hierarchy == nullptr) { //happen for abstract types
                child_type = child.first;
                add_child  = true;
            }

            else {
                if(child_hierarchy->get_parent() != TypeHierarchyNode::NO_PARENT && child_hierarchy->get_parent() != type_hierarchy.node_id(type_hierarchy.get_root())) { 
                    //If a parent -> create a node or get the old created one with the higher-level type
                    TypeHierarchyNode* parent_hierarchy = type_hierarchy.get_node(child_hierarchy->get_parent());
                    child_type = parent_hierarchy->type_combination;
                    auto it = dest_cur_node->children.find(child_type);
                    if(it != dest_cur_node->children.end()) {
                        dest_child_node = &dest.nodes[it->second];
                    }
                    else {
                        add_child = true;
                    }
                }
                else {
                    child_type = child_hierarchy->type_combination;
                    add_child  = true;
                }
            }

            if(add_child) {
                dest_child_node = dest.new_node();
                dest_cur_node   = &dest.nodes[dest_cur_node_id];
                dest_cur_node->children.emplace(child_type, dest.id(dest_child_node));
            }

            /*----------------------------------------------------------------------------*/
            /*-----Do the math but the one concerning the child->parent relationship------*/
            /*----------------------------------------------------------------------------*/

            //Merge subcount
            for(const uint64_t& parent_id : parents) {
                Node& parent = dest.nodes[parent_id];

                auto subcount_it = parent.subcount.find(child_type);
                if(subcount_it != parent.subcount.end())
                    subcount_it->second += src_child_node.count;
                else
                    parent.subcount[child_type] = src_child_node.count;
            }

            //And add its count to the child' subcount data structure too
            dest_child_node->subcount[child_type] += src_child_node.count;

            /*-----------------------------------------------------------------*/
            /*--------recursively create the types in a top-down fashion-------*/
            /*-----------------------------------------------------------------*/
            fn(dest, dest.id(dest_child_node), src_child_node, false, parents, fn);
        }
        parents.pop_back();
    };

    rec_remove_hidden_items(output_graph, 0, rec_remove_hidden_items);
    for(unsigned int i = 0; i < aggregate_coarse_level; i++) {
        Graph g;
        std::vector<uint64_t> parents;
        rec_aggregate(g, g.id(g.get_root()), *output_graph.get_root(), false, parents, rec_aggregate); //note that output_graph is, as far as the recursive lambda is concerned, the input data
        output_graph = std::move(g);
        rec_remove_hidden_items(output_graph, 0, rec_remove_hidden_items);
    }
}

bool
context::process_done() const {
    return indi_cursor >= indi_size;
}

static ostream&
print(ostream& out, 
      const vector<string>& container) {
    int cnt = 0;
    out << '[';
    for (auto& x : container) {
        if (cnt++) out << ',';
        quote(out, x);
    }
    return out << ']';
}

ostream&
context::print(ostream& out, 
               const map<uint64_t,unsigned>& container) {
    int cnt = 0;
    out << '{';
    for (auto& x : container) {
        if (cnt++) out << ',';
        quote(out, event_name(x.first)) << ':';
        quote(out, x.second);
    }
    return out << '}';
}

ostream&
context::print(ostream& out, bool print_histograms) {
    print_prepare();
    return output_graph.print(out, *this, 0, print_histograms);
}

string
context::print_string(bool print_histograms) {
  stringstream out;
  print(out, print_histograms);
  return out.str();
}

string
context::print_histogram(HistogramToPrint name, const Graph& g, node_id id) const {
    Histogram hist(32, 0, 0);
    if(id >= g.nodes.size())
        throw runtime_error(string("Invalid node ID"));

    const Node& node = g.nodes[id];
    if (name == HistogramToPrint::AGE) {
        hist = node.ages;
    }
    else if (name == HistogramToPrint::DURATION) {
        hist.set_range(min_filter_duration, max_filter_duration);
        hist.compute(node.durations);
    }
    else
        throw runtime_error(string("Invalid histogram name"));
    stringstream out;
    hist.print(out);
    return out.str();
}

string
context::print_barchart(BarchartToPrint name, const Graph& g, node_id id) const {
    if(name != BarchartToPrint::DISEASE)
        throw runtime_error(string("Invalid barchart name"));

    //As diseases are stored in 24 bits format, I am not expecting more than 24.
    //This is just in case...
    if(diseases.size() == 0 || diseases.size() > MAX_DISEASES){
        if(verbose)
            cerr << "We expected to get between 1 and 8 diseases, but we read "
                 << diseases.size() << "instead. return empty list." << endl;
        return "[]";
    }

    if(id >= g.nodes.size())
        throw runtime_error(string("Invalid node ID "+id));

    const Node& node = g.nodes[id];

    //Go through all the bitmaps and count occurences of 1s (per disease)
    barchart barchart(diseases.size());
    for(unsigned int i = 0; i < diseases.size(); i++) {
        barchart.set_category_name(i, diseases[i]);
        //for(auto it = node.indi_diseases[i].begin(); it != node.indi_diseases[i].end(); it++)
        barchart.add(i, node.indi_diseases[i]);
    }
    return barchart.str();
}

ostream&
TypeHierarchyNode::print(const TypeHierarchy& hierarchy, ostream& out, unsigned ind) const {
    indent(out, ind) << "{ \"type_combination\": " << type_combination << ',' << endl;
    indent(out, ind+2) << "\"nb_occurences\": " << nb_occurences << ',' << endl;
    indent(out, ind+2) << "\"name\": " << '"' << name << '"';
    if(children.size() != 0) {
        out << ',' << endl;
        indent(out, ind+2) << "\"children\": [";
        out << endl;
        for(unsigned int i = 0; i < children.size()-1; i++)
            hierarchy.get_node(children[i])->print(hierarchy, out, ind+4) << ',' << endl;
        hierarchy.get_node(children.back())->print(hierarchy, out, ind+4) << ']' << endl;
    }
    else
        out << endl;
    indent(out, ind) << '}';
    return out;
}

ostream&
TypeHierarchy::print(ostream& out, unsigned ind) const {
    indent(out, ind) << "\"type_hierarchy\":" << endl; 
    get_root()->print(*this, out, ind+4);
    return out;
}

void
TypeHierarchy::update_nb_occurences(const Graph& g) {
    //Reset counting
    for(auto& it : nodes)
        it.nb_occurences = 0;

    auto cover_graph_rec = [&g,this](const Node& curNode, const auto& fn_rec) -> void {
        for(const auto& it : curNode.children) {
            TypeHierarchyNode* type = find_first_node_by_type_combination(it.first);
            const Node& child = g.nodes[it.second];
            if(type != nullptr && type->children.size() == 0) //Handle only leaves
                type->nb_occurences += child.count;
            fn_rec(child, fn_rec);
        }
    };

    auto update_nb_occurences_rec = [this](TypeHierarchyNode& node, const auto& fn_rec) -> uint64_t {
        if(node.children.size() == 0)
            return node.nb_occurences;
        uint64_t sum = 0;
        for(uint64_t id : node.children)
            sum += fn_rec(*get_node(id), fn_rec);
        node.nb_occurences = sum;
        return sum;
    };
    
    cover_graph_rec(*g.get_root(), cover_graph_rec);
    update_nb_occurences_rec(*get_root(), update_nb_occurences_rec);
    
}

ostream&
context::print_state(ostream& out, unsigned ind) {
    indent(out, ind) << "\"done\": " <<
        (process_done() ? "true" : "false") << "," << endl;
    indent(out, ind) << "\"types\": [" << endl;
    for(size_t i = 0; i < valid_type_combination.size()-1; i++) {
        indent(out, ind+1)
            << "{\"id\":" << valid_type_combination[i]
            << ", \"name\":\"" << event_name(valid_type_combination[i])
            << "\"}," << endl;
    }
    indent(out, ind+1)
        << "{\"id\":" << valid_type_combination.back()
        << ", \"name\":\"" << event_name(valid_type_combination.back())
        << "\"}" << endl;
    indent(out, ind+1) << "]," << endl;
    //::print(out, types) << ',' << endl;
    type_hierarchy.print(out, ind) << ',' << endl;
    indent(out, ind) << "\"drugs\":";
    ::print(out, drugs) << ',' << endl;
    indent(out, ind) << "\"diseases\":";
    ::print(out, diseases) << ',' << endl;
    indent(out, ind) << "\"interruption_duration\":"
                     << interruption_duration << "," << endl;
    indent(out, ind) << "\"notreatment_duration\":"
                     << notreatment_duration << "," << endl;
    indent(out, ind) << "\"max_filter_duration\":"
                     << max_filter_duration << "," << endl;
    indent(out, ind) << "\"min_filter_duration\":"
                     << min_filter_duration << "," << endl;
    indent(out, ind) << "\"min_filter_age\":"
                     << min_filter_age << "," << endl;
    indent(out, ind) << "\"max_filter_age\":"
                     << max_filter_age << "," << endl;
    indent(out, ind) << "\"max_filter_depth\":"
                     << max_filter_depth << "," << endl;
    indent(out, ind) << "\"prune_threshold\":"
                     << prune_threshold << "," << endl;
    indent(out, ind) << "\"duration_filter_type\": \""
                     << duration_type_get() << "\"," << endl;

    indent(out, ind) << "\"align_sequence\": [";
    if(!align_sequence.empty()){
        for(size_t i = 0; i < align_sequence.size()-1; ++i) 
            out << align_sequence[i].type << ",";
        out << align_sequence.back().type;
    }
    out << "], " << endl;

    indent(out, ind) << "\"align_sequence_by\": " << align_sequence_by << ',' << endl;

    indent(out, ind) << "\"quantum\":" << quantum << "," << endl;
    indent(out, ind) << "\"verbose\":"
                     << verbose << "," << endl;
    indent(out, ind) << "\"filtered_count\":" << indi_filtered << "," << endl;
    indent(out, ind) << "\"total_count\":" << indi_size << "," << endl;
    indent(out, ind) << "\"nb_processed\":" << indi_cursor << "," << endl;
    indent(out, ind) << "\"max_duration\":" << max_duration << "," << endl;
    indent(out, ind) << "\"age_histogram\": {" << endl;
    indent(out, ind+1) << "\"range\": ["
                       << hist_age.min_value << ","
                       << hist_age.max_value << "]," << endl;
    indent(out, ind+1) << "\"bins\": [" << hist_age.bins[0];
    for (size_t i = 1; i < hist_age.bins.size(); ++i) {
        out << "," << hist_age.bins[i];
    }
    out << "]," << endl;
    indent(out, ind+1) << "\"median\": " << hist_age.median() << "," << endl;
    indent(out, ind+1) << "\"mean\": " << hist_age.mean() << endl;
    indent(out, ind) << "}," << endl;
    indent(out, ind) << "\"duration_histogram\": {" << endl;        
    indent(out, ind+1) << "\"range\": ["
                       << hist_duration.min_value << ","
                       << hist_duration.max_value << "]," << endl;
    indent(out, ind+1) << "\"bins\": [" << hist_duration.bins[0];
    for (size_t i = 1; i < hist_duration.bins.size(); ++i) {
        out << "," << hist_duration.bins[i];
    }
    out << "]," << endl;
    indent(out, ind+1) << "\"median\": " << hist_duration.median() << "," << endl;
    indent(out, ind+1) << "\"mean\": " << hist_duration.mean() << endl;
    indent(out, ind) << "}"; // no comma at the end
    return out;
}


void
context::push_info(Stack& stack, const Info& info) {
    //Nothing to do
    if (stack.empty()) {
        stack.push_back(info);
        return;
    }

    //Start to apply merging/interruption rules
    auto stack_top = stack.rbegin(); //We use iterator due to push_back that changes things...
    if ((info.days - stack_top->end_date) <= 15 &&
         is_valid_type_combination(stack_top->type | (1U << info.type))) {
        if (stack_top->contains_type(info.type)) {
            stack_top->end_date = max(stack_top->end_date, info.days);
        }
        else {
            stack_top->add_type(info.type);
            stack_top->end_date = max(stack_top->end_date, info.days);
        }
        stack_top->add_drug(info.drug);
    } 
    else if ((info.days - stack_top->end_date) >= notreatment_duration) {
        stack_top->end_date += 3;
        stack.emplace_back(info.name, 
                           notreatment_index, 
                           no_drug_index, 
                           stack_top->end_date, 
                           info.days, 
                           info.age);
        stack.push_back(info);
        stack_top = stack.rbegin();
    } 
    else if ((info.days - stack_top->end_date) >= interruption_duration) {
        stack_top->end_date += 3;                        
        stack.emplace_back(info.name,
                           interruption_index, 
                           no_drug_index, 
                           stack_top->end_date, 
                           info.days,
                           info.age);
        stack.push_back(info);
        stack_top = stack.rbegin();
    } 
    else {                      
        stack.push_back(info);
        stack_top = stack.rbegin();
    }

    //Merge events with the same type.
    while (stack.size() > 1 &&
           (stack.end()-2)->type == (stack.end()-1)->type) {
        (stack.end()-2)->end_date = (stack.end()-1)->end_date;
        stack.pop_back();
    }
}

/*----------------------------------------------------------------------------*/
/*--------------------------Start the python binding--------------------------*/
/*----------------------------------------------------------------------------*/

#ifndef NO_PYTHON
namespace py = pybind11;
using namespace pybind11::literals;

py::dict get_node(context& ctx, Graph& graph, node_id id) {
    py::dict ret("id"_a=id);
    Node& node = graph.nodes[id];
    ret["count"] = node.count;
    py::dict subcount;
    for (auto sub : node.subcount) {
        subcount[py::str(ctx.event_name(sub.first))] = sub.second;
    }
    ret["subcount"] = subcount;
    py::dict children;
    for (auto child : node.children) {
        Node& node = graph.nodes[child.second];
        if (node.count < ctx.prune_threshold)
            continue;
        children[py::str(ctx.event_name(child.first))] = get_node(ctx, graph, child.second);
    }
    if (children.size() != 0)
        ret["children"] = children;
    
    return ret;
}

py::dict get_tree(context& ctx, Graph& graph) {
    py::dict ret = get_node(ctx, graph, 0);
    ret["total_count"]  = ctx.indi_size;
    ret["nb_processed"] = ctx.indi_cursor;
    return ret;
}

std::vector<uint64_t> get_node_parent_and_self_types(Graph& graph, node_id id) {
    std::vector<uint64_t> ret;

    const auto rec_construction = [&graph, &id](const Node& curNode, node_id curNodeID, std::vector<uint64_t>& ret, const auto& fn) -> bool {
        if(curNodeID == id)
            return true;

        for(const auto& child : curNode.children) {
            ret.push_back(child.first);
            if(!fn(graph.nodes[child.second], child.second, ret, fn))
                ret.pop_back();
            else
                return true;
        }
        return false;
    };
    rec_construction(graph.nodes[0], 0, ret, rec_construction);
    return ret;
}

PYBIND11_MAKE_OPAQUE(std::vector<string>);
PYBIND11_MAKE_OPAQUE(std::vector<DiseaseFilterMode>);

PYBIND11_MODULE(parcoursprog, m) {
    m.doc() = R"pbdoc(
        ParcoursVis Progressive System
        -----------------------

        .. currentmodule:: parcoursvis

        .. autosummary::
           :toctree: _generate


    )pbdoc";

    py::class_<Info>(m, "Info", R"pbdoc(
         Information for every event.

        Some other explanation about the Info structure.
    )pbdoc");
    py::class_<Node>(m, "Node")
        .def_readonly("count", &Node::count);

    py::class_<Graph>(m, "Graph")
        .def_readonly("nodes", &Graph::nodes)
        .def("get_node_parent_and_self_types", &get_node_parent_and_self_types);

    py::class_<std::vector<string>>(m, "StringVector")
        .def(py::init([](const py::list& l) {
            auto v = make_unique<vector<string>>();
            for(const py::handle& obj : l)
                v->push_back(obj.attr("__str__")().cast<std::string>());
            return v;
        }))
        .def("__len__", [](const std::vector<string> &v) { return v.size(); })
        .def("__iter__", [](const std::vector<string> &v) {
                             return py::make_iterator(v.begin(), v.end());
                         }, py::keep_alive<0, 1>()) /* Keep vector alive while iterator is used */
        .def("__getitem__", [](const std::vector<string> &v, int i) {
                                if (i < 0 || unsigned(i) > v.size())
                                  throw pybind11::index_error("index out of range");
                                return v[i]; })
        .def("index", [](const std::vector<string> &v, const std::string& s) {
                          auto it = std::find(v.begin(), v.end(), s);
                          if(it == v.end())
                              throw pybind11::value_error("Value " + s + " not found."); 
                          return it-v.begin(); });

    py::enum_<DiseaseFilterMode>(m, "DiseaseFilterMode")
        .value("YES", DiseaseFilterMode::YES)
        .value("NO", DiseaseFilterMode::NO)
        .value("BOTH", DiseaseFilterMode::BOTH);

    py::enum_<HistogramToPrint>(m, "HistogramToPrint")
        .value("AGE", HistogramToPrint::AGE)
        .value("DURATION", HistogramToPrint::DURATION);

    py::enum_<BarchartToPrint>(m, "BarchartToPrint")
        .value("DISEASE", BarchartToPrint::DISEASE);

    py::enum_<ProgressiveStrategy>(m, "ProgressiveStrategy")
        .value("QUANTUM_TIME", ProgressiveStrategy::QUANTUM_TIME)
        .value("CHUNK_PATIENTS", ProgressiveStrategy::CHUNK_PATIENTS);

    py::bind_vector<std::vector<DiseaseFilterMode>>(m, "DiseaseFilterModeVector");

    py::class_<context>(m, "context")
        .def(py::init<>())
        .def("load", &context::load)
        .def_readonly("names",        &context::names)
        .def_readonly("types",        &context::types)
        .def_readonly("drugs",        &context::drugs)
        .def_readonly("diseases",     &context::diseases)
        .def_readonly("indi_size",    &context::indi_size)
        .def_readonly("indi_cursor",  &context::indi_cursor)
        .def_readonly("graph",        &context::graph)
        .def_readonly("output_graph", &context::output_graph)
        .def_readwrite("apply_filter", &context::apply_filter)
        .def_readwrite("use_cnil_requirement", &context::use_cnil_requirement)
        .def_readwrite("interruption_duration", &context::interruption_duration)
        .def_readwrite("no_treatment_duration", &context::notreatment_duration)
        .def_readwrite("max_duration",          &context::max_filter_duration)
        .def_readwrite("min_duration",          &context::min_filter_duration)
        .def_readwrite("min_age",               &context::min_filter_age)
        .def_readwrite("max_age",               &context::max_filter_age)
        .def_readwrite("max_depth",             &context::max_filter_depth)
        .def_readwrite("verbose",               &context::verbose)
        .def_readwrite("disease_filter",        &context::disease_filter)
        .def_readwrite("progressive_strategy",  &context::progressive_strategy)
        .def_readwrite("chunk_size",            &context::chunk_size)
        .def_readwrite("prune_threshold",       &context::prune_threshold)
        .def_property("align_sequence",         &context::align_sequence_get,         &context::align_sequence_set)
        .def_property("align_sequence_by",      &context::align_sequence_by_get,      &context::align_sequence_by_set)
        .def_property("duration_type",          &context::duration_type_get,          &context::duration_type_set)
        .def_property("quantum",                &context::quantum_get,                &context::quantum_set)
        .def_property("aggregate_coarse_level", &context::aggregate_coarse_level_get, &context::aggregate_coarse_level_set)
        .def_property("hidden_items",           &context::hidden_items_get,           &context::hidden_items_set)
        .def_property_readonly("valid_type_combination", [](const context& context) {
                    const auto& types = context.valid_type_combination;
                    py::dict values;
                    for(const auto& t : types)
                        values[py::str(context.event_name(t))] = t; 
                    return values;
                })
        .def("event_name", &context::event_name)
        .def("get_nb_low_level_events",  py::overload_cast<>(&context::get_nb_low_level_events, py::const_))
        .def("get_nb_distinct_pathways", &context::get_nb_distinct_pathways)
        .def("get_nb_useful_nodes", &context::get_nb_useful_nodes)
        .def("update_view_data",  &context::update_view_data)
        .def("process_all",  &context::process_all)
        .def("process_next", &context::process_next)
        .def("process_done", &context::process_done)
        .def("process", py::overload_cast<size_t>(&context::process))
        .def("print", &context::print_string, py::arg("print_histograms") = false)
        .def("print_histogram", py::overload_cast<HistogramToPrint, node_id>(&context::print_histogram, py::const_))
        .def("print_barchart",  py::overload_cast<BarchartToPrint,  node_id>(&context::print_barchart, py::const_))
        .def("get_node", &get_node)
        .def("tree", [](context& context) {
                         return get_tree(context, context.graph);
                     })
        .def("output_tree", [](context& context) {
                         return get_tree(context, context.output_graph);
                     });
#ifdef VERSION_INFO
    m.attr("__version__") = VERSION_INFO;
#else
    m.attr("__version__") = "dev";
#endif
}
#endif
