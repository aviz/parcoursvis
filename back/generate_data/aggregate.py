import sys
import os
import json
import logging
import time
from functools import *

import parcoursprog

logging.basicConfig()

def printDocAndExit():
    print("Run ./aggregate.py [data_directory] [--output JsonOutput] [-h] [--help]")
    print("data_directory: the data directory containing the preparsed data binary files that parcoursvis will read. Default: working_directory/tmp")
    print("--output: output path to save the aggregated tree in a JSON format.")
    print("-h --help: Print this documentation and exit.")

    sys.exit(-1)

if __name__ == '__main__':
    argv          = sys.argv[1:]
    dataDirectory = "tmp"
    files         = []
    outputFile    = os.getcwd() + "/output.json"

    i = 0
    while i < len(argv):
        #Read input file names (beginning of the command)
        arg = argv[i]
        if i == 0 and not (arg.startswith("--") or arg.startswith("-")):
            dataDirectory = arg
            i+=1
        else:
            while i < len(argv):
                arg = argv[i]

                if arg == "-h" or arg == "--help":
                    printDocAndExit()

                elif arg == "--output":
                    if i < len(argv)-1:
                        outputFile = argv[i+1]
                        i+=1
                    else:
                        print("Missing JSON file path value to '--output' parameter")
                        sys.exit(-1)
                else:
                    print("Unknown parameter {}".format(arg))
                    printDocAndExit()
                i+=1

    assert os.path.isfile(os.path.join(dataDirectory, "data.bin"))
    assert os.path.isfile(os.path.join(dataDirectory, "data.map"))
    assert os.path.isfile(os.path.join(dataDirectory, "indi.bin"))

    ctx = parcoursprog.context()
    ctx.apply_filter    = False #Do not filter individuals in the generation of the tree
    ctx.prune_threshold = 0
    ctx.load(dataDirectory)
    ctx.process_all()
    ctx.update_view_data()

    with open(outputFile, 'w') as f:
        f.write(ctx.print(True))
