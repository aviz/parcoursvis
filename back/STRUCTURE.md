# ParcoursVis: Backend code structure

The backend of parcoursvis relies both on a Python main application, and a native C++ library for optimize computation on the datasets.
The application does not store what a user is doing after the end of his/her session. However, the application relies on "delta of operations" (stateful) instead of on "current status" (stateless)

## C++ / Python program

### The C++ library

The core component of the back end is a C++ library. This library does not use external libraries others than the standard UNIX libraries, and roaring bitmap (for a very specific case). The C++ library mainly relies on multiple memory mapping on the different files composing the dataset(s). 

The main file of the library is parser/src/pparser.cpp . The p(rogressive)parser describes the whole "context" of the tree of sequences the user is manipulating in the frontend application.
The messages, for what considers the application, are in a JSON format.

### The Python main app

We rely on Python to handle efficiently the websocket and all the messages received from all the frontend clients. The entry point is in parser/index.py.

This script creates as many "context" objects as necessary (usually one per client, except on some specific use-cases like when a client wants to compare two different contexts).

### Communication with the frontend

The communication relies on simple JSON objects. Most objects that are aimed to be sent provides a "print" and/or "str" functionalities to export their data to a JSON format.

## Generate synthetic data

All the scripts to generate syntethical datasets are in generate\_data/
- A python script "aggregate.py" that takes into parameter the dataset to analyze (e.g., qing\_2m) and outputs the aggregated tree into a JSON file format
- A python script "generate.py" that takes into parameter the JSON file format generated in the previous step and the number of patients to generate. This script will then generate a new dataset with N patients that, if this dataset is aggregated again, it should output a tree similar to what the original dataset outputs.

## Testing

All the tests are in the parser/src/tests/ folder. 
We handle for the moment only unit testing. Those are automatically run using the gitlab continuous integration tool (see gitlab-ci.yml).

### Unit testing

We use the googletest suitcase for our unit testing. We provide a CMakeLists.txt to compile the tests with CMake. 
The unit tests will try to open the folder "tests" (the one we provide in back/parser/tests) which should be at the same level as the unit test program.

## Benchmark and Scalability of ParcoursVis

In the folder "back/parser/benchmark", we provide:
- a Python script (benchmark/benchmark.py) to benchmark the application. The python script outputs a JSON file. This JSON file can be put in benchmark/visualization\_d3/src/data0.json or data1.json to visualize the results
- a D3 Javascript application to visualize the results (see back/parser/benchmark/visualization\_d3). Run ```npm install''' to install the necessary npm packages, and ```npm serv''' to compile the application and launch a local serveur.
- a Python script (benchmark/analyze.py) to analyze automatically the results and outputs the key values we report in our article "TODO"
- a Python script (benchmark/performance.py) to compute and display the total amount of time necessary to build the aggregated tree of given datasets. This script is their to measure the scalability of our application
